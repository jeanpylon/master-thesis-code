import numpy as np

class ISTA:
  """ Implements ISTA for recovery of sparse signals in R^N """
  def __init__(self):
    self.alpha = None

  def recover(self, Y, A, iterations=1000, lmb=.001):
    """ Recovers signals from measurements using the ISTA algorithm.

    This function uses the iterative ISTA algorithm to recover the signals that yielded the given measurements sampling
    with the given sampling modality.

    :param Y: A k by m matrix with rows as measurements
    :param A: The sampling modality, that is an m by N matrix
    """
    print(f"Signal recovery with ISTA")
    M_k, M_m = Y.shape
    A_m, A_N = A.shape
    if M_m != A_m:
      raise ValueError("Incompatible dimensions on measurements and sampling modality.")

    k = M_k
    m = M_m
    N = A_N

    # set t
    L = np.linalg.norm(A, ord=2)**2 # Lipschitz constant of gradient of f
    t = 1/L # step length
    self.alpha = lmb*t

    print(f"Number of signals: {k}")
    print(f"Number of entries in measurements: {m}")
    print(f"Number of entries in signals: {N}")
    print(f"Lipschitz constant: {L}, step size: {t}, lambda: {lmb}, alpha: {self.alpha}")

    # transpose Y s.t. it is set up correctly for direct application of ISTA later in the function
    Y = Y.T

    # initiate recovered signals, now with columns as signals s.t. it is set up correctly for
    # direct application of ISTA later
    X_star = np.zeros((k, N)).T

    # ISTA loop
    for iteration in range(iterations):
      print(f"Iteration {iteration} / {iterations} ...")
      X_star = self._shrink_op(X_star - 2*t*np.matmul(A.T, np.matmul(A, X_star) - Y))

    return X_star.T

  def _shrink_op(self, x, alpha=None):
    """ Implements the shrinkage operator as specified in eq (1.5) in the Beck and Teboulle article. """
    if alpha is None:
      alpha = self.alpha
    return np.multiply(np.maximum((np.abs(x) - alpha), 0), np.sign(x))


### Testing
if __name__ == "__main__":

  # Test the shrinkage operator
  ista = ISTA()
  x = np.linspace(-10, 10, 41)
  y = ista._shrink_op(x, alpha=1)
  import matplotlib.pyplot as plt
  plt.plot(x, y)
  plt.show()