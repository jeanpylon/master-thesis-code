import pywt
import numpy as np
import lib.data_generation.fourier_sampling

class FISTA2D:
  def __init__(self, A, AT):
    """ Sets up FISTA ready to go.

    :param A: the sampling modality used to aquire measurements from signals
    :param AT: the transpose of the sampling modality A
    """
    self.sampling_modality = A
    self.sampling_modality_transpose = AT
    self.wavelet = None # the wavelet to be used
    self.mode = "periodization" # the way to treat the edges in the wavelet transform
    self.alpha = 0

  def dec_to_array(self, wavelet_dec):
    """ Converts the tuples of 2D arrays in the wavelet decomposition of pywavelet to 3D arrays and fiannly multiplies
        all the arrays by t.
    :param wavelet_dec: a py-wavelet wavelet decomposition over n levels (a list as described in pywavelet docs)
    :param t: float to multiply by
    """
    for i in range(1, len(wavelet_dec)):
      wavelet_dec[i] = np.array(wavelet_dec[i])

    return np.array(wavelet_dec)

  def array_to_dec(self, wavelet_dec):
    """ The incverse of dec_to_array."""
    for i in range(1, len(wavelet_dec)):
      wavelet_dec[i] = tuple(wavelet_dec[i])

    return list(wavelet_dec)

  def recover(self, Y, iterations=1000, lmb=.001, wavelet=None):
    """ Recovers images (signals) from measurements using the FISTA algorithm.

    This function uses the iterative FISTA algorithm to recover the images that yielded the given measurements sampling
    with the given sampling modality.

    :param Y: A k by n by n cube with 2nd and 3rd dimensions as images
    :param n: the number of pixels of one side in the image

    :return: the wavelet coefficients of the recovered images
    """
    if wavelet is None:
      self.wavelet = "db4"
    else:
      self.wavelet = wavelet

    print(f"Signal recovery with FISTA")
    k, m = Y.shape

    # set t
    #L = np.linalg.norm(A, ord=2)**2 # Lipschitz constant of gradient of f
    L = 2
    t = 1/L # step length
    self.alpha = lmb*t

    print(f"Number of signals: {k}")
    print(f"Number of entries in measurements: {m}")
    print(f"Lipschitz constant: {L}, step size: {t}, lambda: {lmb}, alpha: {self.alpha}")

    # transpose Y s.t. it is set up correctly for direct application of FISTA later in the function

    # initiate recovered signals, now with columns as signals s.t. it is set up correctly for
    # direct application of FISTA later
    X_old = self.WFtPt(Y)
    Y_old = X_old
    c_old = 1

    # FISTA loop
    for iteration in range(iterations):
      print(f"Iteration {iteration} / {iterations} ...")
      X_new = self.WFtPt(self.PFWt(Y_old) - Y)
      X_new = X_new*t
      X_new = self._shrink_op(Y_old - X_new)
      c_new = (1 + np.sqrt(1 + 4*c_old**2))/2
      Y_new = X_new + (c_old - 1)/(c_new)*(X_new - X_old)
      X_old = X_new
      c_old = c_new
      Y_old = Y_new

    return X_new


  def _shrink_op(self, x, alpha=None):
    """ Implements the shrinkage operator as specified in eq (1.5) in the Beck and Teboulle article. """
    if alpha is None:
      alpha = self.alpha
    for i in range(len(x)):
      x[i] = np.multiply(np.maximum(np.abs(x[i]) - alpha, 0), np.sign(x[i]))

    return x

  def WFtPt(self, Y):
    """ Does a transpose subsampling, a 2D fourier inverse shift, a 2D fourier inverse transform and
        then a wavelet decomposition.

    :param Y: The measurements to perform the operation on.
    """
    if self.sampling_modality_transpose is None:
      raise RuntimeError("No sampling modality transpose.")

    X = self.sampling_modality_transpose(Y)
    X = pywt.wavedec2(X, self.wavelet, mode=self.mode)
    X = self.dec_to_array(X) # convert to format suitable for computation

    return X

  def PFWt(self, X):
    """ Does a 2D wavelet reconstruction, a 2D fourier transform, a 2D fourier shift and then a subsampling.

    :param X: the images (signals) to perform the operation on.
    """
    if self.sampling_modality is None:
      raise RuntimeError("No sampling modality.")

    X = self.array_to_dec(X) # convert to format suitable for pywavelet.waverec2
    X = pywt.waverec2(X, self.wavelet, mode=self.mode)
    Y = self.sampling_modality(X)

    return Y