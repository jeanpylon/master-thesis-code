import numpy as np

class FISTA:
  """ Implements FISTA for recovery of sparse signals in R^N """
  def __init__(self):
    self.alpha = None

  def recover(self, Y, sampling_operator, iterations=1000, lmb=.001):
    """ Recovers signals from measurements using the FISTA algorithm.

    This function uses the iterative FISTA algorithm to recover the signals that yielded the given measurements sampling
    with the given sampling modality.

    :param Y: A k by m matrix with rows as measurements
    :param sampling_operator: An object implementing the SamplingOperator interface.
    """
    print(f"Signal recovery with FISTA")
    M_k, M_m = Y.shape
    A_m, A_N = sampling_operator.shape
    if M_m != A_m:
      raise ValueError("Incompatible dimensions on measurements and sampling modality.")

    k = M_k
    m = M_m
    N = A_N

    # set t
    L = sampling_operator.lipschitz
    t = 1/L # step length
    self.alpha = lmb*t

    print(f"Number of signals: {k}")
    print(f"Number of entries in measurements: {m}")
    print(f"Number of entries in signals: {N}")
    print(f"Lipschitz constant: {L}, step size: {t}, lambda: {lmb}, alpha: {self.alpha}")

    # transpose Y s.t. it is set up correctly for direct application of FISTA later in the function
    #Y = Y.T

    # initiate recovered signals, now with columns as signals s.t. it is set up correctly for
    # direct application of FISTA later
    X_new_list = []
    for i in range(len(Y)):
      print(f"Image {i+1} ...")
      X_old = sampling_operator.adjoint_operator(Y[i:i+1])
      Y_old = X_old
      c_old = 1

      # in case of 0 iterations
      X_new = X_old

      # FISTA loop
      for iteration in range(iterations):
        print(f"    Iteration {iteration} / {iterations} ...")
        X_new = self._shrink_op(Y_old - t * sampling_operator.adjoint_operator(sampling_operator.operator(Y_old) - Y[i:i+1]))
        c_new = (1 + np.sqrt(1 + 4*c_old**2))/2
        Y_new = X_new + (c_old - 1)/(c_new)*(X_new - X_old)
        X_old = X_new
        c_old = c_new
        Y_old = Y_new

      print(f"Shape of X_new: {X_new.shape}")
      X_new_list.append(X_new[0])

    return np.array(X_new_list)

  def _shrink_op(self, x, alpha=None):
    """ Implements the shrinkage operator as specified in eq (1.5) in the Beck and Teboulle article. """
    if alpha is None:
      alpha = self.alpha
    return np.multiply(np.maximum((np.abs(x) - alpha), 0), np.sign(x))


