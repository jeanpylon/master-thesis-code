import os
import numpy as np
import matplotlib.pyplot as plt

def safe_save(keras_model, filename):
  """ This function saves the specified Keras model to the specified filename but avids overwrites.

  :param keras_model: the Keras model you want to save
  :param filename: the filename (with full path) you want to save to
  :return: The filename the model was saved to
  """
  # make sure you don't overwrite an existing file TODO: fix remove '' around filename when loop is executed
  counter = 1
  addition = ""
  while os.path.isfile(filename + addition + ".ker"):
    addition = "[" + str(counter) + "]"
    counter += 1

  # save model
  save_filename = filename + addition + ".ker"
  keras_model.save(save_filename)

  return save_filename

def safe_name(filename, filename_extension):
  """ This function changes filenames for .png-plots until it doesn't exist.

  :param filename: the filename (with full path, without extension) you want to save to
  :param filename_extension: the extension of the filename you want to save to
  :return: a filename that is not used yet
  """
  # TODO: fix remove '' around filename when loop is executed
  counter = 1
  addition = ""
  while os.path.isfile(filename + addition + filename_extension):
    addition = "[" + str(counter) + "]"
    counter += 1

  save_filename = filename + addition + filename_extension

  return save_filename

def safe_save_with_plot(keras_model, filename, training_values):
  """ This function saves the specified Keras model to the specified filename but avids overwrites.

  :param keras_model: the Keras model you want to save
  :param filename: the filename (with full path) you want to save to
  :param training_values: the plot data_generation
  :return: The filename the model was saved to, the filename the plot was saved to
  """
  filename_m = safe_save(keras_model, filename)
  plt.clf()
  plt.plot(training_values)
  plt.xlabel("Epoch")
  plt.ylabel("Loss (MSE)")
  plt.title("Training loss")
  filename_t = safe_name(filename)
  plt.savefig(filename_t)
  plt.clf()

  return filename_m, filename_t

def reshape_y(data):
  """ Reshaping m by n tensor to m by 1 by n tensor """
  new = np.reshape(data, (data.shape[0], 1, data.shape[1]))
  check_y(data, new)
  return new

def check_y(data, conv_data):
  """ Temporary function: checking correctness of reshaping done in reshape_y """
  m, dummy, n = conv_data.shape

  m_prime, n_prime = data.shape
  if m_prime != m or n_prime != n:
    print("Invalid dimensions. [Check1]")

  for i in range(m):
    for j in range(n):
      if data[i,j] != conv_data[i, 0, j]:
        raise Exception("Error in reshaping")

def reshape_x(data):
  """ Reshaping m by n tensor to m by n by 1 tensor """
  new = np.reshape(data, (data.shape[0], data.shape[1], 1))
  check_x(data, new)
  return new

def check_x(data, conv_data):
  """ Temporary function: checking correctness of reshaping done in reshape_x """
  m, n, dummy = conv_data.shape
  m_prime, n_prime = data.shape
  if m_prime != m or n_prime != n:
    print("Invalid dimensions. [Check2]")

  for i in range(m):
    for j in range(n):
      if data[i, j] != conv_data[i, j, 0]:
        raise Exception("Error in reshaping")

def save_training(model, model_name, loss_history, loss, model_path, data_path, custom_info):
  """ Saves the model and a loss plot after a training session.

  More specifically this function generates a filename for the model and a filename for the plot of the training losses.
  It then saves the plot and the model to the specified model path. The saving is safe, meaning that an index will be
  added o the filename and incremented until no overwrite will happen. The function also creates the folder of the
  filename if it doesn't already exist.

  :param model: The trained Keras model
  :param model_name: String describing the model
  :param loss_history: 1d-array of losses for each  epoch
  :param loss: String with name of loss metric
  :param model_path: String with desired save path (without trailing slash)
  :param data_path: String with path of the data_generation used in the training (without trailing slash)
  :param custom_info: String with custom info about NN (used in filename so keep it short)
  """
  # 1 Create filenames
  folder_name = model_path
  filename = folder_name + "/" + model_name + "_epochs_" + str(len(loss_history)) + "_" + custom_info
  model_filename = safe_name(filename, "")
  plot_filename = safe_name(filename, ".png")

  # 2 Make directory if it doesn't already exist
  if not os.path.isdir(folder_name):
    os.mkdir(folder_name)

  # 3 Save model
  model.save(model_filename)

  # 4 Make and save plot
  plt.clf()
  plt.plot(loss_history)
  plt.xlabel("Epoch")
  plt.ylabel(f"Loss ({loss})")
  plt.title("Training loss")
  plt.savefig(plot_filename)
  plt.clf()

  print(f"Model was saved to file: '{model_filename}'.\nPlot was saved to file: '{plot_filename}'")

  return model_filename, plot_filename

