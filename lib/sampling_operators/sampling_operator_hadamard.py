from lib.sampling_operators.sampling_operator_dense import SamplingOperatorDense

class SamplingOperatorHadamard(SamplingOperatorDense):
  def __init__(self, matrix_file_name, sampling_pattern):
    super().__init__(matrix_file_name, "H", "Hadamard", sampling_pattern)

  @property
  def sparsifying_transform(self):
    return False

