from lib.sampling_operators.sampling_operator import SamplingOperator
import numpy as np

class SamplingOperatorFourier(SamplingOperator):
  """ Implements the DFT matrix of given dimension. """
  def __init__(self, sampling_pattern):
    """
    :param n: The number of pixels in each side of the images
    :param sampling_pattern: An instance of the class SamplingPattern
    """
    self.sampling_pattern = sampling_pattern.sampling_pattern
    self.sampling_pattern_object = sampling_pattern
    self.sampling_pattern_meta_data = sampling_pattern.meta_data
    self.n = sampling_pattern.n
    self.input_dim = 2

  @property
  def sparsifying_transform(self):
    return False

  def matlab_reshape(self, x, shape):
    """ Wrapper for numpy's reshape. Reshapes according to matlab matrix indexing. """
    x_new = np.zeros((x.shape[0],) + shape, dtype=np.complex128)
    for i, datapoint in enumerate(x):
      x_new[i] = datapoint.reshape(shape, order=self.__class__.MATLAB_MATRIX_INDEXING)

    return x_new

  def copy_sampling_pattern(self, file_path):
    """ Copies the sampling pattern to the given file path. """
    self.sampling_pattern_object.copy(file_path)

  def operator(self, X):
    """ Copmputes the DFT followed by a sampling pattern.

    Uses self.sampling_pattern as sampling pattern.

    :param X: Input images, 3D-array of floats (dims: (batch size, height, width))
    :return: sub sampled fourier transform of the input image, a 2D-array of floats
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please initiate self.sampling_pattern.")
    Y = np.fft.fft2(X)
    Y = np.fft.fftshift(Y, axes=(-2, -1)) / self.n
    y = self.matlab_reshape(Y, (Y.shape[1] * Y.shape[2],))
    y = y.take(self.sampling_pattern, axis=1)

    return y

  def adjoint_operator(self, y):
    """ This is the adjoint of self.operator. It computes  (P x F)* x Y.

    Uses self.sampling_pattern as sampling pattern.

    :param y: measurements as 2D-array of floats with shape (batch size, no. of measurements)
    :return: noisy images as 3D-arrays of floats with shape (batch size, height, width)
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please initiate self.sampling_pattern.")

    k, m = y.shape # get dims
    rep_sampling_pattern = self.sampling_pattern.reshape((1,) + self.sampling_pattern.shape)
    x = np.zeros((k, self.n**2), dtype=np.complex128)
    np.put_along_axis(x, rep_sampling_pattern, y, axis=1)
    X = self.matlab_reshape(x, (self.n, self.n)) #x.reshape((k, self.n, self.n))
    X = np.fft.ifftshift(X, axes=(1,2))
    X = np.fft.ifft2(X)*self.n

    return X

  @property
  def meta_data(self):
    sampling_operator_meta_data = {}
    sampling_operator_meta_data["Operator type"] = "P o F"
    sampling_operator_meta_data["Operator type description"] = "Fourier transform. Then sub sampling with given sampling pattern."
    sampling_operator_meta_data.update(self.sampling_pattern_meta_data)
    meta_data = {"Sampling operator": sampling_operator_meta_data}

    return meta_data

  @property
  def shape(self):
    return (len(self.sampling_pattern), self.n**2)