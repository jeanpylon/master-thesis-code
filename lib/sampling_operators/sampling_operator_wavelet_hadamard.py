from lib.sampling_operators.sampling_operator_hadamard import SamplingOperatorHadamard
import pywt
import numpy as np

class SamplingOperatorWaveletHadamard(SamplingOperatorHadamard):
  def __init__(self, matrix_file_name, sampling_pattern, wavelet_type, mode):
    super().__init__(matrix_file_name, sampling_pattern)
    self.wavelet_type = wavelet_type
    self.mode = mode
    self.slices = None

  @property
  def sparsifying_transform(self):
    return True

  def operator(self, X):
    """ Computes the inverse wavelet transform followed by the Hadamard transform and a sampling pattern.

    Uses self.sampling_pattern as sampling pattern.

    :param X: Input images coefficients, 3D-array of complex numbers (dims: (batch size, height, width))
    :return: sub sampled fourier transform of the input images, a 2D-array of floats (dim: (batch size, number of samples))
    """
    Y = []
    for x in X:
      y = pywt.array_to_coeffs(x, self.slices, output_format="wavedec2")
      y = pywt.waverec2(y, self.wavelet_type, mode=self.mode)
      Y.append(y)
    Y = np.array(Y)

    return super().operator(Y)

  def adjoint_operator(self, y):
    """ This is the adjoint of self.operator. It computes  (P x F x S)* x Y.

    Uses self.sampling_pattern as sampling pattern.

    :param y: measurements as 2D-array of floats with shape (batch size, no. of measurements)
    :return: wavelet transforms as 3D-arrays of floats with shape (batch size, height, width)
    """
    X = []
    Y = super().adjoint_operator(y)
    for y in Y:
      x = pywt.wavedec2(y, self.wavelet_type, mode=self.mode)
      x, self.slices = pywt.coeffs_to_array(x)
      X.append(x)
    X = np.array(X)

    return X

  def sparsifying_synthesize(self, X):
    """ Synthesizes acc to sparsifying transform."""
    Y = []
    for x in X:
      y = pywt.array_to_coeffs(x, self.slices, output_format="wavedec2")
      y = pywt.waverec2(y, self.wavelet_type, mode=self.mode)
      Y.append(y)
    Y = np.array(Y)

    return Y

  @property
  def meta_data(self):
    sampling_operator_meta_data = {}
    sampling_operator_meta_data["Operator type"] = "P o H o W*"
    sampling_operator_meta_data["Operator type description"] = "Wavelet reconstruction, hadamard transform, sub sampling with given sampling pattern."
    sampling_operator_meta_data["Wavelet type"] = self.wavelet_type
    sampling_operator_meta_data["Wavelet extension type"] = self.mode
    sampling_operator_meta_data.update(self.sampling_pattern_meta_data)
    meta_data = {"Sampling operator": sampling_operator_meta_data}

    return meta_data

