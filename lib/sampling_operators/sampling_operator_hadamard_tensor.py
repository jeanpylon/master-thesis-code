from lib.sampling_operators.sampling_operator_dense_tensor import SamplingOperatorDenseTensor
import numpy as np

class SamplingOperatorHadamardTensor(SamplingOperatorDenseTensor):
  def __init__(self, matrix_file_name, sampling_pattern):
    super().__init__(matrix_file_name, "H", "Hadamard", sampling_pattern)

  @property
  def sparsifying_transform(self):
    return False

  def operator(self, X):
    """ Computes the matrix transform of the image followed by a sampling pattern.

    Uses self.sampling_pattern as sampling pattern.

    :param X: Input images, 3D-array of floats (dims: (batch size, height, width))
    :return: sub sampled gaussian transform of the input image, a 2D-array of floats
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please initiate self.sampling_pattern.")
    Y = np.matmul(np.matmul(self.matrix, X), self.matrixT)
    y = self.matlab_reshape(Y, (Y.shape[1] * Y.shape[2],))/self.n
    y = y.take(self.sampling_pattern, axis=1)

    return y

  def adjoint_operator(self, y):
    """ This is the adjoint of self.operator. It computes  (P x A)* x Y.

    Uses self.sampling_pattern as sampling pattern.

    :param y: measurements as 2D-array of floats with shape (batch size, no. of measurements)
    :return: noisy images as 3D-arrays of floats with shape (batch size, height, width)
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please initiate self.sampling_pattern.")

    k, m = y.shape # get dims
    rep_sampling_pattern = self.sampling_pattern.reshape((1,) + self.sampling_pattern.shape)
    x = np.zeros((k, self.n**2))
    np.put_along_axis(x, rep_sampling_pattern, y, axis=1)
    X = self.matlab_reshape(x, (self.n, self.n_2)) #x.reshape((k, self.n, self.n))
    X = np.matmul(self.matrix, np.matmul(X, self.matrixT))/self.n

    return X

