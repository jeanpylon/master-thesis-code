"""Base class for sampling operators."""

from abc import abstractmethod, ABC

class SamplingOperator(ABC):
  MATLAB_MATRIX_INDEXING = "F" # The correct way to use numpy's reshape in order to index arrays the same way as matlab

  @property
  def sparsifying_transform(self):
    """ Returns whether this operator includes a sparsifying transform. """
    pass

  @abstractmethod
  def operator(self, x):
    """ Implements the sampling operator. """
    pass

  @abstractmethod
  def adjoint_operator(self, x):
    """ Implements the inverse sampling operator. """
    pass

  @property
  @abstractmethod
  def meta_data(self):
    """ Returns a dictionary with operator meta data."""
    pass

  @property
  @abstractmethod
  def shape(self):
    """ Returns the shape of the matrix representation of this operator. """
    pass
