from lib.sampling_operators.sampling_operator import SamplingOperator
import numpy as np
from os.path import abspath

class SamplingOperatorDense1d(SamplingOperator):
  """ Sampling operator class for dense explicit matrices followed by a sampling pattern. """
  def __init__(self, matrix_file_name, matrix_symbol, matrix_name, sampling_pattern, scale=False):
    """
    :param matrix_file_name: The name of the file (.npy) containing the matrix 2D-array for this
                             sampling operator to use.
    :param matrix_symbol: The symbol to denote the matrix by in the meta data.
    :param matrix_name: The name to denote the matrix by in the meta data.
    :param sampling_pattern: An object implementing the SamplingOperator interface. Represents
                             the sampling patten for this sampling operator to use.
    """
    # set parameters
    self.sampling_pattern = sampling_pattern.sampling_pattern
    self.sampling_pattern_object = sampling_pattern
    self.sampling_pattern_meta_data = sampling_pattern.meta_data
    #self._n = sampling_pattern.n # use _n because there is no setter for self.n. Should not be able to be set by user.
    self.matrix_file_name = abspath(matrix_file_name)
    self.matrix_symbol = matrix_symbol
    self.matrix_name = matrix_name
    self.input_dim = 1

    # subsample matrix
    full_matrix = np.load(self.matrix_file_name)
    m, n = full_matrix.shape
    if m != n:
      print(f"WARNING: Matrix is not square. Shape: {full_matrix.shape}.")

    # TODO: setup this check again

    #if self.N != m:
    #  print(f"Sampling pattern n (={sampling_pattern.n}) doesn't match matrix n (={n}).")
    self.matrix = full_matrix[self.sampling_pattern]
    if scale:
      self.matrix = self.matrix/np.sqrt(len(self.sampling_pattern))
    self.m, self.N = self.matrix.shape

  @property
  def A(self):
    return self.matrix

  def operator(self, X):
    """ Computes the matrix transform of the image followed by a sampling pattern.

    Uses self.sampling_pattern as sampling pattern.

    :param X: Input images, 2D-array of floats (dims: (batch size, signal length))
    :return: sub sampled gaussian transform of the input image, a 2D-array of floats
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please initiate self.sampling_pattern.")
    y = np.matmul(self.matrix, X.T).T

    return y

  def adjoint_operator(self, y):
    """ This is the adjoint of self.operator. It computes  (P x A)* x Y.

    Uses self.sampling_pattern as sampling pattern.

    :param y: measurements as 2D-array of floats with shape (batch size, no. of measurements)
    :return: noisy images as 2D-arrays of floats with shape (batch size, signal length)
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please initiate self.sampling_pattern.")

    x = np.matmul(self.matrix.T, y.T).T

    return x

  @property
  def meta_data(self):
    sampling_operator_meta_data = {}
    sampling_operator_meta_data["Operator type"] = f"P o {self.matrix_symbol}"
    sampling_operator_meta_data["Operator type description"] = f"{self.matrix_name} {self.m}x{self.N}-matrix followed by specified sampling pattern."
    sampling_operator_meta_data["Matrix file name"] = self.matrix_file_name
    sampling_operator_meta_data.update(self.sampling_pattern_meta_data)
    meta_data = {"Sampling operator": sampling_operator_meta_data}

    return meta_data

  @property
  def shape(self):
    return self.matrix.shape

  @property
  def lipschitz(self):
    return np.linalg.norm(self.matrix, ord=2) ** 2 # Lipschitz constant of gradient of matrix

  def copy_sampling_pattern(self, file_path):
    """ Copies the sampling pattern to the given file path. """
    self.sampling_pattern_object.copy(file_path)


### Testing ###
if __name__ == "__main__":
  from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern
  from lib.sampling_operators.sampling_operator_gaussian_1d import SamplingOperatorGaussian1d
  N = 1024
  file_path = "/home/jab/Skole/Master/extra_data/matrices/250x500_gaussian_matrix_LISTA_reproduction.npy"
  samp_patt_file = "/home/jab/Skole/Master/extra_data/sampling_patterns/n_250/sampling_pattern_identity_n250_sr1.000.mat"

  sampling_pattern = MatLabSamplingPattern(samp_patt_file)
  samp_op = SamplingOperatorGaussian1d(file_path, sampling_pattern)

  meta_data = samp_op.meta_data
  for key, value in meta_data.items():
    print(key)
    for inner_key, inner_value in value.items():
      print(f"{inner_key}: {inner_value}")
