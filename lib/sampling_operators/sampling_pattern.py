from scipy.io import loadmat
import numpy as np
from shutil import copyfile


class MatLabSamplingPattern:
  """ Adapter for samplingpatterns created in matlab."""
  def __init__(self, mat_lab_workspace_filename):
    self.mat_lab_workspace_file_name = mat_lab_workspace_filename
    workspace = loadmat(mat_lab_workspace_filename)
    if "sampling_pattern" not in workspace:
      raise ValueError("Error: File loaded doesn't contain variable 'sampling_scheme'.")
    sampling_pattern = workspace["sampling_pattern"]
    sampling_pattern = sampling_pattern.reshape(sampling_pattern.shape[0])
    self.sampling_pattern = sampling_pattern

    sampling_pattern_meta_data = {}
    sampling_pattern_meta_data["Sampling ratio"] = float(workspace["sampling_ratio"][0][0])
    sampling_pattern_meta_data["Number of samples"] = int(workspace["nbr_samples"][0][0])
    sampling_pattern_meta_data["Number of levels"] = int(workspace["nbr_levels"][0][0])
    sampling_pattern_meta_data["Number of fully sampled levels (r_0)"] = int(workspace["r_0"][0][0])
    sampling_pattern_meta_data["a"] = float(workspace["a"][0][0])
    sampling_pattern_meta_data["n"] = int(workspace["n"][0][0])
    sampling_pattern_meta_data["String ID"] = str(workspace["string_id"][0])
    self._meta_data = {"Sampling pattern": sampling_pattern_meta_data}
    self._n = int(workspace["n"][0][0])
    self._n_2 = int(workspace["n"][0][0])
    if "n_2" in workspace:
      try:
        self._n_2 = workspace["n_2"][0][0]
      except Exception as e:
        print("WARNING: Something went wrong loading item with key 'n_2': {}".format(str(e)))

  @property
  def meta_data(self):
    return self._meta_data

  @property
  def n(self):
    return self._n

  @property
  def n_2(self):
    return self._n_2

  def copy(self, file_path):
    copyfile(self.mat_lab_workspace_file_name, file_path)

class NumpySamplingPattern:
  """ Adapter for samplingpatterns created in numpy."""
  def __init__(self, sampling_pattern_file_name):
    self.sampling_pattern_file_name = sampling_pattern_file_name
    self.sampling_pattern = np.load(sampling_pattern_file_name)
    self.meta_data = {"Sampling pattern": "Not implemented."}
    raise NotImplementedError("May not hold all the features that the matlab version has.")

  def copy(self, file_path):
    copyfile(self.sampling_pattern_file_name, file_path)

###############
### Testing ###
###############

if __name__ == "__main__":
  file_name = "/home/jab/Skole/Master/data/sampling_patterns/sampling_pattern_0.05.mat"
  samp_patt = MatLabSamplingPattern(file_name)
  print(samp_patt.meta_data)
  print(samp_patt.n)
