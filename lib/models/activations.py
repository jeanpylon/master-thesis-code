from keras.backend import abs, maximum, sign
from keras.layers import Layer

class ProximalActivation:
  """ Proximal activation

  f_alpha(x)_i := (|x_i| - alpha)*sgn(x_i)

  """
  def __init__(self, alpha):
    self.alpha = alpha

  def evaluate(self, x):
    return maximum(abs(x) - self.alpha, 0)*sign(x)

class IdentityActivation:
  def evaluate(self,x ):
    return x


if __name__ == "__main__":
  act = ProximalActivation(0)
  a = [2,4,5,6]
  print(a)
  print(act.evaluate(a))