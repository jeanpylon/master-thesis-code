from tensorflow.keras.layers import Dense, Activation, Conv2D, Input, BatchNormalization, LeakyReLU, Conv2DTranspose, ReLU, Concatenate, Add
from tensorflow.keras.activations import tanh
from tensorflow.keras.models import Sequential, Model

N = 256 # Pixels per side in image
norm_ax = -1 # Axis to normalize over
alpha_l_relu = .2 # the alpha in the leaky rectified linearizer function
alpha_relu = .2 # the alpha in the relu

# Input
x_0 = Input(shape=(N, N))

# Layer 1
x_1 = Conv2D(1, (4,4), strides=(2,2))(x_0)

# Layer 2
x_2 = Conv2D(64, (4,4), strides=(2,2))(x_1)
x_2 = BatchNormalization(axis=norm_ax)(x_2)
x_2 = LeakyReLU(alpha=alpha_l_relu)(x_2)

# Layer 3
x_3 = Conv2D(128, (4,4), strides=(2,2))(x_2)
x_3 = BatchNormalization(axis=norm_ax)(x_3)
x_3 = LeakyReLU(alpha=alpha_l_relu)(x_3)

# Layer 4
x_4 = Conv2D(256, (4,4), strides=(2,2))(x_3)
x_4 = BatchNormalization(axis=norm_ax)(x_4)
x_4 = LeakyReLU(alpha=alpha_l_relu)(x_4)

# Layer 5
x_5 = Conv2D(512, (4,4), strides=(2,2))(x_4)
x_5 = BatchNormalization(axis=norm_ax)(x_5)
x_5 = LeakyReLU(alpha=alpha_l_relu)(x_5)

# Layer 6
x_6 = Conv2D(512, (4,4), strides=(2,2))(x_5)
x_6 = BatchNormalization(axis=norm_ax)(x_6)
x_6 = LeakyReLU(alpha=alpha_l_relu)(x_6)

# Layer 7
x_7 = Conv2D(512, (4,4), strides=(2,2))(x_6)
x_7 = BatchNormalization(axis=norm_ax)(x_7)
x_7 = LeakyReLU(alpha=alpha_l_relu)(x_7)

# Layer 8
x_8 = Conv2D(512, (4,4), strides=(2,2))(x_7)
x_8 = BatchNormalization(axis=norm_ax)(x_8)
x_8 = LeakyReLU(alpha=alpha_l_relu)(x_8)

# Layer 9
x_9 = Conv2DTranspose(512, (4,4), strides=(2,2))(x_8)
x_9 = BatchNormalization(axis=norm_ax)(x_9)
x_9 = ReLU(alpha=alpha_relu)(x_9)

# Layer 10
w = Concatenate()([x_9, x_8])
x_10 = Conv2DTranspose(1024, (4,4), strides=(2,2))(w)
x_10 = BatchNormalization(axis=norm_ax)(x_10)
x_10 = ReLU(alpha=alpha_relu)(x_10)

# Layer 11
w = Concatenate()([x_10, x_7])
x_11 = Conv2DTranspose(1536, (4,4), strides=(2,2))(w)
x_11 = BatchNormalization(axis=norm_ax)(x_11)
x_11 = ReLU(alpha=alpha_relu)(x_11)

# Layer 12
w = Concatenate()([x_11, x_6])
x_12 = Conv2DTranspose(1536, (4,4), strides=(2,2))(w)
x_12 = BatchNormalization(axis=norm_ax)(x_12)
x_12 = ReLU(alpha=alpha_relu)(x_12)

# Layer 13
w = Concatenate()([x_12, x_5])
x_13 = Conv2DTranspose(1536, (4,4), strides=(2,2))(w)
x_13 = BatchNormalization(axis=norm_ax)(x_13)
x_13 = ReLU(alpha=alpha_relu)(x_13)

# Layer 14
w = Concatenate()([x_13, x_4])
x_14 = Conv2DTranspose(768, (4,4), strides=(2,2))(w)
x_14 = BatchNormalization(axis=norm_ax)(x_14)
x_14 = ReLU(alpha=alpha_relu)(x_14)

# Layer 15
w = Concatenate()([x_14, x_3])
x_15 = Conv2DTranspose(384, (4,4), strides=(2,2))(w)
x_15 = BatchNormalization(axis=norm_ax)(x_15)
x_15 = ReLU(alpha=alpha_relu)(x_15)

# Layer 16
w = Concatenate()([x_15, x_2])
x_16 = Conv2DTranspose(192, (4,4), strides=(2,2))(w)
x_16 = BatchNormalization(axis=norm_ax)(x_16)
x_16 = ReLU(alpha=alpha_relu)(x_16)

# Layer 17 TODO: fix correct layer type
x_17 = Conv2DTranspose(64, (4,4), strides=(2,2))(x_16)
x_17 = BatchNormalization(axis=norm_ax, activation="tanh")(x_16)

# Layer 18 TODO: fix correct layer type
w = Add([x_17, x_1])
x_18 = ReLU(max_value=1, )(w)

model = Model(inputs=x_0, outputs=x_18)