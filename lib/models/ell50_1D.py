from tensorflow.keras.layers import Dense, Activation, Conv2D, Input, BatchNormalization, LeakyReLU, Conv2DTranspose, ReLU, concatenate, Add, MaxPooling2D
from tensorflow.keras.activations import tanh
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.models import Sequential, Model

def setup_ELL50_1D(n):
  """ Returns the ELL50 network with input (batchsize, n, 1, 1)
  """
  norm_ax = -1 # Axis to normalize over
  opt_mom = .99

  ### ROW ONE
  # Input
  x_1 = Input(shape=(n, 1, 1))

  # Layer 2
  v = Conv2D(64, (3,1), strides=(1,1), padding="same")(x_1)
  v = BatchNormalization(axis=norm_ax)(v)
  x_2 = ReLU()(v)

  # Layer 3
  v = Conv2D(64, (3,1), strides=(1,1), padding="same")(x_2)
  v = BatchNormalization(axis=norm_ax)(v)
  x_3 = ReLU()(v)

  # Layer 4
  v = Conv2D(64, (3,1), strides=(1,1), padding="same")(x_3)
  v = BatchNormalization(axis=norm_ax)(v)
  x_4 = ReLU()(v)

  # Layer 5
  x_5 = MaxPooling2D(pool_size=(2,1))(x_4)


  ### ROW TWO
  # Layer 6
  v = Conv2D(128, (3,1), strides=(1,1), padding="same")(x_5)
  v = BatchNormalization(axis=norm_ax)(v)
  x_6 = ReLU()(v)

  # Layer 7
  v = Conv2D(128, (3,1), strides=(1,1), padding="same")(x_6)
  v = BatchNormalization(axis=norm_ax)(v)
  x_7 = ReLU()(v)

  # Layer 8
  x_8 = MaxPooling2D(pool_size=(2,1))(x_7)

  ### ROW THREE
  # Layer 9
  v = Conv2D(256, (3,1), strides=(1,1), padding="same")(x_8)
  v = BatchNormalization(axis=norm_ax)(v)
  x_9 = ReLU()(v)

  # Layer 10
  v = Conv2D(256, (3,1), strides=(1,1), padding="same")(x_9)
  v = BatchNormalization(axis=norm_ax)(v)
  x_10 = ReLU()(v)

  # Layer 11
  x_11 = MaxPooling2D(pool_size=(2,1))(x_10)

  ### ROW FOUR (left side)
  # Layer 12
  v = Conv2D(512, (3,1), strides=(1,1), padding="same")(x_11)
  v = BatchNormalization(axis=norm_ax)(v)
  x_12 = ReLU()(v)

  # Layer 13
  v = Conv2D(512, (3,1), strides=(1,1), padding="same")(x_12)
  v = BatchNormalization(axis=norm_ax)(v)
  x_13 = ReLU()(v)

  # Layer 14
  x_14 = MaxPooling2D(pool_size=(2,1))(x_13)

  ### ROW FIVE
  # Layer 15
  v = Conv2D(1024, (3,1), strides=(1,1), padding="same")(x_14)
  v = BatchNormalization(axis=norm_ax)(v)
  x_15 = ReLU()(v)

  # Layer 16
  v = Conv2D(1024, (3,1), strides=(1,1), padding="same")(x_15)
  v = BatchNormalization(axis=norm_ax)(v)
  x_16 = ReLU()(v)

  # Layer 17
  v = Conv2DTranspose(1024, (3, 1), strides=(2,1), padding="same")(x_16)
  v = BatchNormalization(axis=norm_ax)(v)
  x_17 = ReLU()(v)

  ### ROW FOUR (right side)
  # Layer 18
  v = concatenate([x_13, x_17])
  v = Conv2D(512, (3,1), strides=(1,1), padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_18 = ReLU()(v)

  # Layer 19
  v = Conv2D(512, (3,1), strides=(1,1), padding="same")(x_18)
  v = BatchNormalization(axis=norm_ax)(v)
  x_19 = ReLU()(v)

  # Layer 20
  v = Conv2DTranspose(512, (3, 1), strides=(2,1), padding="same")(x_19)
  v = BatchNormalization(axis=norm_ax)(v)
  x_20 = ReLU()(v)

  ### ROW THREE (right side)
  # Layer 21
  v = concatenate([x_10, x_20])
  v = Conv2D(256, (3,1), strides=(1,1), padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_21 = ReLU()(v)

  # Layer 22
  v = Conv2D(256, (3,1), strides=(1,1), padding="same")(x_21)
  v = BatchNormalization(axis=norm_ax)(v)
  x_22 = ReLU()(v)

  # Layer 23
  v = Conv2DTranspose(256, (3, 1), strides=(2,1), padding="same")(x_22)
  v = BatchNormalization(axis=norm_ax)(v)
  x_23 = ReLU()(v)


  ### ROW TWO (right side)
  # Layer 24
  v = concatenate([x_7, x_23])
  v = Conv2D(128, (3,1), strides=(1,1), padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_24 = ReLU()(v)

  # Layer 25
  v = Conv2D(128, (3,1), strides=(1,1), padding="same")(x_24)
  v = BatchNormalization(axis=norm_ax)(v)
  x_25 = ReLU()(v)

  # Layer 26
  v = Conv2DTranspose(128, (3, 1),strides=(2,1), padding="same")(x_25)
  v = BatchNormalization(axis=norm_ax)(v)
  x_26 = ReLU()(v)

  ### ROW ONE (right side)
  # Layer 27
  v = concatenate([x_4, x_26])
  v = Conv2D(64, (3,1), strides=(1,1), padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_27 = ReLU()(v)

  # Layer 28
  v = Conv2D(64, (3,1), strides=(1,1), padding="same")(x_27)
  v = BatchNormalization(axis=norm_ax)(v)
  x_28 = ReLU()(v)

  # Layer 29
  x_29 = Conv2D(1, (1, 1), strides=(1, 1))(x_28)

  # Layer 30
  x_30 = Add()([x_29, x_1])

  model = Model(inputs=x_1, outputs=x_30)
  optimizer = SGD(momentum=opt_mom)
  model.compile(optimizer=optimizer, loss="mse")

  return model
