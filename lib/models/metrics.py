import tensorflow.keras.backend as K
from numpy import log
MAX_SIGNAL_VALUE = 1
LOG_10 = log(10)

def mean_pred(y_true, y_pred):
    return K.mean(y_pred)

def psnr(y_true, y_pred):
  """ Expcets one datapoint as input. (Batchsize == 1.)

  NB: No vectorization implemented.

  :return: A single tensor value """
  mse = K.mean((y_true - y_pred)**2, axis=(-1,-2, -3))
  psnr = 20*log(MAX_SIGNAL_VALUE)/LOG_10 - 10*K.log(mse)/LOG_10

  return psnr

def count(tensor):
  s = tensor.shape
  num = 1
  for dim in s:
    num *= dim

  return num


#model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['accuracy', mean_pred])

if __name__ == "__main__":
  pass
