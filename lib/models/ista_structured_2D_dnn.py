from tensorflow.keras.layers import Dense, Activation, Conv2D, Input, BatchNormalization, LeakyReLU, Conv2DTranspose, ReLU, concatenate, Add, MaxPooling2D
from tensorflow.keras.activations import tanh
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.models import Sequential, Model


def setup_ista_structured_2D_dnn(m, n, no_of_layers):
  """ Returns an ista structured DNN with input and outputdimension, dimension (batchsize, m, n, 1) """

  x = Input(shape=(m, n, 1))

  for layer in no_of_layers:
    x_l = Dense(m*n)

  
