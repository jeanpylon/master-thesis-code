from os.path import join
import json
import numpy as np
from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern
from lib.sampling_operators.sampling_operator_fourier import SamplingOperatorFourier
from lib.sampling_operators.sampling_operator_gaussian import SamplingOperatorGaussian
from lib.sampling_operators.sampling_operator_hadamard import SamplingOperatorHadamard
from lib.sampling_operators.sampling_operator_hadamard_tensor import SamplingOperatorHadamardTensor
import time

class ModelTrainer:
  """ Class for training models. """

  MATLAB_MATRIX_INDEXING = "F"

  def __init__(self, model_constructor, with_adjoint, optimizer=None, optimizer_meta_data=None):
    """
    :param model: An instance of class lib.models.CustomModel
    """
    self.sampling_operator = None
    self.model = None
    self.model_constructor = model_constructor
    self.with_adjoint = with_adjoint
    self.training_meta_data = None
    self.n = None
    self.optimizer = optimizer
    self.optimizer_meta_data = optimizer_meta_data

  def train(self, input_folder_name, target_folder_name, epochs=1, batch_size=1):
    """ Trains the model of the instance of this class with the given data set. 
    
    :param input_folder_name: The name of the folder containing the input for the training
    :param target_folder_name: The name of the folder containing the targets for the training
    :param epochs: Number of epochs of training.
    :param batch_size: The batch size during training. If None it defaults to model's default.
    """
    # load data set
    x_train_file_name = join(input_folder_name, "measurements_train.npy")
    y_train_file_name = join(target_folder_name, "images_train.npy")
    x_train = np.load(x_train_file_name)
    y_train = np.load(y_train_file_name)

    # load meta data
    meta_data_file_name = join(input_folder_name, "meta_data.json")
    with open(meta_data_file_name, "r") as file:
      data_set_meta_data = json.load(file)

    if self.with_adjoint == False:
      self.n = data_set_meta_data["Images"]["n"]
      m = data_set_meta_data["Sampling operator"]["Sampling pattern"]["Number of samples"]
      if self.optimizer is not None:
        print("Warning: Custom optimizer is not implemented yet.")
      self.model = self.model_constructor(m, self.n, 1)

      print(f"Starting training with batch size: {batch_size}, epochs: {epochs}.")
      training_start = time.time()
      x_train = x_train.reshape(x_train.shape + (1,))
      y_train = y_train.reshape(y_train.shape + (1,))
      history = self.model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size)
      training_end = time.time()

    else:
      # load sampling operator
      self.n = data_set_meta_data["Images"]["n"]
      sampling_pattern_file_name = data_set_meta_data["Sampling operator"]["Sampling pattern"]["File name"]
      sampling_pattern = MatLabSamplingPattern(sampling_pattern_file_name)
      sampling_operator_name = data_set_meta_data["Sampling operator"]["Operator type"]
      self.sampling_operator = None
      if sampling_operator_name == "P o F":
        self.sampling_operator = SamplingOperatorFourier(sampling_pattern)

        # format data
        noisy_images = self.sampling_operator.adjoint_operator(x_train)
        noisy_images_real = noisy_images.real.reshape(noisy_images.shape + (1,))
        noisy_images_imag = noisy_images.imag.reshape(noisy_images.shape + (1,))
        noisy_images = np.concatenate([noisy_images_real, noisy_images_imag], axis=-1)

        y_train_real = y_train.reshape(y_train.shape + (1,))
        y_train_imag = np.zeros_like(y_train_real)
        y_train = np.concatenate([y_train_real, y_train_imag], axis=-1)

      elif sampling_operator_name == "P o G":
        matrix_file_name = data_set_meta_data["Sampling operator"]["Matrix file name"]
        self.sampling_operator = SamplingOperatorGaussian(matrix_file_name, sampling_pattern)

        # format data
        noisy_images = self.sampling_operator.adjoint_operator(x_train)
        noisy_images = self.matlab_reshape(noisy_images, (self.n, self.n, 1))

        y_train = y_train.reshape(y_train.shape + (1,))
      elif sampling_operator_name == "P o H":
        matrix_file_name = data_set_meta_data["Sampling operator"]["Matrix file name"]
        self.sampling_operator = SamplingOperatorHadamard(matrix_file_name, sampling_pattern)

        # format data
        noisy_images = self.sampling_operator.adjoint_operator(x_train)
        noisy_images = self.matlab_reshape(noisy_images, (self.n, self.n, 1))

        y_train = y_train.reshape(y_train.shape + (1,))
      elif sampling_operator_name == "P o H0H^T":
        matrix_file_name = data_set_meta_data["Sampling operator"]["Matrix file name"]
        self.sampling_operator = SamplingOperatorHadamardTensor(matrix_file_name, sampling_pattern)

        # format data
        noisy_images = self.sampling_operator.adjoint_operator(x_train)
        noisy_images = noisy_images.reshape(noisy_images.shape + (1,)) #self.matlab_reshape(noisy_images, (self.n, self.n, 1))

        y_train = y_train.reshape(y_train.shape + (1,))
      else:
        raise ValueError(f"Unknown sampling operator: '{sampling_operator_name}'.")

      # free up memory
      del x_train

      # train
      train_k, m, n, k = noisy_images.shape

      self.model = self.model_constructor(m, n, k, optimizer=self.optimizer, optimizer_meta_data=self.optimizer_meta_data)
      print(f"Starting training with batch size: {batch_size}, epochs: {epochs}.")
      if self.optimizer is None:
        print(f"Optimizer is default. (probably SGD)")
      else:
        print(f"Optimizer: {self.optimizer_meta_data}")
      training_start = time.time()
      history = self.model.fit(noisy_images, y_train, epochs=epochs, batch_size=batch_size)
      training_end = time.time()

    self.model.training_meta_data = {"Training start": time.ctime(training_start),
                                     "Training end": time.ctime(training_end),
                                     "Total training time": self.convert_time(training_end - training_start),
                                     "Epochs": epochs,
                                     "Batch size": batch_size,
                                     "Training data set": data_set_meta_data,
                                     "Training losses": history.history}

  def matlab_reshape(self, x, shape):
    """ Wrapper for numpy's reshape. Reshapes according to matlab matrix indexing. """
    x_new = np.zeros((x.shape[0],) + shape, dtype=np.float32)
    for i, datapoint in enumerate(x):
      x_new[i] = datapoint.reshape(shape, order=self.__class__.MATLAB_MATRIX_INDEXING)

    return x_new

  def save(self, path):
    """ Saves the model of the instance of this class to path. """
    self.model.save(path)

  def convert_time(self, seconds):
    seconds = int(seconds)
    seconds_in_minute = 60
    seconds_in_hour = seconds_in_minute*60
    seconds_in_day = 24*seconds_in_hour
    days = seconds//seconds_in_day
    seconds -= days*seconds_in_day
    hours = seconds//seconds_in_hour
    seconds -= hours*seconds_in_hour
    formatted_time = f"{days} days {hours} hours and {seconds} seconds."

    return formatted_time

