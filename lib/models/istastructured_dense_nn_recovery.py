from recovery import Recovery
import keras
from lib.models.activations import ProximalActivation, IdentityActivation
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers.core import Lambda
import numpy as np

class ISTARecovery(Recovery):
  """ This class sets up a neural network with the same structure and activations as the ISTA algorithm set up as a
  neural net. The net can then be trained to see if it learns the modality we used to simulate the measurements. We can
  of course also just see how it performs when it comes to signal recovery.
  """

  def get_activation_layer(self):
    # set up activation function
    lmb = .0001
    t = .5
    alpha = lmb * t
    activation = ProximalActivation(alpha)
    # activation = IdentityActivation()
    activation = Lambda(activation.evaluate)
    return activation

  def set_up_model(self):
    # settting up proximal operator as activation function
    activation = self.get_activation_layer()

    # setting up W_0
    y = keras.Input(shape=(self.m,))
    x_ = keras.layers.Dense(self.N, use_bias=False)(y)

    # setting up remaining layers corresponding to one iteration of ISTA
    for _ in range(self.num_layers):
      conc = keras.layers.Concatenate(axis=1)([x_, y])
      x_ = keras.layers.Dense(self.N, use_bias=False, activation=activation)(conc)

    # instantiating the model
    self.model = keras.models.Model(inputs=y, outputs=x_)
    self.model.compile(optimizer="RMSprop", loss="mse")

    # save a graphical presentation of the model
    plot_model(self.model, "data_generation/sparse_rec.png")


  def main(self):
    # split data_generation into training and validation data_generation
    measurements_train, signals_train, measurements_test, signals_test = self.get_train_test_data(.15)
    # set up model
    self.set_up_model()
    # train model
    self.model.fit(measurements_train, signals_train, epochs=100)

    # evaluate model
    test_result = self.model.evaluate(measurements_test, signals_test)

    print(f"Test result: {test_result}")

    # visualize
    import matplotlib.pyplot as plt
    print(np.shape(measurements_test))
    pred = self.model.predict(measurements_test)

    x_axis = np.linspace(0, self.N, self.N, endpoint=False)
    plt.plot(x_axis, pred[0], "--", x_axis, signals_test[0])
    plt.show()

if __name__ == "__main__":
  sr = ISTARecovery("./data_generation/signals.npy", "./data_generation/measurements.npy")
  sr.main()
