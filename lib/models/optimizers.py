from tensorflow.keras.optimizers import SGD, RMSprop, Adagrad
from tensorflow.keras.optimizers.schedules import ExponentialDecay
import numpy as np

def get_default_SGD(opt_mom=.99, clipvalue=.01, dec_alpha=False):
  optimizer_meta_data = {"Name": "SGD",
                         "Opt mom": opt_mom,
                         "Clip value": clipvalue}
  if dec_alpha:
    init_lr = .1
    decay_steps = 100
    decay_rate = np.exp(np.log(.1) / 15)
    schedule = ExponentialDecay(init_lr, decay_steps, decay_rate)
    optimizer = SGD(momentum=opt_mom, clipvalue=clipvalue, learning_rate=schedule)
    optimizer_meta_data["Decay"] = "exponential"
  else:
    optimizer = SGD(momentum=opt_mom, clipvalue=clipvalue)
    optimizer_meta_data["Decay"] = "possibly 0"

  return optimizer, optimizer_meta_data

def get_default_RMSprop():
  """ Returns a keras implemented rmsprop optimizer. """
  optimizer = RMSprop()

  optimizer_meta_data = {"Name": "RMSprop"}

  return optimizer, optimizer_meta_data

def get_default_Adagrad():
  """ Returns a keras implemented adagrad optimizer. """
  optimizer = Adagrad()

  optimizer_meta_data = {"Name": "Adagrad"}
  return optimizer, optimizer_meta_data

