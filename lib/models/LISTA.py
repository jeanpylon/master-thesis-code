from tensorflow.keras.layers import Input, ReLU, concatenate, Add, Dense, Activation
from tensorflow import constant
from tensorflow.math import multiply
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.optimizers.schedules import ExponentialDecay
from lib.models.custom_model import CustomModel
import numpy as np
from tensorflow.keras import backend as K
from tensorflow

def soft_thresholding(x):
  """
  Not implemented yet.
  """
  alpha = .01

  return multiply(K.max(K.concatenate([K.abs(x) - alpha], axis=-1), axis=-1), K.sign(x))


def setup_LISTA(m, n):
  """
  m: input shape
  n: output dims
  """

  N = n**2

  # Input layer
  b = Input(shape=(m, 1))
  x_0 = K.zeros((N, 1))

  # Add layer k
  W_1 = Dense(N)(b)
  W_2 = Dense(N)(x_0)

  x_temp = Add()([b, x_0])
  x_1 = Activation(soft_thresholding, name="soft_thresholding")(x_temp)

  model = CustomModel(inputs=b, outputs=x_1)

  return model

if __name__ == "__main__":
  model = setup_LISTA(10, 100)


