from tensorflow.keras.models import Model, load_model
from os.path import join
import json

class CustomModel(Model):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.model_meta_data = None
    self.training_meta_data = None

  def save(self, path):
    """ Save model to given path. """
    # save model
    super().save(path)
    print(f"Model successfully saved to folder '{path}'.")

    # save meta data
    meta_data = {"Neural Net": self.model_meta_data,
                 "Training": self.training_meta_data}
    meta_data_file_name = join(path, "meta_data.json")
    with open(meta_data_file_name, "w") as file:
      json.dump(meta_data, file, indent=2)
    print(f"Model meta data successfully saved to file '{meta_data_file_name}'.")


def load_model(path):
  """ Returns the CustomModel stored at the given path.  """
  # load model
  model = load_model(path)

  # load meta data
  meta_data_file_name = join(path, "meta_data.json")
  with open(meta_data_file_name, "r") as file:
    meta_data = json.load(file)
  print(f"Model meta data successfully loaded from file '{meta_data_file_name}'.")

  return model, meta_data

