from tensorflow.keras.layers import Conv2D, Input, ReLU, Dense, Activation, Subtract, Reshape, Conv2DTranspose
from tensorflow.keras import Sequential, Model
from tensorflow.keras.optimizers import RMSprop
from lib.models.custom_model import CustomModel

def setup_AUTOMAP(m, n, k):
  """
  :param m: number of measurements, input
  :param n: image resolution, output is an n by n image
  :param k: number of channels in measurements and output image (1 for real input, 2 for complex input)
  """

  # architecture
  y = Input(shape=(m, k))

  r = Reshape((m*k,))(y)

  x_0 = Dense(k*n ** 2, activation="tanh")(r)

  x_1 = Dense(n ** 2, activation="tanh")(x_0)

  x_2 = Reshape((n, n, 1))(x_1)

  x_3 = Conv2D(64, (5,5), strides=(1,1), padding="same", activation="relu")(x_2)

  x_4 = Conv2D(64, (5,5), strides=(1,1), padding="same", activation="relu")(x_3)

  x_5 = Conv2DTranspose(1, (7, 7), strides=(1, 1), padding="same")(x_4)

  model = CustomModel(inputs=y, outputs=x_5)

  # optimizer
  learning_rate = 2*1e-4
  momentum = 0
  epsilon = 1e-4

  optimizer = RMSprop(learning_rate=learning_rate, momentum=momentum, epsilon=epsilon)

  # training
  batch_size = 100
  loss = "MSE"
  model.compile(optimizer=optimizer, loss=loss)

  meta_data = {"ModelType": "AUTOMAP",
               "Optimizer": "RMSprop",
               "Loss": loss,
               "Learning rate": learning_rate,
               "Momentum": momentum,
               "Epsilon (probably lambda)": epsilon}

  return model

if __name__ == "__main__":
  model = setup_AUTOMAP(2, 10, 2)
  model.summary()

