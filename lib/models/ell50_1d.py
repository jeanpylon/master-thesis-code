from tensorflow.keras.layers import Activation, Conv1D, Input, BatchNormalization, LeakyReLU, Conv2DTranspose, ReLU, concatenate, Add, MaxPooling1D, UpSampling1D
from tensorflow.keras.activations import tanh
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.models import Sequential, Model
import tensorflow.keras.backend as K

# I use the default image_data_format which is set in .keras/keras.json to channels_last
"""
def Conv1DTranspose(input_tensor, filters, kernel_size, strides=2, padding='same'):
    x = (lambda x: K.expand_dims(x, axis=2))(input_tensor)
    print(x.shape)
    x = Conv2DTranspose(filters=filters, kernel_size=(kernel_size, 1), strides=(strides, 1), padding=padding)(x)
    print(x.shape)
    x = (lambda x: K.squeeze(x, axis=2))(x)
    print(x.shape)
    return x
"""

def setup_ELL50(N, opt_mom=.99, norm_ax=-1, channels=8, l_relu_alpha=.2):
  """

  Note that N must be dividable by 16, because of the 4 max pooling operations of size 2.

  :param N: Length of input vector of the net (1-tensor of length N)
  :param opt_mom: SGD parameter
  :param norm_ax: axis to do batch normalization over
  """

  ### ROW ONE
  # Input
  x_1 = Input(shape=(N, 1))

  # Layer 2
  v = Conv1D(channels, 3, strides=1, padding="same")(x_1)
  v = BatchNormalization(axis=norm_ax)(v)
  x_2 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 3
  v = Conv1D(channels, 3, strides=1, padding="same")(x_2)
  v = BatchNormalization(axis=norm_ax)(v)
  x_3 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 4
  v = Conv1D(channels, 3, strides=1, padding="same")(x_3)
  v = BatchNormalization(axis=norm_ax)(v)
  x_4 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 5
  x_5 = MaxPooling1D(pool_size=2)(x_4)


  ### ROW TWO
  # Layer 6
  v = Conv1D(channels*2, 3, strides=1, padding="same")(x_5)
  v = BatchNormalization(axis=norm_ax)(v)
  x_6 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 7
  v = Conv1D(channels*2, 3, strides=1, padding="same")(x_6)
  v = BatchNormalization(axis=norm_ax)(v)
  x_7 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 8
  x_8 = MaxPooling1D(pool_size=2)(x_7)

  ### ROW THREE
  # Layer 9
  v = Conv1D(channels*4, 3, strides=1, padding="same")(x_8)
  v = BatchNormalization(axis=norm_ax)(v)
  x_9 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 10
  v = Conv1D(channels*4, 3, strides=1, padding="same")(x_9)
  v = BatchNormalization(axis=norm_ax)(v)
  x_10 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 11
  x_11 = MaxPooling1D(pool_size=2)(x_10)

  ### ROW FOUR (left side)
  # Layer 12
  v = Conv1D(channels*8, 3, strides=1, padding="same")(x_11)
  v = BatchNormalization(axis=norm_ax)(v)
  x_12 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 13
  v = Conv1D(channels*8, 3, strides=1, padding="same")(x_12)
  v = BatchNormalization(axis=norm_ax)(v)
  x_13 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 14
  x_14 = MaxPooling1D(pool_size=2)(x_13)

  ### ROW FIVE
  # Layer 15
  v = Conv1D(channels*16, 3, strides=1, padding="same")(x_14)
  v = BatchNormalization(axis=norm_ax)(v)
  x_15 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 16
  v = Conv1D(channels*16, 3, strides=1, padding="same")(x_15)
  v = BatchNormalization(axis=norm_ax)(v)
  x_16 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 17
  #v = Conv1DTranspose(x_16, channels*16, 3, strides=2, padding="same")
  v = UpSampling1D(size=2)(x_16)
  v = Conv1D(channels*16, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_17 = LeakyReLU(alpha=l_relu_alpha)(v)

  ### ROW FOUR (right side)
  # Layer 18
  v = concatenate([x_13, x_17], axis=2)
  v = Conv1D(channels*8, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_18 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 19
  v = Conv1D(channels*8, 3, strides=1, padding="same")(x_18)
  v = BatchNormalization(axis=norm_ax)(v)
  x_19 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 20
  #v = Conv1DTranspose(x_19, channels*8, (3, 3), strides=2, padding="same")
  v = UpSampling1D(size=2)(x_19)
  v = Conv1D(channels*8, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_20 = LeakyReLU(alpha=l_relu_alpha)(v)

  ### ROW THREE (right side)
  # Layer 21
  v = concatenate([x_10, x_20])
  v = Conv1D(channels*4, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_21 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 22
  v = Conv1D(channels*4, 3, strides=1, padding="same")(x_21)
  v = BatchNormalization(axis=norm_ax)(v)
  x_22 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 23
  #v = Conv1DTranspose(channels*4, (3, 3), strides=2, padding="same")(x_22)
  v = UpSampling1D(size=2)(x_22)
  v = Conv1D(channels*4, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_23 = LeakyReLU(alpha=l_relu_alpha)(v)


  ### ROW TWO (right side)
  # Layer 24
  v = concatenate([x_7, x_23])
  v = Conv1D(channels*2, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_24 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 25
  v = Conv1D(channels*2, 3, strides=1, padding="same")(x_24)
  v = BatchNormalization(axis=norm_ax)(v)
  x_25 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 26
  #v = Conv1DTranspose(channels*2, (3, 3),strides=2, padding="same")(x_25)
  v = UpSampling1D(size=2)(x_25)
  v = Conv1D(channels*2, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_26 = LeakyReLU(alpha=l_relu_alpha)(v)

  ### ROW ONE (right side)
  # Layer 27
  v = concatenate([x_4, x_26])
  v = Conv1D(channels, 3, strides=1, padding="same")(v)
  v = BatchNormalization(axis=norm_ax)(v)
  x_27 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 28
  v = Conv1D(channels, 3, strides=1, padding="same")(x_27)
  v = BatchNormalization(axis=norm_ax)(v)
  x_28 = LeakyReLU(alpha=l_relu_alpha)(v)

  # Layer 29
  x_29 = Conv1D(1, 1, strides=1)(x_28)

  # Layer 30
  x_30 = Add()([x_29, x_1])

  model = Model(inputs=x_1, outputs=x_30)
  optimizer = SGD(momentum=opt_mom)
  model.compile(optimizer=optimizer, loss="mse")

  return model





