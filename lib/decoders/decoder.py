from abc import ABC, abstractmethod

class Decoder(ABC):
  """ Abstract base class for decoders. """

  @abstractmethod
  def decode(self, Y):
    """ Implements the decoder.

    :param Y: The measurements as rows in a 2D numpy array with dimensions (batch size, m)
    :returns: the reconstructed images as a 3D numpy array with dimensions (batch size, n, n)
    """
    pass

  @property
  @abstractmethod
  def meta_data(self):
    """ The meta data of this decoder. """
    pass
