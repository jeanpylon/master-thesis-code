from lib.decoders.decoder import Decoder
import numpy as np
from tensorflow.keras.models import load_model
from os.path import join
import json

class DecoderAdjointNeuralNet(Decoder):
  MATLAB_MATRIX_INDEXING = "F"

  def __init__(self, sampling_operator, neural_net_folder, parallel=False):
    """
    :param sampling_operator: A class that implements the SamplingOperator interface. (We use the adjoint of this.)
    :param neural_net_folder: The path to the keras neural net.
    :param parallel: If true, the class computes reconstructions using numpy vectorization. If false, reconstructions
                     are computed using a for loop.
    """
    self.sampling_operator = sampling_operator
    self.neural_net, self.neural_net_meta_data = self.load_neural_net(neural_net_folder)
    self.parallel = parallel
    self.n = sampling_operator.n

  def load_neural_net(self, path):
    """ Loads the neural net at path. """
    # load model
    model = load_model(path)

    # load meta data
    meta_data_file_name = join(path, "meta_data.json")
    with open(meta_data_file_name, "r") as file:
      meta_data = json.load(file)

    return (model, meta_data)

  def decode(self, Y):
    if self.parallel:
      return self.decode_parallel(Y)
    else:
      return self.decode_serial(Y)

  def decode_parallel(self, Y):
    """ Decodes using numpy vectorization. """
    raise NotImplementedError()

  def decode_serial(self, Y):
    """ Decodes using a for loop iterating through each data point."""
    if self.sampling_operator.meta_data["Sampling operator"]["Operator type"] == "P o F":
      k = len(Y)
      recovered_images = np.zeros((k, self.n, self.n), dtype=np.complex128)
      for i in range(k):
        print(f"{i+1} / {k}")
        noisy_image = self.sampling_operator.adjoint_operator(Y[i:i + 1])
        noisy_img_real = noisy_image.real.reshape(noisy_image.real.shape + (1,))
        noisy_img_imag = noisy_image.imag.reshape(noisy_image.imag.shape + (1,))

        noisy_image = np.concatenate([noisy_img_real, noisy_img_imag], axis=-1)

        recovered_image = self.neural_net.predict(noisy_image)[0]
        recovered_images[i].real = recovered_image[:, :, 0]
        recovered_images[i].imag = recovered_image[:, :, 1]
    elif self.sampling_operator.meta_data["Sampling operator"]["Operator type"] == "P o G" \
            or self.sampling_operator.meta_data["Sampling operator"]["Operator type"] == "P o H":
      k = len(Y)
      recovered_images = np.zeros((k, self.n, self.n))
      for i in range(k):
        # format data
        noisy_image = self.sampling_operator.adjoint_operator(Y[i:i + 1])
        noisy_image = self.matlab_reshape(noisy_image, (self.n, self.n, 1))
        temp = self.neural_net.predict(noisy_image)
        recovered_images[i] = temp[0,:,:,0]
    elif self.sampling_operator.meta_data["Sampling operator"]["Operator type"] == "P o H0H^T":
      k = len(Y)
      recovered_images = np.zeros((k, self.n, self.n))
      for i in range(k):
        # format data
        noisy_image = self.sampling_operator.adjoint_operator(Y[i:i + 1])
        noisy_image = noisy_image.reshape(noisy_image.shape + (1,))
        temp = self.neural_net.predict(noisy_image)
        recovered_images[i] = temp[0, :, :, 0]
    else:
      raise ValueError(f"Unknown sampling operator: {self.sampling_operator.meta_data['Sampling operator']['Operator type']}.")

    return recovered_images

  @property
  def meta_data(self):
    """ Returns meta data about this instance of Decoder. """
    decoder_meta_data = {"Operator whose adjoint is used": self.sampling_operator.meta_data,
                         "Neural net": self.neural_net_meta_data}
    meta_data = {"Decoder": decoder_meta_data}

    return meta_data

  def concatenate_abs(self, real, img):
    """ Concatenate and modulus composed. """
    return np.sqrt(real**2 + img**2)

  def matlab_reshape(self, x, shape):
    """ Wrapper for numpy's reshape. Reshapes according to matlab matrix indexing. """
    x_new = np.zeros((x.shape[0],) + shape)
    for i, datapoint in enumerate(x):
      x_new[i] = datapoint.reshape(shape, order=self.__class__.MATLAB_MATRIX_INDEXING)

    return x_new

