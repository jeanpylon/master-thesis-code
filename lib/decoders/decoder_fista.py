from lib.decoders.decoder import Decoder
from lib.algorithms.fista import FISTA
import numpy as np


class DecoderFISTA(Decoder):
  MATLAB_MATRIX_INDEXING = "F"

  def __init__(self, sampling_operator, iterations=1000, lmb=.001, parallel=True):
    self.parallel = parallel
    self.sampling_operator = sampling_operator
    self.fista = FISTA()
    self.iterations = iterations
    self.lmb = lmb

  def matlab_reshape(self, x, shape):
    """ Wrapper for numpy's reshape. Reshapes according to matlab matrix indexing. """
    x_new = np.zeros((x.shape[0],) + shape, dtype=np.complex128)
    for i, datapoint in enumerate(x):
      x_new[i] = datapoint.reshape(shape, order=self.__class__.MATLAB_MATRIX_INDEXING)

    return x_new

  def decode(self, Y):
    if self.parallel:
      return self.decode_parallel(Y)
    else:
      return self.decode_serial(Y)

  def decode_serial(self, Y):
    raise NotImplementedError()

  def decode_parallel(self, Y):
    n = self.sampling_operator.n
    if self.sampling_operator.sparsifying_transform:
      recovered_coefficients = self.fista.recover(Y, self.sampling_operator, iterations=self.iterations)
      recovered_images = self.sampling_operator.sparsifying_synthesize(recovered_coefficients)
    else:
      recovered_images = self.fista.recover(Y, self.sampling_operator, iterations=self.iterations)
      recovered_images = self.matlab_reshape(recovered_images, (n, n))

    return recovered_images

  @property
  def meta_data(self):
    decoder_meta_data = {"Decoder": "FISTA",
                         "Number of iterations": self.iterations,
                         "Lambda": self.lmb,
                         "Sampling operator": self.sampling_operator.meta_data}
    meta_data = {"Decoder meta data": decoder_meta_data}

    return meta_data
