from lib.decoders.decoder import Decoder
import numpy as np

class DecoderAdjoint(Decoder):
  def __init__(self, sampling_operator, parallel=False):
    """
    :param sampling_operator: A class that implements the SamplingOperator interface. (We use the adjoint of this.)
    :param n: The length of the sides in the images.
    :param parallel: If true, the class computes reconstructions using numpy vectorization. If false, reconstructions
                     are computed using a for loop.
    """
    self.sampling_operator = sampling_operator
    self.parallel = parallel
    self.n = sampling_operator.n

  def decode(self, Y):
    if self.parallel:
      return self.decode_parallel(Y)
    else:
      return self.decode_serial(Y)

  def decode_parallel(self, Y):
    """ Decodes using numpy vectorization. """
    raise NotImplementedError()

  def decode_serial(self, Y):
    """ Decodes using a for loop iterating through each data point."""
    k = len(Y)
    recovered_images = np.zeros((k, self.n, self.n), dtype=np.complex128)
    for i in range(k):
      print(f"{i+1} / {k}")
      recovered_images[i] = self.sampling_operator.adjoint_operator(Y[i:i + 1])

    return recovered_images

  @property
  def meta_data(self):
    """ Returns meta data about this instance of Decoder. """
    decoder_meta_data = {"Operator whose adjoint is used": self.sampling_operator.meta_data}
    meta_data = {"Decoder": decoder_meta_data}

    return meta_data

