import numpy as np
from os.path import join, isdir
import json
from os import mkdir

class MeasurementSimulator:
  def __init__(self, sampling_operator, dtype=np.float32):
    # Image attributes
    self.images_train = None
    self.images_test = None
    self.meta_data = None # Meta data dictionary. Contains meta data only on images so far.
    self.image_folder = None
    self._images_dim = None

    # Measurement attributes
    self.measurements_train = None
    self.measurements_test = None
    self.dtype = dtype

    # Sampling operator attributes
    self.sampling_operator = sampling_operator

  def load_image_set(self, folder):
    """ Loads the image set located in the given folder. """
    images_train_filename = join(folder, "images_train.npy")
    images_test_filename = join(folder, "images_test.npy")
    meta_data_filename = join(folder, "meta_data.json")

    # Loading
    self.images_train = np.load(images_train_filename)
    print(f"Training images successfully loaded from file '{images_train_filename}'. Shape {self.images_train.shape}.")
    self.images_test = np.load(images_test_filename)
    print(f"Testing images successfully loaded from file '{images_test_filename}'. Shape {self.images_test.shape}.")
    with open(meta_data_filename, "r") as file:
      self.meta_data = json.load(file)
      print(f"Meta data successfully loaded from file '{meta_data_filename}'.")
    self.image_folder = folder

    # verification of shapes
    if len(self.images_train.shape) == 2:
      if self.images_train.shape[1] != self.images_test.shape[1]:
        raise ValueError(f"Training images and test images have unequal second dimensions.")
      self._images_dim = 1
    elif len(self.images_train.shape) == 3:
      if self.images_train.shape[1] != self.images_test.shape[1]:
        raise ValueError(f"Training images and test images have unequal second dimensions.")
      if self.images_train.shape[2] != self.images_test.shape[2]:
        raise ValueError(f"Training images and test images have unequal third dimensions.")
      if self.images_train.shape[1] != self.images_train.shape[2] or self.images_test.shape[1] != self.images_test.shape[2]:
        print(f"WARNING: Images are not square.")
      self._images_dim = 2
    else:
      raise ValueError(f"Unknown shape length: {self.images_train.shape}.")

    if self._images_dim != self.sampling_operator.input_dim:
      raise ValueError("Images dim is not the same as sampling operator input dim.")

  def simulate_measurements(self):
    """ Simulates training and testing measurements from the given training and test image sets with the given sampling operator. """
    if self.images_train is None:
      print("Error: Training images not loaded. Simulation aborted.")
      return
    if self.images_test is None:
      print("Error: Testing images not loaded. Simulation aborted.")
      return
    if self.sampling_operator is None:
      print("Error: Sampling operator not specified. Simulation aborted.")
      return

    self.measurements_train = self.sampling_operator.operator(self.images_train)
    self.measurements_test = self.sampling_operator.operator(self.images_test)

  def make_measurements_folder(self):
    index = 1
    measurements_folder = f"{join(self.image_folder, 'measurements')}_{index}"

    while isdir(measurements_folder):
      index += 1
      measurements_folder = f"{join(self.image_folder, 'measurements')}_{index}"

    mkdir(measurements_folder)

    return measurements_folder

  def save_measurements(self):
    """ Saves the measurements to a sub folder of the image set folder. """
    if self.measurements_train is None:
      print("Error: No training measurements simulated. Save aborted.")
      return
    if self.measurements_test is None:
      print("Error: No testing measurements simulated. Save aborted.")
      return
    if self.image_folder is None:
      print("Error: No image folder specified. Save aborted.")
      return

    # Create folder
    measurements_folder = self.make_measurements_folder()

    # Save measurements to measurements folder
    measurements_train_file_name = join(measurements_folder, "measurements_train")
    measurements_test_file_name = join(measurements_folder, "measurements_test")
    np.save(measurements_train_file_name, self.measurements_train.astype(self.dtype))
    print(f"Training measurements with shape {self.measurements_train.shape} successfully saved to file '{measurements_train_file_name}'.")
    np.save(measurements_test_file_name, self.measurements_test.astype(self.dtype))
    print(f"Testing measurements with shape {self.measurements_test.shape} successfully saved to file '{measurements_test_file_name}'.")

    # Save sampling pattern to measurements folder
    sampling_pattern_file_name = join(measurements_folder, "sampling_pattern.mat")
    self.sampling_operator.copy_sampling_pattern(sampling_pattern_file_name)
    print(f"Sampling pattern with shape {self.sampling_operator.sampling_pattern.shape} successfully saved to file '{sampling_pattern_file_name}'.")

    # Save meta data to measurements folder
    meta_data_file_name = join(measurements_folder, "meta_data.json")
    self.meta_data.update(self.sampling_operator.meta_data)
    self.meta_data["Sampling operator"]["Sampling pattern"]["File name"] = sampling_pattern_file_name
    measurements_meta_data = {}
    measurements_meta_data["Training measurements path"] = measurements_train_file_name + ".npy"
    measurements_meta_data["Training measurements data type"] = str(self.dtype)
    measurements_meta_data["Test measurements path"] = measurements_test_file_name + ".npy"
    measurements_meta_data["Test measurements data type"] = str(self.dtype)
    self.meta_data["Measurements"] = measurements_meta_data
    with open(meta_data_file_name, "w") as file:
      json.dump(self.meta_data, file, indent=2)
    print(f"Meta data successfully saved to file '{meta_data_file_name}'.")

