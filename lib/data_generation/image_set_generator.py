from os.path import join, isfile
from os import listdir
from imageio import imread
from pydicom import dcmread
import numpy as np
import json


class ImageSetGenerator:
  """ Generates image sets from .png files, .dcm files or generates at random."""
  def __init__(self):
    self.images = None
    self.train_images = None
    self.test_images = None
    self.n = None
    self.k = None # TODO: is this variable necessary?
    self.image_description = None

  #region Create data from raw data
  def load_png_images(self, folder):
    """ Loads all .png-files in given folder and stores as 3D-array.

    :param folder: name of folder with .png-images
    """
    images = []
    # Generates list of files in given folder
    files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f)) and f.split(".")[-1] == "png"]

    # TODO: fix so it check dimensions more safely
    for file in files:
      image = imread(file) / 255  # load and normalize image
      images.append(image)
      self.n, m = image.shape

    if m != self.n:
      raise ValueError("Pictures should be square")

    self.k = len(images)
    self.images = np.array(images)
    self.image_description = ".png-files"

    print(f"{self.k} .png-files of size {self.n} by {self.n} successfully loaded.")


  def load_dicom_images(self, folder):
    """ Loads all .dcm-files in given folder and stores as 3D-array.

    :param folder: name of folder with .dcm-images
    """
    images = []
    # Generates list of files in given folder
    files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f)) and f.split(".")[-1] == "dcm"]

    # TODO: fix so it check dimensions more safely
    for file in files:
      data = dcmread(file)  # load and normalize image
      images.append(data.pixel_array)
      self.n, m = data.pixel_array.shape

    if m != self.n:
      raise ValueError("Pictures should be square")

    # set k (number of images)
    self.k = len(images)

    # normalize images
    images = np.array(images)
    max_entry = images.max()
    self.images = images / max_entry
    self.image_description = ".dcm-files"

    print(f"{self.k} .dcm-files of size {self.n} by {self.n} successfully loaded.")


  def crop_images(self, new_n):
    """ Cut out the quadratic center of the image with given dimension.

    :param new_n: The length of the sides of the center cut out.
    """
    center = round(self.n/2)
    left_edge = center - round(new_n/2)
    right_edge = left_edge + new_n

    self.images = self.images[:, left_edge:right_edge, left_edge:right_edge]
    self.n = new_n

  #endregion Create data from raw data


  #region generate data
  def generate_random_dots_images(self, k, n, d, d_size, threshold=1, dtype="float64"):
    """ Generates a n by n image with pixel value equal to zero. Then adds d dots of size d_size at uniformly
        random coordinates. The value of the dots are drawn uniformly at random from the interval (threshold, 1].

    :param k: number of images o generate, integer
    :param n: length of sides of images, integer (images have area n**2)
    :param d: number of dots in images
    :param d_size: length of sides of dots (dots have area d_size**2)
    :param threshold: lower limit of the interval from where the dot values are drawn uniformly at random
    """
    if threshold > 1 or threshold < 0:
      print("Value error: Argument threshold is invalid. Threshold must be in the interval [0,1].")
      return

    # set up zero-images
    images = np.zeros((k, n, n), dtype=dtype)

    outside_counter = 0
    for l in range(k):
      # draw dot coordinates
      ind = np.random.choice(n ** 2, d, replace=False)

      # insert dots TODO: fix so that drawn index is center of dot
      for i in ind:
        x = i // n
        y = i % n
        value = (-np.random.random() + 1) * (1 - threshold) + threshold
        outside = False
        for x_r in range(x, x + d_size):
          for y_r in range(y, y + d_size):
            try:
              images[l, x_r, y_r] = value
            except IndexError as e:
              outside = True
              #print("Dot was partially outside of image. " + str(e))
        if outside:
          outside_counter += 1
          outside = False

    self.images = images
    self.k = k
    self.n = n
    self.image_description = f"Zero images with {d} randomly placed dots of size {d_size} by {d_size} with value in ({threshold},1]."

    print(f"{self.k} images of size {self.n} by {self.n} with {d} random dots successfully generated.")
    print(f"{outside_counter} dots intersected outside of their respective image.")


  def generate_random_s_sparse_signals(self, k, m, n, s, max_value=None):
    """ Sets up k random m by n, s-sparse signals and stores them.

    Generates an m by n zero-matrix. Then fills in with s random numbers between zero and max_value at random indicies.
    If max_value=None, max_value is set to self.MAX_VALUE.

    :param k: number of signals to generate
    :param m: image height
    :param n: image width
    :param s: max number of non-zero entries
    :param max_value: the maximum value of a pixel
    """
    if max_value is None:
      max_value = self.__class__.MAX_VALUE

    signals = []
    for _ in range(k):
      # set up zero-signal
      signal = np.zeros((m, n))
      # generate random indicies
      ind_1 = np.random.choice(m, size=s)
      ind_2 = np.random.choice(n, size=s)
      # generate random signal
      for i,j in zip(ind_1, ind_2):
        signal[i, j] = np.random.random()*max_value
      signals.append(signal)

    signals = np.array(signals)
    self.images = signals
    self.k = k
    self.n = n
    self.image_description = f"Zero images with {s} randomly (uniformly) placed pixels with value in [0,{self.__class__.MAX_VALUE})."

    print(f"{self.k} {s}-sparse images of size {self.n} by {self.n} successfully generated.")

  #end region generate data


  def divide_images(self, ratio=.1):
    """ Divides the data into training and test data.

    :param ratio: the fraction of the image set to become test images. Float in the interval [0,1]
    """
    if self.images is None:
      print("Unable to split data. No images.")
      return

    # divide
    k_test = round(self.k*ratio)
    self.train_images = self.images[:-k_test]
    self.test_images = self.images[-k_test:]


  def setup_meta_data(self, tag):
    meta_data = {}
    if tag is not None:
      meta_data["Tag"] = tag
    meta_data["Image description"] = self.image_description
    meta_data["Total number of images"] = self.k
    meta_data["n"] = self.n
    meta_data["Number of images for training"] = len(self.train_images)
    meta_data["Number of images for testing"] = len(self.test_images)

    return meta_data


  def save_image_set(self, save_folder, ratio=.1, tag=None):
    """ Saves the image set to the given folder in two separate files, one for training images and one for test images.
        The function saves the training and test sets as images_train.npy and images_test.npy respectively.

    :param save_folder: The folder for saving the image set
    :param ratio: The fraction of the image set to become test images. Float in the interval [0,1]
    :param tag: If NOT None, the meta data file will record this tag.
    """
    self.divide_images(ratio)

    images_train_file_name = join(save_folder, "images_train")
    images_test_file_name = join(save_folder, "images_test")
    meta_data_file_name = join(save_folder, "meta_data.json")

    np.save(images_train_file_name, self.train_images)
    print(f"{len(self.train_images)} training images successfully saved as {self.train_images.shape}-array to file '{images_train_file_name}'.")
    np.save(images_test_file_name, self.test_images)
    print(f"{len(self.test_images)} test images successfully saved as {self.test_images.shape}-array to file '{images_test_file_name}'.")

    image_meta_data = self.setup_meta_data(tag)
    image_meta_data["Training images path"] = images_train_file_name
    image_meta_data["Test images path"] = images_test_file_name
    meta_data = {"Images": image_meta_data}
    with open(meta_data_file_name, "w") as file:
      json.dump(meta_data, file, indent=2)

