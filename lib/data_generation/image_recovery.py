from os.path import join, isdir, abspath
from os import mkdir
import numpy as np
import json


class ImageRecovery:
  """ Does image recovery from the given measurements with the given decoder. """
  def __init__(self, decoder, train_set=True, test_set=True, dtype=np.float32):
    """
    :param train_set: If True, the training set is recovered.
    :param test_set: If True, the test set is recovered.
    """
    self._train_set = train_set
    self._test_set = test_set

    self.measurements_train = None
    self.measurements_test = None
    self.measurements_folder = None

    self.meta_data = None
    self.decoder = decoder

    self.recovered_images_train = None
    self.recovered_images_test = None

    self.choose_k = None
    self.dtype = dtype

  def load_data_set(self, measurements_folder):
    """ Loads the measurements and the meta data. """
    measurements_folder = abspath(measurements_folder)
    if self._train_set:
      measurements_train_file_name = join(measurements_folder, "measurements_train.npy")
      self.measurements_train = np.load(measurements_train_file_name)
      print(f"Training measurements with shape {self.measurements_train.shape} loaded successfully from file '{measurements_train_file_name}'.")

    if self._test_set:
      measurements_test_file_name = join(measurements_folder, "measurements_test.npy")
      self.measurements_test = np.load(measurements_test_file_name)
      print(f"Test measurements with shape {self.measurements_test.shape} loaded successfully from file '{measurements_test_file_name}'.")

    meta_data_file_name = join(measurements_folder, "meta_data.json")
    with open(meta_data_file_name, "r") as file:
      self.meta_data = json.load(file)
    print(f"Meta data loaded successfully from file '{meta_data_file_name}'.")

    self.measurements_folder = measurements_folder

  def recover(self, choose_k=None):
    """ Decodes the train and test measurements with the given decoder.
        :param choose_k: The number of images to recover from each of the training set and the test set.
                         The images are drawn uniformly at random.
    """
    if choose_k is None:
      if self._train_set:
        self.recovered_images_train = self.decoder.decode(self.measurements_train)
      if self._test_set:
        self.recovered_images_test = self.decoder.decode(self.measurements_test)
    else:
      if self._train_set:
        k_train = self.measurements_train.shape[0]
        indices_train = np.random.choice(k_train, choose_k, replace=False)
        self.recovered_images_train = self.decoder.decode(self.measurements_train[indices_train])
      if self._test_set:
        k_test = self.measurements_test.shape[0]
        indices_test = np.random.choice(k_test, choose_k, replace=False)
        self.recovered_images_test = self.decoder.decode(self.measurements_test[indices_test])
      self.choose_k = choose_k


  def make_recovery_folder(self):
    index = 1
    recovery_folder = f"{join(self.measurements_folder, 'recoveries')}_{index}"

    while isdir(recovery_folder):
      index += 1
      recovery_folder = f"{join(self.measurements_folder, 'recoveries')}_{index}"

    mkdir(recovery_folder)

    return recovery_folder

  def save_recoveries(self):
    recovery_folder = self.make_recovery_folder()

    # save data
    if self._train_set:
      train_recoveries_file_name = join(recovery_folder, "recoveries_train")
      np.save(train_recoveries_file_name, self.recovered_images_train.astype(dtype=self.dtype))
      print(f"Training recoveries with shape {self.recovered_images_train.shape} successfully saved to file '{train_recoveries_file_name}'.")

    if self._test_set:
      test_recoveries_file_name = join(recovery_folder, "recoveries_test")
      np.save(test_recoveries_file_name, self.recovered_images_test.astype(dtype=self.dtype))
      print(f"Test recoveries with shape {self.recovered_images_test.shape} successfully saved to file '{test_recoveries_file_name}'.")

    # write and save meta data
    self.meta_data.update(self.decoder.meta_data)
    recoveries_meta_data = {}
    if self._train_set:
      recoveries_meta_data["Training recoveries path"] = train_recoveries_file_name
      recoveries_meta_data["Training recoveries data type"] = str(self.dtype)
      if self.choose_k is None:
        recoveries_meta_data["Number of training images recovered"] = "All"
      else:
        recoveries_meta_data["Number of training images recovered (drawn uniformly at random)"] = str(self.choose_k)
    if self._test_set:
      recoveries_meta_data["Test recoveries path"] = test_recoveries_file_name
      recoveries_meta_data["Test recoveries data type"] = str(self.dtype)
      if self.choose_k is None:
        recoveries_meta_data["Number of test images recovered"] = "All"
      else:
        recoveries_meta_data["Number of test images recovered (drawn uniformly at random)"] = str(self.choose_k)
    self.meta_data["Recoveries"] = recoveries_meta_data
    meta_data_file_name = join(recovery_folder, "meta_data.json")
    with open(meta_data_file_name, "w") as file:
      json.dump(self.meta_data, file, indent=2)


### Testing ###

if __name__ == "__main__":
  n = 512
  samp_patt_path = "/home/jab/Skole/Master/data/sampling_patterns/sampling_scheme.mat"
  measurements_folder = "/home/jab/Skole/Master/data/data_sets/2D/random_dots_test/measurements_1"
  neural_net_path = "/home/jab/Skole/Master/models/transpose_ELL50_2D_epochs_1000__channels_64"

  from lib.sampling_operators.sampling_operator_fourier import SamplingOperatorFourier
  from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern
  from lib.decoders.decoder_adjoint_neural_net import DecoderAdjointNeuralNet

  decoder = DecoderAdjointNeuralNet(SamplingOperatorFourier(n, MatLabSamplingPattern(samp_patt_path)), neural_net_path, n)
  img_rec = ImageRecovery(decoder)
  img_rec.load_data_set(measurements_folder)
  print("Recovering ...")
  img_rec.recover()
  img_rec.save_recoveries()