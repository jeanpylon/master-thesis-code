import matplotlib.pyplot as plt
import numpy as np
#import tensorflow.keras.backend as K
#import tensorflow as tf

class Evaluation:
  """ Implements methods for evaluating recovery results of 1D signals. """

  MAX_SIGNAL_ENTRY = 1
  LOG_10 = np.log(10)

  @classmethod
  def plot_signal_comparison(cls, recovered_signal, original_signal):
    """ Plots the given signals in the same plot with dots for visual comparison.

    :param recovered_signal: The recovered signal as a row-vector
    :param original_signal: The original signal as a row-vector
    """
    if recovered_signal.shape != original_signal.shape:
      raise ValueError("Input has unequal dimensions.")

    N = len(recovered_signal)
    x_axis = np.linspace(0, N, N, endpoint=False)
    plt.clf()
    plt.plot(x_axis, recovered_signal, "r*", label="Recovered signal")
    plt.plot(x_axis, original_signal, "bo", label="Original signal")
    plt.legend()
    plt.show()

  @classmethod
  def plot_signal_error(cls, recovered_signal, original_signal, error_type="squared"):
    """ Plots the specified error type of each entry in the recovered signal.

    :param recovered_signal: The recovered signal as a row-vector
    :param original_signal: The original signal as a row-vector
    :param error_type: The type of error you would like to be plotted ("squared", "mse" or "difference")
    """
    # Check for inconsistency
    if recovered_signal.shape != original_signal.shape:
      raise ValueError("Input has unequal dimensions.")

    # Calculate error
    error = None
    label = None
    if error_type == "squared":
      error = (recovered_signal - original_signal)**2
      label = "Squared errors"
    elif error_type == "abs":
      error = np.abs(recovered_signal - original_signal)
      label = "Absolute errors"
    elif error_type == "difference":
      error = recovered_signal - original_signal
      label = "Differences"
    else:
      raise ValueError("Invalid argument: error_type. Must be either 'squared' or 'abs'")

    # plot error
    N = len(recovered_signal)
    x_axis = np.linspace(0, N, N, endpoint=False)
    plt.plot(x_axis, error, label=label)
    plt.legend()
    plt.show()

  @classmethod
  def plot_all_errors(cls, recovered_signals, original_signals, error_type="squared"):
    """ Plots the specified errors as a surface.
    """
    if recovered_signals.shape != original_signals.shape:
      raise ValueError("Input has unequal dimensions.")

  @classmethod
  def L_inf(cls, original_signal, recovered_signal):
    return np.max(abs(original_signal - recovered_signal), axis=(-2,-1))

  @classmethod
  def PSNR_big(cls, original_signal, recovered_signal):
    """ Computes the peak signal to noise ratio of the given signal recovery. """
    return 20*np.log10(cls.MAX_SIGNAL_ENTRY) - 10*np.log10(cls.MSE_big(original_signal, recovered_signal))

  @classmethod
  def PSNR(cls, original_signal, recovered_signal):
    """ Computes the peak signal to noise ratio of the given signal recoveries. """
    psnr = 20*np.log10(cls.MAX_SIGNAL_ENTRY) - 10*np.log10(cls.MSE(original_signal, recovered_signal))
    return psnr

  @classmethod
  def PSNR_2D(cls, original_signal, recovered_signal):
    """ Computes the peak signal to noise ratio of the given signal recoveries. """
    psnr = 20*np.log10(cls.MAX_SIGNAL_ENTRY) - 10*np.log10(cls.MSE_2D(original_signal, recovered_signal))
    return psnr

  @classmethod
  def PSNR_big_2D(cls, original_signal, recovered_signal):
    """ Computes the peak signal to noise ratio of the given signal recovery. """
    return 20*np.log10(cls.MAX_SIGNAL_ENTRY) - 10*np.log10(cls.MSE_big_2D(original_signal, recovered_signal))

  @classmethod
  def psnr_K(cls, original_signal, recovered_signal):
    """ Expcets one datapoint as input. (Batchsize == 1.)

    NB: No vectorization implemented.

    :return: A single tensor value """
    mse = K.mean((original_signal - recovered_signal) ** 2) / tf.size(original_signal)
    psnr = 20 * K.log(cls.MAX_SIGNAL_ENTRY) / cls.LOG_10 - 10 * K.log(mse) / cls.LOG_10

    return psnr

  @classmethod
  def MSE_big(cls, original_signal, recovered_signal):
    """ Computes the mean squared error of the given recovery of the given signal.

    NOTE: This function treats all the signals as one big signal

    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if original_signal.shape != recovered_signal.shape:
      raise ValueError("Recovered and original signal have different shapes.")
    k, N = original_signal.shape
    return np.sum((original_signal - recovered_signal)**2)/(k*N)

  @classmethod
  def MSE_big_2D(cls, original_signal, recovered_signal):
    """ Computes the mean squared error of the given recovery of the given signal.

    NOTE: This function treats all the signals as one big signal

    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if original_signal.shape != recovered_signal.shape:
      raise ValueError("Recovered and original signal have different shapes.")
    k, m, n = original_signal.shape
    return np.sum((original_signal - recovered_signal)**2)/(k*m*n)

  @classmethod
  def MSE(cls, original_signals, recovered_signals):
    """ Computes the mean squared error of the given recoveries of the given signals.

    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if original_signals.shape != recovered_signals.shape:
      raise ValueError("Recovered and original signal have different shapes.")
    k, N = original_signals.shape
    mse = np.sum((original_signals - recovered_signals) ** 2, axis=1) / N
    return mse

  @classmethod
  def sup_norm_2D(cls, original_signals, recovered_signals):
    """ Computes the distances between the given images and recoveries in the sup_norm.

    :param original_signals: The original images in a 3-tensor with rows as images
    :param recovered_signals: The recovered images in a 3-tensor with rows as images
    """
    if original_signals.shape != recovered_signals.shape:
      raise ValueError("Recovered and original signal have different shapes.")
    sup_norms = (original_signals - recovered_signals).max(axis=(-2,-1))

    return sup_norms

  @classmethod
  def MSE_2D(cls, original_signals, recovered_signals):
    """ Computes the mean squared error of the given recoveries of the given images.

    :param original_signals: The original images in a 3-tensor with rows as images
    :param recovered_signals: The recovered images in a m3-tensor with rows as images
    """
    if original_signals.shape != recovered_signals.shape:
      raise ValueError("Recovered and original signal have different shapes.")
    k, m, n = original_signals.shape
    mse = np.sum((original_signals - recovered_signals) ** 2, axis=(1,2))/(m*n)

    return mse

  @classmethod
  def plot_12_random(cls, original_signals, recovered_signals):
    """ Plots 12 random signals together with it's recovery in a grid.

    Expects signals to be rows in matrices.

    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if recovered_signals.shape != original_signals.shape:
      raise ValueError("Input has unequal dimensions.")
    # hyper parameters
    cols = 4 # no of plots in column
    rows = 3 # no of plots in row
    r = cols*rows # no of plots
    k, N = recovered_signals.shape

    # randomly choose k signals to plot
    indecies = np.random.choice(k, r)

    # plot
    plt.clf()
    fig, axs = plt.subplots(ncols=cols, nrows=rows)
    x_axis = np.linspace(0, N, N, endpoint=False)
    for i in range(rows):
      for j in range(cols):
        axs[i, j].plot(x_axis, recovered_signals[indecies[i*cols + j]], "r*", label="Recovered signal")
        axs[i, j].plot(x_axis, original_signals[indecies[i*cols + j]], "bo", label="Original signal")
        org = original_signals[indecies[i*cols + j]:indecies[i*cols + j] + 1]
        rec = recovered_signals[indecies[i*cols + j]:indecies[i*cols + j] + 1]
        axs[i, j].set_title(f"PSNR:{cls.PSNR_big(org, rec)}")

    fig.suptitle("12 recovered signals (red=recovery, blue=original)")
    plt.show()

  @classmethod
  def k_random_PSNR(cls, k, original_signals, recovered_signals):
    """ Prints the PSNR of k random recovered signals.

    Assumes rows are signals.

    :param k: The number og PSNRs to compute
    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if recovered_signals.shape != original_signals.shape:
      raise ValueError("Input has unequal dimensions.")
    k_large = recovered_signals.shape[0]
    indecies = np.random.choice(k_large, k)
    original_signals_samples = original_signals[indecies]
    recovered_signals_samples = recovered_signals[indecies]

    for i in range(k):
      org = original_signals_samples[i:i+1]
      rec = recovered_signals_samples[i:i+1]
      print(f"PSNR of signal {indecies[i]}: {cls.PSNR_big(org, rec)}.")

    return original_signals_samples, recovered_signals_samples

  @classmethod
  def k_random_PSNR_2D(cls, k, original_signals, recovered_signals):
    """ Prints the PSNR of k random recovered images.

    Assumes rows are signals.

    :param k: The number og PSNRs to compute
    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if recovered_signals.shape != original_signals.shape:
      raise ValueError("Input has unequal dimensions.")
    k_large = recovered_signals.shape[0]
    indecies = np.random.choice(k_large, k)
    original_signals_samples = original_signals[indecies]
    recovered_signals_samples = recovered_signals[indecies]

    for i in range(k):
      org = original_signals_samples[i:i+1]
      rec = recovered_signals_samples[i:i+1]
      print(f"PSNR of signal {indecies[i]}: {cls.PSNR_big_2D(org, rec)}.")

    return original_signals_samples, recovered_signals_samples

  @classmethod
  def count_valid_PSNR(cls, psnr_threshold, original_signals, recovered_signals):
    """ Counts the number of signals that has PSNR greater than or equal to the given threshold

    :param psnr_threshold: The desired PSNR threshold
    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if recovered_signals.shape != original_signals.shape:
      raise ValueError("Input has unequal dimensions.")

    over_threshold_counter = 0
    PSNRs = cls.PSNR(original_signals, recovered_signals)
    for i in range(len(PSNRs)):
      if PSNRs[i] >= psnr_threshold:
        over_threshold_counter +=1

    return over_threshold_counter


  @classmethod
  def count_valid_PSNR_2D(cls, psnr_threshold, original_signals, recovered_signals):
    """ Counts the number of images that has PSNR greater than or equal to the given threshold

    :param psnr_threshold: The desired PSNR threshold
    :param original_signals: The original signals in a matrix with rows as signals
    :param recovered_signals: The recovered signals in a matrix with rows as signals
    """
    if recovered_signals.shape != original_signals.shape:
      raise ValueError("Input has unequal dimensions.")

    over_threshold_counter = 0
    PSNRs = cls.PSNR_2D(original_signals, recovered_signals)
    for i in range(len(PSNRs)):
      if PSNRs[i] >= psnr_threshold:
        over_threshold_counter += 1

    return over_threshold_counter


###############
### Testing ###
###############

if __name__ == "__main__":
  x = np.linspace(0,10)
  y = x**2
  Evaluation.plot_12_random(9)

