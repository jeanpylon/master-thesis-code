import numpy as np

class GenerateGaussianMatrix:
  def __init__(self):
    self._matrix = None
    self._N = None

  @property
  def matrix(self):
    return self._matrix

  @property
  def N(self):
    return self._N

  def generate_matrix(self, N):
    """ Generates and returns a random gaussian N x N matrix.

    :param N: Number of rows/columns in matrix.
    """
    if N != round(np.sqrt(N))**2:
      raise ValueError(f"N is not a power of two: {N}")
    self._matrix = np.random.normal(size=(N, N))
    self._n = np.sqrt(N)

    return self._matrix

  def save_matrix(self, file_path):
    """ Saves the matrix to the given file path.

    :param path: The path to save the matrix file to.
    """
    if self._matrix is None:
      raise RuntimeError("Matrix must be initialized first.")

    np.save(file_path, self._matrix)


if __name__ == "__main__":
  n = 128
  N = n**2
  file_path = f"/home/jab/Skole/Master/data/gaussian_matrices/{N}x{N}_gaussian_matrix"

  gen = GenerateGaussianMatrix()
  gen.generate_matrix(N)
  gen.save_matrix(file_path)