""" Makes excerpts of (large) recovered image files. """
from os.path import join, isdir
import numpy as np
import json
from os import mkdir, listdir
from os.path import abspath, split
import random

class Excerpt:
  def make_excerpt_folder(self, images_folder):
    """ Loops through indices until finding an unused folder name. Then creates the according folder. """
    index = 1
    excerpt_folder = f"{join(images_folder, 'excerpt')}_{index}"

    while isdir(excerpt_folder):
      index += 1
      excerpt_folder = f"{join(images_folder, 'excerpt')}_{index}"

    mkdir(excerpt_folder)

    return excerpt_folder

  def get_measurement_folders(self, images_folder):
    """ Returns the measurements folders contained in the given folder (relative to given folder, NOT absolute path). """
    folders = [path for path in listdir(images_folder) if isdir(join(images_folder, path))]
    measurement_folders = [folder for folder in folders if "measurements" in folder] #TODO: make this more acccurate

    return measurement_folders

  def get_recovery_folders(self, measurements_folder):
    """ Returns the measurements folders contained in the given folder (relative to given folder, NOT absolute path). """
    folders = [path for path in listdir(measurements_folder) if isdir(join(measurements_folder, path))]
    recoveries_folders = [folder for folder in folders if "recoveries" in folder]  # TODO: make this more acccurate

    return recoveries_folders

  def get_k(self, meta_data_file_name):
    with open(meta_data_file_name, "r") as file:
      meta_data = json.load(file)
      print(f"Meta data successfully loaded from file '{meta_data_file_name}'.")

      k_train = meta_data["Images"]["Number of images for training"]
      k_test = meta_data["Images"]["Number of images for testing"]

    return k_train, k_test

  def make_indices(self, choose_k, N, rand):
    """ Returns list of indices or None if choose_k <= 0. """
    indices = None

    if choose_k > 0:
      if rand:
        # indices = np.random.choice(N, choose_k, replace=False)
        indices = random.sample(range(N), choose_k) # TODO: check that this function is legit
      else:
        indices = list(range(choose_k))

    return indices

  def make_and_save_excerpt(self, file_name, excerpt_file_name, indices, set):
    try:
      original_array = np.load(file_name)
    except FileNotFoundError:
      print(f"File '{file_name}' not found. File was probably just not generated.")
      return

    print(f"{set} with shape {original_array.shape} successfully loaded from file '{file_name}'.")
    np.save(excerpt_file_name, original_array[indices])
    print(f"Excerpt of {set} with shape {original_array[indices].shape} successfully saved to file '{excerpt_file_name}.npy'.")

  def update_meta_data(self, meta_data_file_name, excerpt_meta_data_file_name, indices_train, indices_test, set):
    """ Copies the meta data file to new location and updates it with excerpt info. """
    if set != "images" and set != "measurements" and set != "recoveries":
      raise ValueError(f"Invalid argument type: '{set}'.")

    with open(meta_data_file_name, "r") as file:
      meta_data = json.load(file)
      print(f"Meta data successfully loaded from file '{meta_data_file_name}'.")

    # variable update for images
    meta_data["Images"]["Original total number of images"] = meta_data["Images"]["Total number of images"]
    meta_data["Images"]["Total number of images"] = len(indices_train) + len(indices_test)
    meta_data["Images"]["Original number of images for training"] = meta_data["Images"]["Number of images for training"]
    meta_data["Images"]["Number of images for training"] = len(indices_train)
    meta_data["Images"]["Original number of images for testing"] = meta_data["Images"]["Number of images for testing"]
    meta_data["Images"]["Number of images for testing"] = len(indices_test)
    meta_data["Images"]["Excerpt indices training"] = indices_train
    meta_data["Images"]["Excerpt indices test"] = indices_test

    # additional updates for measurements
    if set == "measurements":
      if "Training measurements path" in meta_data["Measurements"]:
        meta_data["Measurements"]["Original training measurements path"] = meta_data["Measurements"]["Training measurements path"]
        meta_data["Measurements"]["Training measurements path"] = split(excerpt_meta_data_file_name)[0]
      if "Test measurements path" in meta_data["Measurements"]:
        meta_data["Measurements"]["Original test measurements path"] = meta_data["Measurements"]["Test measurements path"]
        meta_data["Measurements"]["Test measurements path"] = split(excerpt_meta_data_file_name)[0]

    # additional updates for recoveries
    if set == "recoveries":
      if "Training measurements path" in meta_data["Measurements"]:
        meta_data["Measurements"]["Original training measurements path"] = meta_data["Measurements"]["Training measurements path"]
        meta_data["Measurements"]["Training measurements path"] = split(split(excerpt_meta_data_file_name)[0])[0]
      if "Test measurements path" in meta_data["Measurements"]:
        meta_data["Measurements"]["Original test measurements path"] = meta_data["Measurements"]["Test measurements path"]
        meta_data["Measurements"]["Test measurements path"] = split(split(excerpt_meta_data_file_name)[0])[0]
      if "Training recoveries path" in meta_data["Recoveries"]:
        meta_data["Recoveries"]["Original training recoveries path"] = meta_data["Recoveries"]["Training recoveries path"]
        meta_data["Recoveries"]["Training recoveries path"] = split(excerpt_meta_data_file_name)[0]
      if "Test recoveries path" in meta_data["Recoveries"]:
        meta_data["Recoveries"]["Original test recoveries path"] = meta_data["Recoveries"]["Test recoveries path"]
        meta_data["Recoveries"]["Test recoveries path"] = split(excerpt_meta_data_file_name)[0]

    with open(excerpt_meta_data_file_name, "w") as file:
      json.dump(meta_data, file, indent=2)
    print(f"Meta data successfully saved to file '{excerpt_meta_data_file_name}'.")

  def make(self, image_folder, choose_k_train, choose_k_test, random_train, random_test):
    """ Makes an excerpt of the image folder, it's measurements and recoveries and saves it.
        :param image_folder: The folder containing the image files, measurements and recoveries.
        :param choose_k_train: The number of images to pick for the excerpt of training set.
        :param choose_k_test: The number of images to pick for the excerpt of test set.
        :param random_train: If True, choose_k_train images are picked uniformly at random from the training set.
                             If False the choose_k_train first images are picked from the training set.
        :param random_test: If True, choose_k_test images are picked uniformly at random from the test set.
                             If False the choose_k_test first images are picked from the test set.
    """
    if choose_k_test == 0 and choose_k_train == 0:
      print("Arguments choose_k_train and choose_k_test both equal to zero. No excerpt made.")
      return

    # set up excerpt folder
    image_folder = abspath(image_folder)
    excerpt_folder = self.make_excerpt_folder(image_folder)

    # get k's
    meta_data_file_name = join(image_folder, "meta_data.json")
    k_train, k_test = self.get_k(meta_data_file_name)

    # make indices
    indices_train = self.make_indices(choose_k_train, k_train, random_train)
    indices_test = self.make_indices(choose_k_test, k_test, random_test)

    # copy and edit images meta data file
    images_meta_data_path = meta_data_file_name
    excerpt_images_meta_data_path = join(excerpt_folder, "meta_data.json")
    self.update_meta_data(images_meta_data_path, excerpt_images_meta_data_path, indices_train, indices_test, "images")

    # make images excerpts
    if indices_train is not None:
      images_train_file_name = join(image_folder, "images_train.npy")
      excerpt_images_train_file_name = join(excerpt_folder, "images_train_excerpt.npy")
      self.make_and_save_excerpt(images_train_file_name, excerpt_images_train_file_name, indices_train, "Training images")
    if indices_test is not None:
      images_test_file_name = join(image_folder, "images_test.npy")
      excerpt_images_test_file_name = join(excerpt_folder, "images_test_excerpt.npy")
      self.make_and_save_excerpt(images_test_file_name, excerpt_images_test_file_name, indices_test, "Test images")


    # make measurements excerpts and recoveries excerpts
    measurements_folders = self.get_measurement_folders(image_folder)

    for measurements_folder in measurements_folders:
      # create folder
      measurements_excerpt_folder = join(excerpt_folder, measurements_folder)
      mkdir(measurements_excerpt_folder)

      # copy and edit measurements meta data file
      measurements_meta_data_path = join(image_folder, measurements_folder, "meta_data.json")
      measurements_excerpt_meta_data_path = join(measurements_excerpt_folder, "meta_data.json")
      self.update_meta_data(measurements_meta_data_path, measurements_excerpt_meta_data_path, indices_train, indices_test, "measurements")

      # make measurements excerpts
      if indices_train is not None:
        measurements_train_file_name = join(image_folder, measurements_folder, "measurements_train.npy")
        measurements_excerpt_file_name = join(measurements_excerpt_folder, "measurements_train_excerpt")
        self.make_and_save_excerpt(measurements_train_file_name, measurements_excerpt_file_name, indices_train, "Training measurements")
      if indices_test is not None:
        measurements_test_file_name = join(image_folder, measurements_folder, "measurements_test.npy")
        measurements_excerpt_file_name = join(measurements_excerpt_folder, "measurements_test_excerpt")
        self.make_and_save_excerpt(measurements_test_file_name, measurements_excerpt_file_name, indices_test, "Test measurements")

      recovery_folders = self.get_recovery_folders(measurements_folder)
      for recovery_folder in recovery_folders:
        # create folder
        recoveries_excerpt_folder = join(measurements_excerpt_folder, recovery_folder)
        mkdir(recoveries_excerpt_folder)

        # copy and edit measurements meta data file
        recoveries_meta_data_path = join(image_folder, measurements_folder, recovery_folder, "meta_data.json")
        recoveries_excerpt_meta_data_path = join(recoveries_excerpt_folder, "meta_data.json")
        self.update_meta_data(recoveries_meta_data_path, recoveries_excerpt_meta_data_path, indices_train, indices_test, "recoveries")

        # make recovery excerpts
        if indices_train is not None:
          recoveries_train_file_name = join(image_folder, measurements_folder, recovery_folder, "recoveries_train.npy")
          recoveries_excerpt_file_name = join(recoveries_excerpt_folder, "recoveries_train_excerpt")
          self.make_and_save_excerpt(recoveries_train_file_name, recoveries_excerpt_file_name, indices_train, "Training recoveries")
        if indices_test is not None:
          recoveries_test_file_name = join(image_folder, measurements_folder, recovery_folder, "recoveries_test.npy")
          recoveries_excerpt_file_name = join(recoveries_excerpt_folder, "recoveries_test_excerpt")
          self.make_and_save_excerpt(recoveries_test_file_name, recoveries_excerpt_file_name, indices_test, "Test recoveries")

