import tensorflow as tf
from os.path import join
import numpy as np

class TFRecords_converter:
  """ Converts a .npy file containing a 3-d array into a .tfrec file.

  Limitations: Expects images to be two-dimensional.
  """
  def save_as_tfrec(self, input_file_name, save_folder, test_train, ratio=.01):
    """ Opens .npy-files containing signal data, and saves as .tfrec file.

    :param input_file_name: Name of .npy-file containing the image data, array with dim [batch size, height, width]
    :param save_folder: Folder to save result(s) in. One file if test_train=="test",
    one for training and one for validation if test_train=="train".
    :param test_train: Set to "train" in order to save one file with training data and one with validation data. Set
    to "test" in order to save only one file with test data.
    :param ratio: The size ratio train data/validation data when test_train=="train".
    """
    data_set = np.load(input_file_name)
    print(f"Data set with shape {data_set.shape} successfully loaded from file '{input_file_name}'.")

    if test_train.lower()=="train":
      train_tfrec_file_name = join(save_folder, "images_train.tfrec")
      validation_tfrec_file_name = join(save_folder, "images_validation.tfrec")
      k = len(data_set)
      val_zize = round(k*ratio)
      train_data_set = data_set[val_zize:]
      validation_data_set = data_set[:val_zize]

      with tf.io.TFRecordWriter(train_tfrec_file_name) as writer:
        for data_point in train_data_set:
          serialized_data_point = self.serialize_example(data_point)
          writer.write(serialized_data_point)
      print(f"Training data set with shape {train_data_set.shape} successfully written to file '{train_tfrec_file_name}'.")

      with tf.io.TFRecordWriter(validation_tfrec_file_name) as writer:
        for data_point in validation_data_set:
          serialized_data_point = self.serialize_example(data_point)
          writer.write(serialized_data_point)
      print(f"Validation data set with shape {validation_data_set.shape} successfully written to file '{validation_tfrec_file_name}'.")

    elif test_train.lower()=="test":
      test_tfrec_file_name = join(save_folder, "images_test.tfrec")

      with tf.io.TFRecordWriter(test_tfrec_file_name) as writer:
        for data_point in data_set:
          serialized_data_point = self.serialize_example(data_point)
          writer.write(serialized_data_point)
      print(f"Test data with shape {data_set.shape} set successfully written to file '{test_tfrec_file_name}'.")
    else:
      raise ValueError("Arg test_train must be either 'train' or 'test'.")

  def _bytes_feature(self, value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
      value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

  def _int64_feature(self, value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

  def serialize_example(self, image):
    feature = {'image': self._bytes_feature(image.tostring())}
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))

    return example_proto.SerializeToString()

### TESTING
if __name__ == "__main__":
  import numpy as np

  targets_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/random_dots_32/images_train.npy"
  input_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/random_dots_32/measurements_1/measurements_train.npy"
  batch_size = 64
  shuffle_buffer_size = 100
  TFRec_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/random_dots_32"

  # convert
  tfr = TFRecords_converter()
  tfr.save_as_tfrec(targets_file_name, TFRec_file_name, "train")
