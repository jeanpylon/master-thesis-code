import numpy as np
from scipy.linalg import hadamard


class GenerateHadamardMatrix:
  def __init__(self):
    self._matrix = None
    self._N = None

  @property
  def matrix(self):
    return self._matrix

  @property
  def N(self):
    return self._N

  def generate_matrix(self, N):
    """ Generates and returns a random gaussian N x N matrix.

    :param N: Number of rows/columns in matrix.
    """
    if N != round(2**np.log2(N)):
      raise ValueError(f"N={N} is not a power of two.")
    self._matrix = hadamard(N)
    self._N = N

    return self._matrix

  def generate_sequency_hadamard_matrix(self, N):
    H = np.zeros((N, N))
    for m in range(N):
      for n in range(N):
        H[m, n] = self.v(m, n/N)

    self._matrix = H
    self._N = N

    return H

  def v(self, n, x):
    """ Computes the sequency ordered walsh functions. """
    n_dot = self.dyad_N(n)
    x_dot = self.dyad_X(x, len(n_dot) - 1)

    # compute the function value
    pow = 0
    for i in range(len(x_dot)):
      pow += (n_dot[i] + n_dot[i+1])*x_dot[i]

    return (-1)**pow

  def dyad_N(self, n):
    # compute dyadic expansion of n
    if round(n) != n:
      raise ValueError(f"Arg n must be an integer: n = {n}.")
    n_dot = [int(i) for i in format(n, 'b')]
    n_dot.reverse()
    n_dot = np.array(n_dot + [0])

    return n_dot

  def dyad_X(self, x, n):
    # compute the len(n_dot) -  1 first dyadic exp numbers of x
    x_k = 0
    x_dot = np.zeros(n)
    for k in range(len(x_dot)):
      if x_k + 2**(-(1 + k)) < x:
        x_dot[k] = 1
        x_k += 2**(-(1 + k))
      elif x_k + 2**(-(1 + k)) == x:
        x_dot[k] = 1
        return x_dot

    return x_dot


  def save_matrix(self, file_path):
    """ Saves the matrix to the given file path.

    :param path: The path to save the matrix file to.
    """
    if self._matrix is None:
      raise RuntimeError("Matrix must be initialized first.")

    np.save(file_path, self._matrix)

  def kroenecker(self):
    """ Replaces the matrix with the kroenecker product of itself with itself. """
    self._matrix = np.kron(self._matrix, self._matrix)
    self._N = self._N**2


if __name__ == "__main__":
  n = 128
  N = n**2
  file_path = f"/home/jab/Skole/Master/extra_data/matrices/{n**2}x{n**2}_sequency_hadamard_kroenecker_matrix"
  gen = GenerateHadamardMatrix()
  gen.generate_sequency_hadamard_matrix(n)

  print("Taking kron prod...")
  gen.kroenecker()
  print("Done")
  gen.save_matrix(file_path)
