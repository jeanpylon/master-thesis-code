import numpy as np
import os


class NewSparse1DRandomDataGenerator:
  """
  This class randomly generates s-sparse 1D signals and gaussian modalities, and simulates measurements from this.

  It follows the convention of storing signals and measurements in the following 3D format (batch size, signal length, 1).
  That is, any time you encounter a "3-tensor" of signals or measurements, the rows are the signals/measurements.
  """

  def __init__(self):
    self.signals = None
    self.modality = None
    self.measurements = None
    self.N = None
    self.m = None
    self.k = None
    self.s = None

  def generate_signals(self, N, k, s):
    """ Generates and stores a k by N by 1 cube where the rows are s-sparse signals.

    Note: The signals generated do not necessarily have support with cardinality s. The fact that they
    are s-sparse just means that the cardinality of the support of the signals are in the set {1, 2, ... ,s}.

    :param N: Signal length (columns in signals matrix), integer
    :param k: Number of signals (rows in signal matrix), integer
    :param s: Sparsity of signal, that is the maximum number of non-zero entries in each signal
    """
    # generate N by k matrix of signals
    self.signals = np.zeros((k, N, 1))
    for i in range(k):
      s_0 = np.random.randint(s)
      signal = np.random.rand(s_0)
      ind = np.random.choice(N, s_0)
      self.signals[i, ind, 0] = signal

    self.N = N
    self.k = k
    self.s = s

    print(f"{k} {s}-sparse signals of length {N} generated and stored in {self.signals.shape}-array. ")

  def save_signals(self, filename):
    if self.signals is None:
      print("Error: No signals to save.")
    else:
      np.save(filename, self.signals)
      print(f"Signals with shape {self.signals.shape} saved to file '{filename}.npy'")

  def generate_gaussian_modality(self, m):
    """ Generates and stores an m by N gaussian measurement matrix.

    self.generate_signals must be called before this function.

    :param m: the number of rows in the measurement matrix (number of measurements per signal)
    """
    if self.N is None:
      raise RuntimeError("Function generate_signals must be called before generate_gaussian_modality is called.")

    self.modality = np.random.normal(size=(m, self.N))
    self.m = m
    print(f"{m}x{N} random gaussian measuring matrix generated.")

  def save_modality(self, filename):
    if self.modality is None:
      print("Error: No modality to save.")
    else:
      np.save(filename, self.modality)
      print(f"Sampling modality with shape {self.modality.shape} aved to file '{filename}.npy'")

  def generate_measurements(self):
    """ Generates and stores a k by m matrix where the rows are simulated samplings/measurements of self.signals.

    More specifically this function computes Y = (A*X')' where A is the matrix self.modality and
    X' is the matrix self.signals transposed. The final result (A*X')' is then, as you might have guessed,
    A*X' transposed.
    """
    if self.modality is None or self.signals is None:
      raise RuntimeError("No modality or signals, unable to simulate measurements.")

    self.measurements = np.matmul(self.modality, self.signals.T).T
    print(f"Measurements array with shape {self.measurements.shape} generated.")

  def save_measurements(self, filename):
    if self.measurements is None:
      print("Error: No measurements to save.")
    else:
      np.save(filename, self.measurements)
      print(f"Measurements with shape {self.measurements.shape} saved to file '{filename}.npy'")

  def generate_data_set(self, m, N, k, s):
    """ Sets up a randomly generated data_generation set of k s-sparse signals of length N with simulated measurements of length m.

    :param m: the number of rows in the measurement matrix (number of measurements per signal)
    :param N: Signal length (columns in signals matrix), integer
    :param k: Number of signals (rows in signal matrix), integer
    :param s: Sparsity of signal, that is the maximum number of non-zero entries in each signal
    """
    self.generate_signals(N, k, s)
    self.generate_gaussian_modality(m)
    self.generate_measurements()

  def save_all(self, path, ratio):
    """ Saves all of the generated data and meta data in a folder located at the given path.

    :param path: Path to save the files to
    :param ratio: the proportion of data points to be test data
    """
    metadata = ""
    measurements_train, signals_train, measurements_test, signals_test = self.get_train_test_data(ratio)

    signals_train_filename = os.path.join(path, "signals_train")
    np.save(signals_train_filename, signals_train)
    print(f"Training signals array with shape {signals_train.shape} saved to file '{signals_train_filename}'.")
    metadata += f"Training signals array shape: {signals_train.shape}\n"

    signals_test_filename = os.path.join(path, "signals_test")
    np.save(signals_test_filename, signals_test)
    print(f"Test signals array with shape {signals_test.shape} saved to file '{signals_test_filename}'.")
    metadata += f"Test signals array shape: {signals_test.shape}\n"

    self.save_modality(os.path.join(path, "modality"))

    measurements_train_filename = os.path.join(path, "measurements_train")
    np.save(measurements_train_filename, measurements_train)
    print(f"Training measurements array with shape {measurements_train.shape} saved to file '{measurements_train_filename}'.")
    metadata += f"Training measurements array shape: {measurements_train.shape}\n"

    measurements_test_filename = os.path.join(path, "measurements_test")
    np.save(measurements_test_filename, measurements_test)
    print(f"Test measurements array with shape {measurements_test.shape} saved to file '{measurements_test_filename}'.")
    metadata += f"Test measurements array shape: {measurements_test.shape}\n"

    self.save_meta_data(os.path.join(path, "metadata.txt"), metadata)

  def get_train_test_data(self, ratio):
    """ Returns data set partitioned into training and test data with the given ratio in size.
    :param ratio: the proportion of data points to be test data
    """
    divide = round(ratio * self.k)
    measurements_train = self.measurements[divide:]
    signals_train = self.signals[divide:]
    measurements_test = self.measurements[:divide]
    signals_test = self.signals[:divide]

    return measurements_train, signals_train, measurements_test, signals_test

  def save_meta_data(self, filename, extra_data):
    metadata = "Signals is randomly generated. Measurements are simulated using a Gaussian sampling modality.\n"
    metadata += f"k: {self.k} (Number of data points)\n"
    metadata += f"m: {self.m} (Number of measurements per signal)\n"
    metadata += f"k: {self.N} (Signal length)\n"
    metadata += f"s: {self.s} (Signal sparsity)\n\n"

    metadata += extra_data

    with open(filename, "w") as file:
      file.write(metadata)

    print(f"Metadata written to file '{filename}'.")

if __name__ == "__main__":
  N = 128  # number of entries in signals
  k = 100_000  # number of signals
  s = 20  # sparsity of signals
  m = 60  # number of measurements per signal (rows in modality)

  sig = NewSparse1DRandomDataGenerator()
  sig.generate_signals(N, k, s)
  sig.generate_gaussian_modality(m)
  sig.generate_measurements()


  path = "/home/jab/Skole/Master/data/data_sets/1D/random_s_sparse"
  if not os.path.isdir(path):
    raise ValueError("Path does not exist.")

  sig.save_all(path, .1)

