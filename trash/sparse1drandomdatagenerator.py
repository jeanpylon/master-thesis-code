import numpy as np
import os

class Sparse1DRandomDataGenerator:
  """
  This class randomly generates s-sparse signals and gaussian modalities, and simulates measurements from this.

  It follows the convention of storing signals and measurements as row vectors. That is, any time you encounter
  a matrix of signals or measurements, the rows are the signals/measurements.
  """

  def __init__(self):
    self.signals = None
    self.modality = None
    self.measurements = None
    self.N = None
    self.m = None
    self.k = None
    self.s = None

  def generate_signals(self, N, k, s):
    """ Generates and stores a k by N matrix where the rows are s-sparse signals.

    Note: The signals generated do not necessarily have support with cardinality s. The fact that they
    are s-sparse just means that the cardinality of the support of the signals are in the set {1, 2, ... ,s}.

    :param N: Signal length (columns in signals matrix), integer
    :param k: Number of signals (rows in signal matrix), integer
    :param s: Sparsity of signal, that is the maximum number of non-zero entries in each signal
    """
    # generate N by k matrix of signals
    self.signals = np.zeros((k, N))
    for i in range(k):
      s_0 = np.random.randint(s)
      signal = np.random.rand(s_0)
      ind = np.random.choice(N, s_0)
      self.signals[i, ind] = signal

    self.N = N
    self.k = k
    self.s = s

    print(f"{k} {s}-sparse signals of length {N} generated and stored in {k}x{N}-matrix. ")

  def save_signals(self, filename):
    if self.signals is None:
      print("Error: No signals to save.")
    else:
      np.save(filename, self.signals)
      print(f"{self.k}x{self.N}-matrix with rows as signals saved to file '{filename}.npy'")

  def generate_gaussian_modality(self, m):
    """ Generates and stores an m by N gaussian measurement matrix.

    self.generate_signals must be called before this function.

    :param m: the number of rows in the measurement matrix (number of measurements per signal)
    """
    if self.N is None:
      raise RuntimeError("Function generate_signals must be called before generate_gaussian_modality is called.")

    self.modality = np.random.normal(size=(m, self.N))
    self.m = m
    print(f"{m}x{N} random gaussian measuring matrix generated.")

  def save_modality(self, filename):
    if self.modality is None:
      print("Error: No modality to save.")
    else:
      np.save(filename, self.modality)
      print(f"{self.m}x{self.N}-modality saved to file '{filename}.npy'")

  def generate_measurements(self):
    """ Generates and stores a k by m matrix where the rows are simulated samplings/measurements of self.signals.

    More specifically this function computes Y = (A*X')' where A is the matrix self.modality and
    X' is the matrix self.signals transposed. The final result (A*X')' is then, as you might have guessed,
    A*X' transposed.
    """
    if self.modality is None or self.signals is None:
      raise RuntimeError("No modality or signals, unable to simulate measurements.")

    self.measurements = np.matmul(self.modality, self.signals.T).T
    print(f"{self.k}x{self.m}-matrix with rows as measurements generated.")

  def save_measurements(self, filename):
    if self.measurements is None:
      print("Error: No measurements to save.")
    else:
      np.save(filename, self.measurements)
      print(f"{self.k}x{self.m}-matrix with rows as measurements saved to file '{filename}.npy'")

  def generate_data_set(self, m, N, k, s):
    """ Sets up a randomly generated data_generation set of k s-sparse signals of length N with simulated measurements of length m.

    :param m: the number of rows in the measurement matrix (number of measurements per signal)
    :param N: Signal length (columns in signals matrix), integer
    :param k: Number of signals (rows in signal matrix), integer
    :param s: Sparsity of signal, that is the maximum number of non-zero entries in each signal
    """
    self.generate_signals(N, k, s)
    self.generate_gaussian_modality(m)
    self.generate_measurements()

  def save_all(self, path):
    """ Saves all of the generated data and meta data in a folder located at the given path.

    :param path: Path to save the files to
    """
    
    self.save_signals(path + "/signals")
    self.save_modality(path + "/modality")
    self.save_measurements(path + "/measurements")
    self.save_meta_data()

  def save_meta_data(self):
    pass


if __name__ == "__main__":
  N = 128 # number of entries in signals
  k = 100_000 # number of signals
  s = 20 # sparsity of signals
  m = 60 # number of measurements per signal (rows in modality)

  sig = Sparse1DRandomDataGenerator()
  sig.generate_signals(N, k, s)
  sig.generate_gaussian_modality(m)
  sig.generate_measurements()

  path = f"../../data/m{m}_N{N}_s{s}_k{k}"
  if not os.path.isdir(path):
    os.mkdir(path)
  sig.save_signals(path + "/signals")
  sig.save_modality(path + "/modality")
  sig.save_measurements(path + "/measurements")
