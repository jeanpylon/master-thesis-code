from trash.data_manager import DataManager
from lib.utilities import *
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.models import Sequential

# Load data_generation and set hyper parameters
data_folder = "m60_N128_s20_k100000"
data = DataManager()
A = data.load_modality("data_generation/" + data_folder + "/modality.npy") # Sampling modality
data.load_measurements("data_generation/" + data_folder + "/measurements.npy") # Input in recovery process
data.load_signals("data_generation/" + data_folder + "/signals.npy") # Output from recovery process
m, N, k = data.get_dims() # m: no of entries in measurements, N: no of entries in signal, k: no of signals/measurements
test_ratio = .2
k_test = round(test_ratio*k)
k_train = k - k_test
epochs = 1 # number of epochs of training
num_layers = 8
name = "pseudo_inv_dense_nn_recovery"
model_path = "training_output"
optimizer = "adam"
loss = "mse"

# Verify data_generation
#data_generation.verify(data_generation)

# Compute pseudo inverse
# Note: As the matrices treated in this module are expected to be real,
# we only need to transpose instead of conjugate transpose.
A_dagger = np.matmul(A.T ,np.linalg.inv(np.matmul(A, A.T)))

# set up dense NN
DNN = Sequential()
DNN.add(Dense(N, input_dim=N, activation="relu"))
for _ in range(num_layers - 1):
  DNN.add(Dense(N))
  DNN.add(Activation("relu"))
DNN.compile(optimizer=optimizer, loss=loss)

# split data_generation
X_train, Y_train, X_test, Y_test = data.get_train_test_data(test_ratio)

#X_train = X_train[:100] # for debugging
#Y_train = Y_train[:100] # for debugging

#print(f"Dimensions of training input: {X_train.shape}")
#print(f"Dimensions of training output: {Y_train.shape}")
#print(f"Dimensions of testing input: {X_test.shape}")
#print(f"Dimensions of testing output: {Y_test.shape}")

# train DNN
DNN_X_train = np.matmul(A_dagger, X_train.T).T
history = DNN.fit(DNN_X_train, Y_train, epochs=epochs)
"""
### Evaluate on training set
# recover with (DNN) o (A_dagger) on training set
Y_star_train = DNN.predict(DNN_X_train)

# Compute error training set
total_MSE_train = Evaluation.MSE_big(Y_train, Y_star_train)
print(f"MSE of all entries of all recovered training signals: {total_MSE_train}")

# keras' error metric training set
#print(f"Keras evaluation on training set: {CNN.evaluate(CNN_X_train_reshape, Y_train_reshape)}")

print(f"PSNR on test set (when we treat all the k signals as one signal): {Evaluation.PSNR_big(Y_train, Y_star_train)}")

### Evaluate on test set
# recover with (DNN) o (A_dagger) on test set
DNN_X_test = np.matmul(A_dagger, X_test.T).T
Y_star_test = DNN.predict(DNN_X_test)

# Compute error test set
total_MSE_test = Evaluation.MSE_big(Y_test, Y_star_test)
print(f"MSE of all entries of all recovered test signals: {total_MSE_test}")

# Keras' error metric test set
#print(f"Keras evaluation on test set: {CNN.evaluate(CNN_X_test_reshape, Y_test_reshape)}")

print(f"PSNR on test set (when we treat all the k signals as one signal): {Evaluation.PSNR_big(Y_test, Y_star_test)}")
"""
# save model and trainingplot
model_saved_to, plot_saved_to = save_training(DNN, name, history.history["loss"], "MSE", model_path, data_folder, "_layers_" + str(num_layers))

# plot comparisons on test set
#for i in range(k):
#  Evaluation.plot_signal_comparison(Y_star_test[i], Y_test[i])

