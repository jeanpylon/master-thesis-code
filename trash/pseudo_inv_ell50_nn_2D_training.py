print("Loading modules ...")
from lib.models.ell50_2D import setup_ELL50_2D
from lib.utilities import *
from lib.data_generation.fourier_sampling import FourierSampling
#import matplotlib.pyplot as plt
import numpy as np

# Load data_generation and set hyper parameters

data_folder = "../data_sets/2D/mri_512"
epochs = 1000 # number of epochs of training
channels = 64
batch_size = 1
name = "transpose_ELL50_2D"
model_path = "../models"

signals_file = os.path.join(data_folder, "signals_train.npy")
measurements_file = os.path.join(data_folder, "measurements_train.npy")
sampling_pattern_file = os.path.join(data_folder, "sampling_scheme.mat")

print("Instantiating class FourierSampling ...")
data = FourierSampling()

print("Loading training data ...")
signals = np.load(signals_file)
measurements = np.load(measurements_file)
data.load_sampling_pattern_matlab(sampling_pattern_file) # load sampling scheme (vector)

k, dummy, n = signals.shape
k, m = measurements.shape
data.m = m
data.n = n
data.k = k
print(f"k: {k}, m: {m}, n: {n}")

print("Setting up network ...")
# set up ELL50 CNN
CNN, network_meta_data = setup_ELL50_2D(n, n, 2)


# train DNN
print(signals.shape)
print(measurements.shape)
CNN_X_train_real = data.PFT(measurements).real
CNN_X_train_img = data.PFT(measurements).imag


CNN_X_train_real = CNN_X_train_real.reshape(CNN_X_train_real.shape + (1,))
CNN_X_train_img = CNN_X_train_img.reshape(CNN_X_train_img.shape + (1,))

CNN_X_train = np.concatenate([CNN_X_train_real, CNN_X_train_img], axis=-1)

Y_train = signals.reshape(signals.shape + (1,)) # add third dimension with length 1 (channel dimension)
Y_train = np.concatenate([Y_train, np.zeros_like(Y_train)], axis=-1)

history = CNN.fit(CNN_X_train, Y_train, epochs=epochs, batch_size=batch_size)

# save model and trainingplot
model_saved_to, plot_saved_to = save_training(CNN, name, history.history["loss"], "MSE", model_path, data_folder, "_channels_" + str(channels))

