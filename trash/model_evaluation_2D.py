print("Loading libraries ...")
from lib.utilities import *
from lib.data_generation.fourier_sampling import FourierSampling
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
from lib.data_generation.evaluation import Evaluation
import os
import numpy as np


def eval_visual_serial(measurements, target_signals):
  for i in range(len(measurements)):
    CNN_X_train = data.PFT(measurements[i:i + 1])

    CNN_X_train_real = CNN_X_train.real
    CNN_X_train_img = CNN_X_train.imag

    CNN_X_train_real = CNN_X_train_real.reshape(CNN_X_train_real.shape + (1,))
    CNN_X_train_img = CNN_X_train_img.reshape(CNN_X_train_img.shape + (1,))

    CNN_X_train = np.concatenate([CNN_X_train_real, CNN_X_train_img], axis=-1)

    pred = CNN.predict(CNN_X_train)
    Y_train_star = special_abs(pred[:,:,:,0], pred[:,:,:,1])
    print(f"{target_signals[i].shape}, {Y_train_star.shape}")
    plot_img = np.concatenate([target_signals[i], Y_train_star[0]], axis=1)
    plt.clf()
    plt.imshow(plot_img)
    plt.show()

def eval_serial(measurements, target_signals):
  psnrs = []
  for i in range(len(measurements)):
    CNN_X_train = data.PFT(measurements[i:i + 1])

    CNN_X_train_real = CNN_X_train.real
    CNN_X_train_img = CNN_X_train.imag

    CNN_X_train_real = CNN_X_train_real.reshape(CNN_X_train_real.shape + (1,))
    CNN_X_train_img = CNN_X_train_img.reshape(CNN_X_train_img.shape + (1,))

    CNN_X_train = np.concatenate([CNN_X_train_real, CNN_X_train_img], axis=-1)

    pred = CNN.predict(CNN_X_train)
    Y_train_star = special_abs(pred[:,:,:,0], pred[:,:,:,1])

    #print(f"Prediction shape: {Y_train_star.shape}")
    #print(Y_train_star.dtype)
    psnr = Evaluation.PSNR_2D(target_signals[i:i+1], Y_train_star)
    print(f"PSNR of image reconstruction {i}: {psnr}")
    psnrs.append(psnr)

    #plt.clf()
    #plt.imshow(np.concatenate([target_signals[i], Y_train_star[0]], axis=1))
    #plt.show()

  return np.array(psnrs)


def special_abs(real, img):
  return np.sqrt(real**2 + img**2)


def eval_parallel(measurements, target_signals):
  CNN_X_train = data.PFT(measurements)

  CNN_X_train_real = CNN_X_train.real
  CNN_X_train_img = CNN_X_train.imag

  CNN_X_train_real = CNN_X_train_real.reshape(CNN_X_train_real.shape + (1,))
  CNN_X_train_img = CNN_X_train_img.reshape(CNN_X_train_img.shape + (1,))

  CNN_X_train = np.concatenate([CNN_X_train_real, CNN_X_train_img], axis=-1)

  pred = CNN.predict(CNN_X_train)
  Y_train_star = special_abs(pred[:, :, :, 0], pred[:, :, :, 1])

  psnrs = Evaluation.PSNR_2D(Y_train_star, target_signals)

  return np.array(psnrs)

# Load data_generation and set hyper parameters
data_folder = "/home/jab/Skole/Master/data/data_sets/2D/random_dots_512_01/"
#data_folder = "../data_sets/2D/random_dots_512_004"
model_path = "/home/jab/Skole/Master/models/transpose_ELL50_2D_epochs_1000__channels_64"
#model_path = "../models/transpose_ELL50_2D_epochs_1000__channels_64"

signals_file = os.path.join(data_folder, "signals_test.npy")
measurements_file = os.path.join(data_folder, "measurements_test.npy")
sampling_pattern_file = os.path.join(data_folder, "sampling_scheme.mat")

batch_size = 1


print("Instantiating class FourierSampling ...")
data = FourierSampling()

print(f"Loading training data ('{data_folder}')...")
signals = np.load(signals_file)
measurements = np.load(measurements_file)
data.load_sampling_pattern_matlab(sampling_pattern_file) # load sampling scheme (vector)

k, dummy,n = signals.shape
k, m = measurements.shape
data.m = m
data.n = n
data.k = k
print(f"k: {k}, m: {m}, n: {n}")

print("Setting up network ...")
# set up ELL50 CNN
CNN = load_model(model_path)

### Evaluate on training set
print("Evaluating model")
# recover with (DNN) o (A_T) on training set

eval_visual_serial(measurements, signals)
"""
psnrs = eval_serial(measurements, signals)

print(f"Max PSNR: {psnrs.max()}")
print(f"Min PSNR: {psnrs.min()}")
print(f"Mean PSNR: {psnrs.mean()}")
"""