from lib.models.ell50_1D import setup_ELL50_1D
from lib.utilities import *
from trash.data_manager import DataManager

# Load data_generation and set hyper parameters

data_folder = "m60_N128_s20_k100000"
data = DataManager()

data.load_measurements("data_generation/" + data_folder + "/measurements.npy") # Input in recovery process
data.load_signals("data_generation/" + data_folder + "/signals.npy") # Output from recovery process
data.load_modality("data_generation/" + data_folder + "/modality.npy") # load sampling scheme (vector)
m, N, k = data.get_dims() # m: no of entries in measurements, n: images have size n*n, k: no of signals/measurements
print(f"k: {k}, m: {m}, n: {N}")
test_ratio = .2
k_test = round(test_ratio*k)
k_train = k - k_test
epochs = 1 # number of epochs of training
channels = 64
batch_size = 1
name = "pseudo_inv_ELL50_recovery_1D_new"
model_path = "training_output"

# set up ELL50 CNN
CNN = setup_ELL50_1D(N)

# split data_generation
X_train, Y_train, X_test, Y_test = data.get_train_test_data(test_ratio)

print(f"Dimensions of training input: {X_train.shape}")
print(f"Dimensions of training output: {Y_train.shape}")
print(f"Dimensions of testing input: {X_test.shape}")
print(f"Dimensions of testing output: {Y_test.shape}")

# train DNN
# Multiply with pseudo inverse
print(X_train.shape)
CNN_X_train = data.A_pseudo(X_train.T).T
# reshape
CNN_X_train = CNN_X_train.reshape(CNN_X_train.shape + (1,1)) # add third dimension with length 1 (channel dimension)
Y_train = Y_train.reshape(Y_train.shape + (1,1)) # add third dimension with length 1 (channel dimension)
# train
print(CNN_X_train.shape)
print(Y_train.shape)
history = CNN.fit(CNN_X_train, Y_train, epochs=epochs, batch_size=batch_size)

"""
### Evaluate on training set
# recover with (DNN) o (A_dagger) on training set
Y_star_train = CNN.predict(CNN_X_train_reshape)
Y_star_train_reshape = Y_star_train.reshape(Y_star_train.shape[0:2])

# Compute error training set
total_MSE_train = Evaluation.MSE_big(Y_train, Y_star_train_reshape)
print(f"MSE of all entries of all recovered training signals: {total_MSE_train}")

# keras' error metric training set
#print(f"Keras evaluation on training set: {CNN.evaluate(CNN_X_train_reshape, Y_train_reshape)}")

print(f"PSNR on test set (when we treat all the k signals as one signal): {Evaluation.PSNR_big(Y_train, Y_star_train_reshape)}")

### Evaluate on test set
# recover with (DNN) o (A_dagger) on test set
CNN_X_test = np.matmul(A_dagger, X_test.T).T
CNN_X_test_reshape = reshape_x(CNN_X_test)

Y_star_test = CNN.predict(CNN_X_test_reshape)
Y_star_test_reshape = Y_star_test.reshape(Y_star_test.shape[0:2])

# Compute error test set
total_MSE_test = Evaluation.MSE_big(Y_test, Y_star_test_reshape)
print(f"MSE of all entries of all recovered test signals: {total_MSE_test}")

# Keras' error metric test set
#print(f"Keras evaluation on test set: {CNN.evaluate(CNN_X_test_reshape, Y_test_reshape)}")

print(f"PSNR on test set (when we treat all the k signals as one signal): {Evaluation.PSNR_big(Y_test, Y_star_test_reshape)}")

"""
# save model and trainingplot
model_saved_to, plot_saved_to = save_training(CNN, name, history.history["loss"], "MSE", model_path, data_folder, "_channels_" + str(channels))

"""
# plot comparisons on test set
for i in range(k):
  Evaluation.plot_signal_comparison(Y_star_test_reshape[i], Y_test[i])
"""
