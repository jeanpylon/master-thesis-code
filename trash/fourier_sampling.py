import numpy as np
from os.path import isfile, join
from scipy.io import savemat, loadmat


class FourierSampling:
  MAX_VALUE = 1
  MATLAB_MATRIX_INDEXING = "F"

  def __init__(self):
    self._sampling_pattern = None
    self.k = None
    self.m = None
    self._n = None
    self.N = None
    self.images = None
    self.measurements = None
    self.workspace = None

  @property
  def n(self):
    return self._n

  @n.setter
  def n(self, value):
    self._n = value
    self.N = value**2

  @property
  def sampling_pattern(self):
    return self._sampling_pattern

  @sampling_pattern.setter
  def sampling_pattern(self, value):
    self._sampling_pattern = value
    self.m = len(value)

  def get_dims(self):
    """ Returns metadata.
    m is number of measurements per datapoint
    every image (signal) is n by n
    k is the number of data_generation points
    """
    return self.m, self.n, self.k


  def save_image_data(self, filename):
    if self.images is None:
      raise RuntimeError("No image_data created. Please run function load_images first.")

    np.save(filename, self.images)

    print(f"Image data_generation with shape {self.images.shape} saved to file {filename}.npy")


  def load_measurement_data(self, filename):
    self.measurements = np.load(filename)
    print(self.measurements.shape)
    k, self.m = self.measurements.shape

    if self.k is not None:
      if self.k != k:
        raise ValueError("Inconsistent data_generation set. Unequal number of datapoints.")
    else:
      self.k = k

    return self.measurements

  def save_measurement_data(self, filename):
    if self.measurements is None:
      raise RuntimeError("No measurement data_generation created. Please run function simulate_measurements first.")

    np.save(filename, self.measurements)
    print(f"Measurement data_generation with shape {self.measurements.shape} saved to file {filename}.npy")


  def load_data(self, foldername):
    """ Loads an entire data_generation set. That is both image data_generation and measurement data_generation; input and output."""
    pass


  def setup_sampling_pattern(self, ratio1, ratio2):
    """ Sets up sampling pattern keeping low frequencies and sampling uniformly random from higher frequencies.
    """
    # TODO: Fix input as follows: sampling_ratio, stoc_det_ratio (so that you determine the toatal no of samples
    #  and the ratio between inner circle and random samples outside)
    if self.images is None:
      raise RuntimeError("No images loaded. Please load images first.")

    n = self.n
    center = n/2
    ind = []
    r = (center*ratio1)**2

    # pick deterministically inner circle
    for i in range(n):
      for j in range(n):
        if (n/2 - i)**2 + (n/2 - j)**2 <= r:
          ind.append(i + j*n)

    # randomly pick the rest
    for i in range(round(self.N*ratio2)):
      stoc_ind = np.random.randint(low=0, high=n, size=2)
      if (n/2 - stoc_ind[0])**2 + (n / 2 - stoc_ind[1])**2 > r:
        ind.append((stoc_ind[0] + stoc_ind[1]*n))

    self.sampling_pattern = np.array(ind)
    self.sampling_ratio = len(ind)  / self.N
    print(f"Sampling ratio: {self.sampling_ratio}")


  def setup_sampling_pattern_1(self):
    self.sampling_pattern = np.array([i for i in range(self.N)])

  def save_sampling_pattern(self, filename):
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling scheme. Please run function setup_sampling_pattern.")

    np.save(filename, self.sampling_pattern)
    print(f"Sampling scheme with shape {self.sampling_pattern.shape} saved to file {filename}.npy")

  def save_sampling_pattern_matlab(self, filename):
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling scheme. Please run function setup_sampling_pattern.")

    savemat(filename, mdict={"Sampling_scheme": self.sampling_pattern})
    print(f"Sampling scheme with shape {self.sampling_pattern.shape} saved to file {filename}.npy")

  def load_sampling_pattern(self, filename):
    self.sampling_pattern = np.load(filename)

  def load_sampling_pattern_matlab(self, filename):
    workspace = loadmat(filename)
    if "sampling_pattern" not in workspace:
      print("Error: File loaded doesn't contain variable 'sampling_scheme'.")
    else:
      sampling_pattern = workspace["sampling_pattern"]
      sampling_pattern = sampling_pattern.reshape(sampling_pattern.shape[0]) # reshape to one dimension
      self.sampling_pattern = sampling_pattern

    self.workspace = workspace

  def simulate_measurements(self):
    """ Simulates subsampling with the given sampling pattern and DFT. """
    if self.images is None:
      raise RuntimeError("No images loaded. Please run function load_images.")

    self.measurements = self.PF(self.images)


  def PF(self, X):
    """ This is the (model of the) sampling operator. It computes  P*F*X.

    Uses self.sampling_pattern as sampling pattern.

    :param X: Input images, 3D-array of floats (dims: (batch size, height, width))
    :return: sub sampled fourier transform of the input image, a 1D-array of floats
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please run setup_sampling_pattern.")
    Y = np.fft.fft2(X)/self.n
    Y = np.fft.fftshift(Y, axes=(-2, -1))
    y = self.matlab_reshape(Y, (Y.shape[1]*Y.shape[2],))#Y.reshape((Y.shape[0], Y.shape[1]*Y.shape[2])) # This is th transpose of matlab indexing !
    y = y.take(self.sampling_pattern, axis=1)

    return y


  def PFT(self, y):
    """ This is the (model of the) adjoint of the sampling operator. It computes  (P x F)* x Y.

    Uses self.sampling_pattern as sampling pattern.

    :param y: measurements as 2D-array of floats with shape (batch size, no. of measurements)
    :return: noisy images as 3D-arrays of floats with shape (batch size, height, width)
    """
    if self.sampling_pattern is None:
      raise RuntimeError("No sampling pattern. Please run setup_sampling_pattern or load_sampling_pattern.")

    k, m = y.shape # get dims
    rep_sampling_pattern = self.sampling_pattern.reshape((1,) + self.sampling_pattern.shape)
    x = np.zeros((k, self.N), dtype=np.complex128)
    np.put_along_axis(x, rep_sampling_pattern, y, axis=1)
    X = self.matlab_reshape(x, (self.n, self.n)) #x.reshape((k, self.n, self.n))
    X = np.fft.ifftshift(X, axes=(1,2))
    X = np.fft.ifft2(X)*self.n

    return X

  def matlab_reshape(self, x, shape):
    x_new = np.zeros((x.shape[0],) + shape, dtype=np.complex128)
    for i, datapoint in enumerate(x):
      x_new[i] = datapoint.reshape(shape, order=self.__class__.MATLAB_MATRIX_INDEXING)

    return x_new

  def get_train_test_data(self, ratio):
    if self.images is None:
      raise RuntimeError("No images.")
    if self.measurements is None:
      raise RuntimeError("No measurements")

    k_test = round(self.k*ratio)
    images_test = self.images[:k_test-1]
    measurements_test = self.measurements[:k_test-1]
    images_train = self.images[k_test:]
    measurements_train = self.measurements[k_test:]

    self.training_images = images_train
    self.training_measurements = measurements_train
    self.test_images = images_test
    self.test_measurements = measurements_test

    return measurements_train, images_train, measurements_test, images_test



  def save_train_test(self, folder, ratio):
    """ This function saves four files: training signals, training measurements, test signals and test measurements.

    :param n_of_test_points: Number of test data points, integer.
    """
    # make file names
    x_train_file = join(folder, "measurements_train")
    y_train_file = join(folder, "signals_train")
    x_test_file = join(folder, "measurements_test")
    y_test_file = join(folder, "signals_test")
    sample_scheme_filename = join(folder, "sampling_scheme.mat")

    # split data
    x_train, y_train, x_test, y_test = self.get_train_test_data(ratio)

    # save data
    np.save(x_train_file, x_train)
    print(f"Training measurements with shape {x_train.shape} saved to file '{x_train_file}'.")
    np.save(y_train_file, y_train)
    print(f"Training images with shape {y_train.shape} saved to file '{y_train_file}'.")
    np.save(x_test_file, x_test)
    print(f"Testing measurements with shape {x_test.shape} saved to file '{x_test_file}'.")
    np.save(y_test_file, y_test)
    print(f"Testing images with shape {y_test.shape} saved to file '{y_test_file}'.")
    savemat(sample_scheme_filename, mdict=self.workspace)
    print(f"Sampling scheme with shape {self.workspace['sampling_pattern'].shape} saved to file '{sample_scheme_filename}'.")


  def save_all_matlab(self, filename, ratio):
    # split data
    x_train, y_train, x_test, y_test = self.get_train_test_data(ratio)

    workspace = {"Images_train": y_train, "Measurements_train": x_train,  "SamplingScheme": self.sampling_pattern, "Images_test": y_test, "Measurements_test": x_test}

    savemat(filename, mdict=workspace)
    print(f"Training images with shape {y_train.shape} saved to file {filename}")
    print(f"Training measurements with shape {x_train.shape} saved to file {filename}")
    print(f"Test images with shape {y_test.shape} saved to file {filename}")
    print(f"Test measurements with shape {x_test.shape} saved to file {filename}")
    print(f"Sampling scheme with shape {self.sampling_pattern.shape} saved to file {filename}")

