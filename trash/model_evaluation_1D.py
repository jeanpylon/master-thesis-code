from trash.data_manager import DataManager
from lib.data_generation.evaluation import Evaluation
from tensorflow.keras.models import load_model
from lib.utilities import *

# Load data_generation and set hyper parameters
data_folder = "data_generation/m80_N128_s20_k100000"
data = DataManager()
A = data.load_modality(data_folder + "/modality.npy") # Sampling modality
data.load_measurements(data_folder + "/measurements.npy") # Input in recovery process
data.load_signals(data_folder + "/signals.npy") # Output from recovery process
m, N, k = data.get_dims() # m: no of entries in measurements, N: no of entries in signal, k: no of signals/measurements
test_ratio = .2
k_test = round(test_ratio*k)
k_train = k - k_test
#model_path = "training_output/pseudo_inv_dense_nn_recovery_epochs_200000__layers_8"
model_path = "training_output/pseudo_inv_ELL50_recovery_1D_new_epochs_10000__channels_64"

# Compute pseudo inverse
# Note: As the matrices treated in this module are expected to be real,
# we only need to transpose instead of conjugate transpose.
A_dagger = np.linalg.pinv(A)

# split data_generation
X_train, Y_train, X_test, Y_test = data.get_train_test_data(test_ratio)
print(f"Dimensions of training input: {X_train.shape}")
print(f"Dimensions of training output: {Y_train.shape}")
print(f"Dimensions of testing input: {X_test.shape}")
print(f"Dimensions of testing output: {Y_test.shape}")

# load network
print("loading model ...")
DNN = load_model(model_path)

### Evaluate on training set
print("Evaluating model on training set")
# recover with (DNN) o (A_dagger) on training set
DNN_X_train = np.matmul(A_dagger, X_train.T).T
DNN_X_train_reshape = DNN_X_train.reshape(DNN_X_train.shape + (1,1))
Y_star_train = DNN.predict(DNN_X_train_reshape)
Y_star_train_reshape = Y_star_train.reshape(Y_star_train.shape[0:2])
"""
# Compute error training set
total_MSE_train = Evaluation.MSE_big(Y_train, Y_star_train_reshape)
psnr_train = Evaluation.PSNR_big(Y_train, Y_star_train_reshape)
print(f"MSE of all entries of all recovered training signals: {total_MSE_train}")
print(f"PSNR of all training signals treated as one signal: {psnr_train}")

# keras' error metric training set
#print(f"Keras evaluation on training set: {DNN.evaluate(DNN_X_train, Y_train)}")

### Evaluate on test set
"""
print("Evaluating model on test set")
# recover with (DNN) o (A_dagger) on test set
DNN_X_test = np.matmul(A_dagger, X_test.T).T
DNN_X_test_reshape = DNN_X_test.reshape(DNN_X_test.shape + (1,1))
Y_star_test = DNN.predict(DNN_X_test_reshape)
Y_star_test_reshape = Y_star_test.reshape(Y_star_test.shape[0:2])
"""
# Compute error test set
total_MSE_test = Evaluation.MSE_big(Y_test, Y_star_test_reshape)
psnr_test = Evaluation.PSNR_big(Y_test, Y_star_test_reshape)
print(f"MSE of all entries of all recovered test signals: {total_MSE_test}")
print(f"PSNR of all test signals treated as one signal: {psnr_test}")
"""

# Training set PSNR function
PSNR_threhold = 30
print("Values for training set:")
Evaluation.k_random_PSNR(10, Y_train, Y_star_train_reshape)
valid_psnr = Evaluation.count_valid_PSNR(PSNR_threhold, Y_train, Y_star_train_reshape)
print(f"{valid_psnr/len(Y_train)*100}%  of recovered training signals hae PSNR >= 30.")
#Evaluation.plot_12_random(Y_train, Y_star_train_reshape)

# Test set PSNR
print("Values for test set:")
Evaluation.k_random_PSNR(10, Y_test, Y_star_test_reshape)
valid_psnr = Evaluation.count_valid_PSNR(PSNR_threhold, Y_test, Y_star_test_reshape)
print(f"{valid_psnr/len(Y_test)*100}%  of recovered test signals hae PSNR >= 30.")
#Evaluation.plot_12_random(Y_test, Y_star_test_reshape)

# keras' error metric test set
#print(f"Keras evaluation on test set: {DNN.evaluate(DNN_X_test, Y_test)}")

# plot one comparison on test set
#for i in range(k):
#  Evaluation.plot_signal_comparison(Y_star_test_reshape[i], Y_test[i])

