import pywt
from imageio import imread
import matplotlib.pyplot as plt
import numpy as np

image_path = "/home/jab/Skole/Master/data/data_sets/2d/mri_128/images_test.npy"
matrix_path = "/home/jab/Skole/Master/data/matrices/16384x16384_hadamard_matrix.npy"
sampling_rate = .1

image = np.load(image_path)[0]
n = image.shape[0]
#sampling_pattern = np.random.choice(n, round(n*sampling_rate))
#matrix = np.load(matrix_path)[sampling_pattern]
#image_flat = image.reshape((n**2,))
#measurements = np.matmul(matrix, image_flat)

wavelet_transform = pywt.wavedec2(image, "db4")

transformed_image, indecies = pywt.coeffs_to_array(wavelet_transform)


"""
for i, detail in enumerate(details):
  print(f"Length of details #{i}: {len(detail)}")
  print(f"Shape og horisontal detail: {detail[0].shape}")
  print(f"Shape og vertical detail: {detail[1].shape}")
  print(f"Shape og diagonal detail: {detail[2].shape}")

  upper_part = np.concatenate([transformed_image, detail[0]], axis=-1)
  lower_part = np.concatenate([detail[1], detail[2]], axis=-1)
  transformed_image = np.concatenate([upper_part, lower_part], axis=-2)

"""
print(transformed_image.shape)
back_transform = pywt.array_to_coeffs(transformed_image, indecies, "wavedec2")
back = pywt.waverec2(back_transform, "db4")
plt.imshow(np.concatenate([back, image], axis=-1))
plt.show()

