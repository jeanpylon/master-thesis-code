from lib.algorithms.fista_2D import FISTA2D
from lib.data_generation.fourier_sampling import FourierSampling
import pywt

data_folder = "mri_512"
data = FourierSampling()

data.load_measurement_data("data_generation/" + data_folder + "/measurements.npy") # Input in recovery process
data.load_image_data("data_generation/" + data_folder + "/signals.npy") # Output from recovery process
data.load_sampling_pattern("data_generation/" + data_folder + "/sampling_scheme.npy") # load sampling scheme (vector)
m, n, k = data.get_dims() # m: no of entries in measurements, n: images have size n*n, k: no of signals/measurements
print(f"k: {k}, m: {m}, n: {n}")
test_ratio = .2
K = 10
k_test = round(test_ratio*k)
k_train = k - k_test

# split data_generation
X_train, Y_train, X_test, Y_test = data.get_train_test_data(test_ratio)

# smaller batch
X_train = X_train[0:K]
Y_train = Y_train[0:K]
X_test = X_test[0:round(K*test_ratio)]
Y_test = Y_test[0:round(K*test_ratio)]

print(f"Dimensions of training input: {X_train.shape}")
print(f"Dimensions of training output: {Y_train.shape}")
print(f"Dimensions of testing input: {X_test.shape}")
print(f"Dimensions of testing output: {Y_test.shape}")

# doing recovery with FISTA 2D
fista = FISTA2D(data.PF, data.PFT)
wavedec_recovery = fista.recover(X_train, iterations=1)
wavedec_recovery = fista.array_to_dec(wavedec_recovery)
rec_img = pywt.waverec2(wavedec_recovery, wavelet="db4")
print(rec_img.shape)

### Testing
"""
import pywt
import matplotlib.pyplot as plt

img = Y_train[0:1]
print(f"Shape of img: {img.shape}")
dec_img = pywt.wavedec2(img, "db4", mode="periodization")
for coff in dec_img:
  print(type(coff))
  print(coff)

rec_img = pywt.waverec2(dec_img, "db4", mode="periodization")
plt.clf()
plt.imshow(rec_img[0])
plt.show()
"""
