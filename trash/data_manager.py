""" """


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

class DataManager:
  def __init__(self):
    self.signals = None
    self.modality = None
    self.measurements = None
    self.m = None # number of entries in measurements
    self.N = None # number of entries in signals
    self.k = None # number of signals/measurements

    self.training_signals = None
    self.training_measurements = None
    self.test_signals = None
    self.test_measurements = None
    self.test_train_ratio = None

  def get_signals(self):
    return self.signals

  def get_modality(self):
    return self.modality

  def get_measurements(self):
    return self.measurements

  def get_dims(self):
    """ Returns the following:
    m = number entries in each measurement
    N = number of entries in each signal
    k = number og signals/measurements
    """
    return self.m, self.N, self.k

  def load_signals(self, signals_file):
    """ Loads signals (training output) file in .npy format.

    Note: Expects signals file to contain matrix of dim k * N, where k is the number of signals and N is signal length.
    :param signals_file: The path and filename of the .npy file containing the signals matrix
    :return: The loaded signal matrix
    """
    # load file
    self.signals = np.load(signals_file, allow_pickle=False)
    print(f"Signals matrix successfully loaded. Shape: {self.signals.shape}")
    k, N = self.signals.shape

    # check for inconsistencies
    if self.k is not None:
      if self.k != k:
        print("Warning: Variable k was already set to something different from file loaded.")
    if self.N is not None:
      if self.N != N:
        print("Warning: Variable N was already set to something different from file loaded.")

    # update metadata
    self.k = k
    self.N = N

    return self.signals


  def load_measurements(self, measurements_file):
    """ Loads measurements (training input) file in .npy format.

    Note: Expects measurements file to contain matrix of dim k * m, where k is the number of measurements
    and m is measurement length.
    :param measurements_file: The path and filename of the .npy file containing the measurements matrix
    :return: The loaded measurements
    """
    # load file
    self.measurements = np.load(measurements_file, allow_pickle=False)
    print(f"Measurements matrix successfully loaded. Shape: {self.measurements.shape}")
    k, m = self.measurements.shape

    # check for inconsistencies
    if self.k is not None:
      if self.k != k:
        print("Warning: Variable k was already set to something different from file loaded.")
    if self.m is not None:
      if self.m != m:
        print("Warning: Variable m was already set to something different from file loaded.")

    # update metadata
    self.k = k
    self.m = m

    return self.measurements


  def load_modality(self, modality_filename):
    """ Loads the .npy file containing the modality.

    :param modality_filename: The path and filename of the .npy file containing th measurement matrix (modality)
    :return: The loaded modality
    """
    # load file
    self.modality = np.load(modality_filename, allow_pickle=False)
    print(f"Measurement matrix successfully loaded. Shape: {self.modality.shape}")
    m, N = self.modality.shape

    # check for inconsistencies
    if self.m is not None:
      if self.m != m:
        print("Warning: Variable m was already set to something different from file loaded.")
    if self.N is not None:
      if self.N != N:
        print("Warning: Variable N was already set to something different from file loaded.")

    # update metadata
    self.m = m
    self.N = N

    return self.modality


  def get_train_test_data(self, ratio):
    """
    Splits signals and measurements into training and test batches with the given ratio. Stores and returns the result.

    (Expects self.signals to have rows as signals and self.measurements to have rows as measurements.)

    :param ratio: The percentage of signals and measurements to become test-data_generation. 1 - ratio then is the percentage of
                  the data_generation that becomes training data_generation.
    :return: measurements for training, corresponding signals for training,
             measurements for testing, corresponding signals for testing
    """
    if self.signals is None:
      raise RuntimeError("No signals loaded. Unable to perform split.")
    if self.measurements is None:
      raise RuntimeError("No measurements loaded. Unable to perform split.")
    if not ratio <= 1 or not ratio >=0:
      raise ValueError("Argument ratio must be a number in the interval [0,1].")

    k_test = round(self.k*ratio)
    signals_test = self.signals[:k_test-1,:]
    measurements_test = self.measurements[:k_test-1,:]
    signals_train = self.signals[k_test:,:]
    measurements_train = self.measurements[k_test:,:]

    self.training_signals = signals_train
    self.training_measurements = measurements_train
    self.test_signals = signals_test
    self.test_measurements = measurements_test

    return measurements_train, signals_train, measurements_test, signals_test


  def verify(self, plot_errors=False):
    """ Computes and prints the total error.

    :param plot_errors: Plot the errors as a surface if this is set to true
    :return: the sum of the squared errors over all entries over all measurements.
    """
    if self.signals is None:
      print("Error: No signals loaded.")
      return None
    if self.modality is None:
      print("Error: No modality loaded.")
      return None
    if self.measurements is None:
      print("Error: No measurements loaded.")
      return None

    print("Signals shape: " + str(self.signals.shape))
    print("Modality shape: " + str(self.modality.shape))
    print("Measurements shape: " + str(self.measurements.shape))
    error = np.matmul(self.modality, self.signals.T).T - self.measurements

    print(f"Sum of squared errors over all measurement entries over all measurements: {np.sum(error ** 2)}")

    if plot_errors:
      # plot the errors as a surface
      k = error.shape[0]
      m = error.shape[1]
      x = np.linspace(0, k - 1, num=k)
      y = np.linspace(0, m - 1, num=m)
      x, y = np.meshgrid(y, x)
      z = error

      fig = plt.figure()
      ax = fig.gca(projection='3d')
      ax.plot_surface(x, y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
      plt.show()

    return error

  def A(self, x):
    """ Multiplies the sampling modality with the given input.

    :param x: input array, N long 1d-array of real numbers
    :return: y=Ax
    """
    if self.modality is None:
      raise RuntimeError("No modality loaded or generated.")

    return np.matmul(self.modality, x)

  def A_pseudo(self, x):
    """ Multiplies the pseudo inverse of the sampling modality with the given input.

    NB: Works only for real numbers.

    :param x: input array, N long 1d-array of real numbers
    :return: y=A_dagger*x
    """
    if self.modality is None:
      raise RuntimeError("No modality loaded or generated.")

    return np.matmul(np.linalg.pinv(self.modality), x)
