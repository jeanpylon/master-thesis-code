print("Loading modules ...")
from lib.models.ell50_1D import setup_ELL50_1D
from lib.utilities import *
from lib.data_generation.fourier_sampling import FourierSampling
#import matplotlib.pyplot as plt
import numpy as np

# Load data_generation and set hyper parameters

data_folder = "/home/jab/Skole/Master/data/data_sets/1D/random_s_sparse"
epochs = 10 # number of epochs of training
channels = 64
batch_size = 10
name = "pseudo_inv_ELL50_recovery_1D"
model_path = "training_output"

signals_file = os.path.join(data_folder, "signals_train.npy")
measurements_file = os.path.join(data_folder, "measurements_train.npy")
sampling_modality_file = os.path.join(data_folder, "modality.npy")

print("Loading training data ...")
signals = np.load(signals_file)
print(f"Signals array with shape {signals.shape} loaded from file '{signals_file}'.")
measurements = np.load(measurements_file)
print(f"Measurements array with shape {measurements.shape} loaded from file '{measurements_file}'.")
A = np.load(sampling_modality_file)
print(f"Sampling modality with shape {A.shape} laode from file '{sampling_modality_file}'.")

k, N, dummy = signals.shape
k, m, dummy = measurements.shape
print(f"k: {k}, m: {m}, n: {N}")

print("Setting up network ...")
# set up ELL50 CNN
CNN = setup_ELL50_1D(N)

# train DNN
print(measurements.shape)

A_dagger = np.linalg.pinv(A)
print(A_dagger.shape)

# computing noisy signals
CNN_X_train = np.matmul(A_dagger, measurements.T).T
print(CNN_X_train.shape)
CNN_X_train = CNN_X_train.reshape(CNN_X_train.shape + (1,))
print(CNN_X_train.shape)

Y_train = signals.reshape(signals.shape + (1,))
print(Y_train.shape)

history = CNN.fit(CNN_X_train, Y_train, epochs=epochs, batch_size=batch_size)

# save model and trainingplot
model_saved_to, plot_saved_to = save_training(CNN, name, history.history["loss"], "MSE", model_path, data_folder, "_channels_" + str(channels))

