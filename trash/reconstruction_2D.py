print("Loading libraries ...")
from lib.utilities import *
from lib.data_generation.fourier_sampling import FourierSampling
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
from lib.data_generation.evaluation import Evaluation
import os
import numpy as np

# set parameters
data_folder = "/home/jab/Skole/Master/data/data_sets/2D/random_dots_512_001"
#data_folder = "../data_sets/2D/mri_512"
model_path = "/home/jab/Skole/Master/models/transpose_ELL50_2D_epochs_1000__channels_64"
#model_path = "training_output/pseudo_inv_ELL50_recovery_2D_complex_epochs_100__channels_64"
reconstruction_path = "/home/jab/Skole/Master/data/reconstructions/2D/random_dots_512_001/mri_net"

#region Functions
def recon_serial(measurements):
  recovered_images = []
  for i in range(len(measurements)):
    CNN_X_train = data.PFT(measurements[i:i + 1])
    CNN_X_train_real = CNN_X_train.real
    CNN_X_train_img = CNN_X_train.imag

    CNN_X_train_real = CNN_X_train_real.reshape(CNN_X_train_real.shape + (1,))
    CNN_X_train_img = CNN_X_train_img.reshape(CNN_X_train_img.shape + (1,))

    CNN_X_train = np.concatenate([CNN_X_train_real, CNN_X_train_img], axis=-1)

    pred = CNN.predict(CNN_X_train)
    pred = pred.reshape(pred.shape[1:4])

    Y_train_star = special_abs(pred[:,:,0], pred[:,:,1])
    recovered_images.append(Y_train_star)

    print(f"Image {i + 1} / {len(measurements)} recovered.")

  return np.array(recovered_images)


def special_abs(real, img):
  return np.sqrt(real**2 + img**2)
#endregion

if not os.path.isdir(reconstruction_path):
  raise ValueError(f"Folder '{reconstruction_path}' does not exist.")

signals_file = os.path.join(data_folder, "signals_test.npy")
measurements_file = os.path.join(data_folder, "measurements_test.npy")
sampling_pattern_file = os.path.join(data_folder, "sampling_scheme.mat")
reconstruction_file = os.path.join(reconstruction_path, "reconstructed_images")

print("Instantiating class FourierSampling ...")
data = FourierSampling()

print("Loading training data ...")
signals = np.load(signals_file)
print(f"Signals with shape {signals.shape} loaded from file '{signals_file}'.")
measurements = np.load(measurements_file)
print(f"Measurements with shape {measurements.shape} loaded from file '{measurements_file}'.")
data.load_sampling_pattern_matlab(sampling_pattern_file) # load sampling scheme (vector)
print(f"Sampling pattern with shape {data.sampling_pattern.shape} loaded from file '{sampling_pattern_file}'.")

k, dummy, n = signals.shape
k, m = measurements.shape
data.m = m
data.n = n
data.k = k
print(f"k: {k}, m: {m}, n: {n}")

print("Setting up network ...")
# set up ELL50 CNN
CNN = load_model(model_path)

### Evaluate on training set
print("Evaluating model")
# recover with (DNN) o (A_T) on training set

images = recon_serial(measurements)
np.save(reconstruction_file, images)
print(f"{len(images)} recovered images with shape {images.shape} saved to file '{reconstruction_file}'.")



