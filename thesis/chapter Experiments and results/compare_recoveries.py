"""
Creates a figure of the desired number of images with their NN and FISTA recoveries.
"""

import matplotlib.pyplot as plt
import numpy as np
from os.path import join, expanduser

# parameters
k = 8

save_file_name = "/home/jab/Skole/Master/Figurer/chapter Experiments and results/Recovered images test.png"
org_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/images_test.npy")
FBPConvNet_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_11/recoveries_test.npy")
LISTA_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_16/recoveries_test.npy")
LISTAss_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_21/recoveries_test.npy")
LISTAcp_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_28/recoveries_test.npy")
LISTAcpss_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_37/recoveries_test.npy")
FISTA_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_2/recoveries_test.npy")

# load images
Org_img = np.load(org_file_name)
FBPCN_img = np.load(FBPConvNet_file_name)
LISTA_img = np.load(LISTA_file_name)
LISTAss_img = np.load(LISTAss_file_name)
LISTAcp_img = np.load(LISTAcp_file_name)
LISTAcpss_img = np.load(LISTAcpss_file_name)
FISTA_img = np.load(FISTA_file_name)


# make figure
columns = 3
rows = 3
fig, ax = plt.subplots(rows, columns)

# Row 0
ax[0, 1].imshow(abs(Org_img[k]), cmap="gray", vmin=0, vmax=1)
ax[0, 0].axis("off")
ax[0, 1].axis("off")
ax[0, 2].axis("off")

# Row 1
ax[1, 0].imshow(abs(FBPCN_img[k]), cmap="gray", vmin=0, vmax=1)
ax[1, 0].axis("off")
ax[1, 1].imshow(abs(LISTA_img[k]), cmap="gray", vmin=0, vmax=1)
ax[1, 1].axis("off")
ax[1, 2].imshow(abs(LISTAss_img[k]), cmap="gray", vmin=0, vmax=1)
ax[1, 2].axis("off")

# Row 2
ax[2, 0].imshow(abs(LISTAcp_img[k]), cmap="gray", vmin=0, vmax=1)
ax[2, 0].axis("off")
ax[2, 1].imshow(abs(LISTAcpss_img[k]), cmap="gray", vmin=0, vmax=1)
ax[2, 1].axis("off")
ax[2, 2].imshow(abs(FISTA_img[k]), cmap="gray", vmin=0, vmax=1)
ax[2, 2].axis("off")

# add text
#ax[0, 0].set_title("Original images")
#ax[0, 1].set_title("Recovery with LISTAcpss")
#ax[0, 2].set_title("Recovery with FISTA")

# fix layout
fig.tight_layout(h_pad=.2, w_pad=.1)
plt.subplots_adjust(left=.05 ,right=.555, wspace=0)

plt.get_current_fig_manager().full_screen_toggle()
plt.show()
plt.savefig(save_file_name)