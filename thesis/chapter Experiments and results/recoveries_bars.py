"""
Plots bars showing performance on 25 sample images.
"""

import numpy as np
from lib.data_generation.evaluation import Evaluation
import matplotlib.pyplot as plt
from os.path import join, expanduser

# parameters
k = 15
org_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/images_test.npy")
ELL50_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_11/recoveries_test.npy")
LISTA_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_16/recoveries_test.npy")
LISTAss_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_21/recoveries_test.npy")
LISTAcp_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_28/recoveries_test.npy")
LISTAcpss_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_37/recoveries_test.npy")
FISTA_file_name = join(expanduser("~"), "Skole/Master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_2/recoveries_test.npy")
save_path = join(expanduser("~"), "Skole/Master/Figurer/chapter Experiments and results/")

# load images
org_img = np.load(org_file_name)
print(f"Original images test set with shape {org_img.shape} loaded from file '{org_file_name}'.")
ELL50_images = np.load(ELL50_file_name)
print(f"ELL50 recoveries of test set with shape {ELL50_images.shape} loaded from file '{ELL50_file_name}'.")
LISTA_images = np.load(LISTA_file_name)
print(f"LISTA recoveries of test set with shape {LISTA_images.shape} loaded from file '{LISTA_file_name}'.")
LISTAss_images = np.load(LISTAss_file_name)
print(f"LISTA recoveries of test set with shape {LISTAss_images.shape} loaded from file '{LISTAss_file_name}'.")
LISTAcp_images = np.load(LISTAcp_file_name)
print(f"LISTA recoveries of test set with shape {LISTAcp_images.shape} loaded from file '{LISTAcp_file_name}'.")
LISTAcpss_images = np.load(LISTAcpss_file_name)
print(f"LISTA recoveries of test set with shape {LISTAcpss_images.shape} loaded from file '{LISTAcpss_file_name}'.")
FISTA_images = np.load(FISTA_file_name)
print(f"ELL50 recoveries of test set with shape {FISTA_images.shape} loaded from file '{FISTA_file_name}'.")

# compute stats
ELL50_psnrs = Evaluation.PSNR_2D(org_img, ELL50_images)
LISTA_psnrs = Evaluation.PSNR_2D(org_img, LISTA_images)
LISTAss_psnrs = Evaluation.PSNR_2D(org_img, LISTAss_images)
LISTAcp_psnrs = Evaluation.PSNR_2D(org_img, LISTAcp_images)
LISTAcpss_psnrs = Evaluation.PSNR_2D(org_img, LISTAcpss_images)
FISTA_psnrs = Evaluation.PSNR_2D(org_img, FISTA_images)

ELL50_sup_norm = Evaluation.sup_norm_2D(org_img, ELL50_images)
LISTA_sup_norm = Evaluation.sup_norm_2D(org_img, LISTA_images)
LISTAss_sup_norm = Evaluation.sup_norm_2D(org_img, LISTAss_images)
LISTAcp_sup_norm = Evaluation.sup_norm_2D(org_img, LISTAcp_images)
LISTAcpss_sup_norm = Evaluation.sup_norm_2D(org_img, LISTAcpss_images)
FISTA_sup_norm = Evaluation.sup_norm_2D(org_img, FISTA_images)

N = len(ELL50_images)
indices = np.random.choice(N, k)

# make file names
psnr_file_name = join(save_path, "psnr_plot_all_five.png")
sup_norm_file_name = join(save_path, "sup_norm_plot_all_five.png")

# plot PSNRs test set
plt.clf()
X = np.arange(k) * 1.75

offset = 0
print(ELL50_psnrs.shape)
plt.bar(X + offset, ELL50_psnrs[indices], color="b", width=0.25, label="FBPConvNet")
offset = .25
plt.bar(X + offset, LISTA_psnrs[indices], color="g", width=0.25, label="LISTA")
offset = .5
plt.bar(X + offset, LISTAss_psnrs[indices], color="r", width=0.25, label="LISTAss")
offset = .75
plt.bar(X + offset, LISTAcp_psnrs[indices], color="c", width=0.25, label="LISTAcp")
offset = 1
plt.bar(X + offset, LISTAcpss_psnrs[indices], color="m", width=0.25, label="LISTAcpss")
offset = 1.25
plt.bar(X + offset, FISTA_psnrs[indices], color="y", width=0.25, label="FISTA")

plt.legend()
plt.ylabel("Peak signal noise ratio")
plt.xlabel("Images")
plt.xticks([])
plt.title("PSNRs of reconstructions on the test set")
plt.get_current_fig_manager().full_screen_toggle()
plt.savefig(psnr_file_name)
plt.show()

# plot sup-norms test set
plt.clf()
offset = 0
plt.bar(X + offset, ELL50_sup_norm[indices], color="b", width=0.25, label="FBPConvNet")
offset = .25
plt.bar(X + offset, LISTA_sup_norm[indices], color="g", width=0.25, label="LISTA")
offset = .5
plt.bar(X + offset, LISTAss_sup_norm[indices], color="r", width=0.25, label="LISTAss")
offset = .75
plt.bar(X + offset, LISTAcp_sup_norm[indices], color="c", width=0.25, label="LISTAcp")
offset = 1
plt.bar(X + offset, LISTAcpss_sup_norm[indices], color="m", width=0.25, label="LISTAcpss")
offset = 1.25
plt.bar(X + offset, FISTA_sup_norm[indices], color="y", width=0.25, label="FISTA")

plt.legend()
plt.ylabel("Sup-norm error")
plt.xlabel("Images")
plt.xticks([])
plt.title("Sup-norm errors of reconstrucions on the test set")
plt.get_current_fig_manager().full_screen_toggle()
plt.savefig(sup_norm_file_name)
plt.show()



