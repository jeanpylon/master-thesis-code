"""
Computes statistics on the recoveries by ELL50, LISTA, LISTAcp, LISTAcpss and FISTA
"""

import numpy as np
from os.path import expanduser, join
from lib.data_generation.evaluation import Evaluation

# parameters
org_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/images_test.npy")
ELL50_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_11/recoveries_test.npy")
LISTA_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_16/recoveries_test.npy")
LISTAss_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_21/recoveries_test.npy")
LISTAcp_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_28/recoveries_test.npy")
LISTAcpss_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_37/recoveries_test.npy")
FISTA_file_name = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_2/recoveries_test.npy")
FISTA_file_name_long = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/recoveries_3/recoveries_test.npy")
save_file = join(expanduser("~"), "master/extra_data/data_sets/2d/random_dots_64/measurements_4/stats.txt")

# load images
org_img = np.load(org_file_name)
print(f"Original images test set with shape {org_img.shape} loaded from file '{org_file_name}'.")
ELL50_images = np.load(ELL50_file_name)
print(f"ELL50 recoveries of test set with shape {ELL50_images.shape} loaded from file '{ELL50_file_name}'.")
LISTA_images = np.load(LISTA_file_name)
print(f"LISTA recoveries of test set with shape {LISTA_images.shape} loaded from file '{LISTA_file_name}'.")
LISTAss_images = np.load(LISTAss_file_name)
print(f"LISTA recoveries of test set with shape {LISTAss_images.shape} loaded from file '{LISTAss_file_name}'.")
LISTAcp_images = np.load(LISTAcp_file_name)
print(f"LISTA recoveries of test set with shape {LISTAcp_images.shape} loaded from file '{LISTAcp_file_name}'.")
LISTAcpss_images = np.load(LISTAcpss_file_name)
print(f"LISTA recoveries of test set with shape {LISTAcpss_images.shape} loaded from file '{LISTAcpss_file_name}'.")
FISTA_images = np.load(FISTA_file_name)
print(f"FISTA recoveries of test set with shape {FISTA_images.shape} loaded from file '{FISTA_file_name}'.")
FISTA_long_images = np.load(FISTA_file_name_long)
print(f"FISTA recoveries of test set with shape {FISTA_long_images.shape} loaded from file '{FISTA_file_name_long}'.")

# compute stats
ELL50_psnrs = Evaluation.PSNR_2D(org_img, ELL50_images)
LISTA_psnrs = Evaluation.PSNR_2D(org_img, LISTA_images)
LISTAss_psnrs = Evaluation.PSNR_2D(org_img, LISTAss_images)
LISTAcp_psnrs = Evaluation.PSNR_2D(org_img, LISTAcp_images)
LISTAcpss_psnrs = Evaluation.PSNR_2D(org_img, LISTAcpss_images)
FISTA_psnrs = Evaluation.PSNR_2D(org_img, FISTA_images)
FISTA_long_psnrs = Evaluation.PSNR_2D(org_img, FISTA_long_images)

ELL50_sup_norm = Evaluation.sup_norm_2D(org_img, ELL50_images)
LISTA_sup_norm = Evaluation.sup_norm_2D(org_img, LISTA_images)
LISTAss_sup_norm = Evaluation.sup_norm_2D(org_img, LISTAss_images)
LISTAcp_sup_norm = Evaluation.sup_norm_2D(org_img, LISTAcp_images)
LISTAcpss_sup_norm = Evaluation.sup_norm_2D(org_img, LISTAcpss_images)
FISTA_sup_norm = Evaluation.sup_norm_2D(org_img, FISTA_images)
FISTA_long_sup_norm = Evaluation.sup_norm_2D(org_img, FISTA_long_images)

# print stats
print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("", "ELL50", "LISTA", "LISTAss", "LISTAcp", "LISTAcpss", "FISTA 1000 it", "FISTA 5000 it"))

print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("Min PSNR", ELL50_psnrs.min(), LISTA_psnrs.min(), LISTAss_psnrs.min(), LISTAcp_psnrs.min(), LISTAcpss_psnrs.min(), FISTA_psnrs.min(), FISTA_long_psnrs.min()))
print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("Max PSNR", ELL50_psnrs.max(), LISTA_psnrs.max(), LISTAss_psnrs.max(), LISTAcp_psnrs.max(), LISTAcpss_psnrs.max(), FISTA_psnrs.max(), FISTA_long_psnrs.max()))
print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("Mean PSNR", ELL50_psnrs.mean(), LISTA_psnrs.mean(), LISTAss_psnrs.mean(), LISTAcp_psnrs.mean(), LISTAcpss_psnrs.mean(), FISTA_psnrs.mean(), FISTA_long_psnrs.mean()))

print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("Min sup-n-error", ELL50_sup_norm.min(), LISTA_sup_norm.min(), LISTAss_sup_norm.min(), LISTAcp_sup_norm.min(), LISTAcpss_sup_norm.min(), FISTA_sup_norm.min(), FISTA_long_sup_norm.min()))
print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("Max sup-n-error", ELL50_sup_norm.max(), LISTA_sup_norm.max(), LISTAss_sup_norm.max(), LISTAcp_sup_norm.max(), LISTAcpss_sup_norm.max(), FISTA_sup_norm.max(), FISTA_long_sup_norm.max()))
print("{:<15} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format("Mean sup-n-error", ELL50_sup_norm.mean(), LISTA_sup_norm.mean(), LISTAss_sup_norm.max(), LISTAcp_sup_norm.mean(), LISTAcpss_sup_norm.mean(), FISTA_sup_norm.mean(), FISTA_long_sup_norm.mean()))

# save stats
psnrs = [ELL50_psnrs, LISTA_psnrs, LISTAss_psnrs, LISTAcp_psnrs, LISTAcpss_psnrs, FISTA_psnrs]
sup_norms = [ELL50_sup_norm, LISTA_sup_norm, LISTAss_sup_norm, LISTAcp_sup_norm, LISTAcpss_sup_norm, FISTA_sup_norm]

text = ""
text += "        Min PSNR "
for psnr in psnrs:
  text += "& {:.2f} ".format(psnr.min())
text += "\n"
text += "        Max PSNR "
for psnr in psnrs:
  text += "& {:.2f} ".format(psnr.max())
text += "\n"
text += "        Mean PSNR "
for psnr in psnrs:
  text += "& {:.2f} ".format(psnr.mean())
text += "\n"

text += "        Min sup-norm "
for sup_norm in sup_norms:
  text += "& {:.2E} ".format(sup_norm.min())
text += "\n"
text += "        Max sup-norm "
for sup_norm in sup_norms:
  text += "& {:.2E} ".format(sup_norm.max())
text += "\n"
text += "        Mean sup-norm "
for sup_norm in sup_norms:
  text += "& {:.2E} ".format(sup_norm.mean())

with open(save_file, "w") as file:
  file.write(text)
  print("Following text was written to file:")
  print("'''")
  print(text)
  print("'''")


