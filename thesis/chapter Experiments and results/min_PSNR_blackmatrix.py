import numpy as np
from lib.data_generation.evaluation import Evaluation
n = 64
k = 7

image_rec = np.zeros((1, n,n))
image_org = np.zeros((1, n,n))
indices_0 = np.zeros((k,), dtype=np.int)
indices_1 = np.random.choice(n, k)
indices_2 = np.random.choice(n, k)

for i in zip(indices_0, indices_1, indices_2):
  image_org[i[0], i[1], i[2]] = 1

print(f"PSNR of black image: {Evaluation.PSNR_2D(image_org, image_rec)}")
