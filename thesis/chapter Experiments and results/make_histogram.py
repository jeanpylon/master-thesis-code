"""
Makes histogram on sup-norm distribution.
"""

import numpy as np
from lib.data_generation.evaluation import Evaluation
import matplotlib.pyplot as plt

# parameters
org_file_name_train = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_large/excerpt_1/images_train_excerpt.npy"
nn_file_name_train = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_large/excerpt_1/measurements_1/recoveries_5/recoveries_train_excerpt.npy"
fista_file_name_train = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_large/excerpt_1/measurements_1/recoveries_6/recoveries_test_excerpt.npy" # DUMMY
org_file_name_test = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_large/excerpt_1/images_test_excerpt.npy"
nn_file_name_test = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_large/excerpt_1/measurements_1/recoveries_5/recoveries_test_excerpt.npy"
fista_file_name_test = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_large/excerpt_1/measurements_1/recoveries_6/recoveries_test_excerpt.npy"

# load images
org_img_train = np.load(org_file_name_train)
print(f"Original images training set with shape {org_img_train.shape} loaded from file '{org_file_name_train}'.")
nn_img_train = np.load(nn_file_name_train)
print(f"NN recoveries of training set with shape {nn_img_train.shape} loaded from file '{nn_file_name_train}'.")
fista_img_train = np.load(fista_file_name_train)
print(f"FISTA recoveries on training set with shape {fista_img_train.shape} loaded from file '{fista_file_name_train}'.")
org_img_test = np.load(org_file_name_test)
print(f"Original images test set with shape {org_img_test.shape} loaded from file '{org_file_name_test}'.")
nn_img_test = np.load(nn_file_name_test)
print(f"NN recoveries of test set with shape {nn_img_test.shape} loaded from file '{nn_file_name_test}'.")
fista_img_test = np.load(fista_file_name_test)
print(f"FISTA recoveries on test set with shape {fista_img_test.shape} loaded from file '{fista_file_name_test}'.")

# compute stats
nn_sup_norms_train = Evaluation.sup_norm_2D(org_img_train, nn_img_train)
nn_sup_norms_test = Evaluation.sup_norm_2D(org_img_test, nn_img_test)
fista_sup_norms_train = Evaluation.sup_norm_2D(org_img_train, fista_img_train)
fista_sup_norms_test = Evaluation.sup_norm_2D(org_img_test, fista_img_test)

# plot
fig, ax = plt.subplots(2, 2)
bins = [.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1.0]
x_label = "Distance from original image in sup-norm"
y_label = "Number of images"
ax[0,0].hist(nn_sup_norms_train, bins=bins)
ax[0,0].set_title("Sup-norms with NN on training set")
ax[0,0].set_xlabel(x_label)
ax[0,0].set_ylabel(y_label)
ax[0,1].hist(nn_sup_norms_test, bins=bins)
ax[0,1].set_title("Sup-norms with NN on test set")
ax[0,1].set_xlabel(x_label)
ax[0,1].set_ylabel(y_label)
ax[1,0].hist(fista_sup_norms_train, bins=bins)
ax[1,0].set_title("Sup-norms with FISTA on training set")
ax[1,0].set_xlabel(x_label)
ax[1,0].set_ylabel(y_label)
ax[1,1].hist(fista_sup_norms_test, bins=bins)
ax[1,1].set_title("Sup-norms with FISTA on test set")
ax[1,1].set_xlabel(x_label)
ax[1,1].set_ylabel(y_label)

plt.get_current_fig_manager().full_screen_toggle()
#fig.tight_layout(h_pad=.2, w_pad=.1)
plt.show()

