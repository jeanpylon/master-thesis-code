import matplotlib.pyplot as plt
import numpy as np
from os.path import join
from imageio import imwrite

def augment(image, a):
  """Augments an image by a.
  :param image: the image to augment as 2d numpy array
  :param a: augmentation factor
  """
  m, n = image.shape
  aug = np.ones((a,1))
  right = np.kron(np.eye(n), aug.T)
  left = np.kron(np.eye(m), aug)

  aug_image_hor = np.matmul(left, image)
  aug_image_ver = np.matmul(aug_image_hor, right)

  return aug_image_ver

# parameters
org_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/random_dots_128_large/excerpt_2/images_test_excerpt.npy"
nn_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/random_dots_128_large/excerpt_2/measurements_1/recoveries_5/recoveries_test_excerpt.npy"
fista_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/random_dots_128_large/excerpt_2/measurements_1/recoveries_2/recoveries_test_excerpt.npy"
save_folder_name = "/home/jab/Skole/Master/Figurer/chapter Experiments and results/recovered images/raw_images"

k = 3
a = 16

# load images
org_img = np.load(org_file_name)
nn_img = np.load(nn_file_name)
fista_img = np.load(fista_file_name)

for i in range(k):
  org_file_name = join(save_folder_name, f"Image_{i+1}_original.png")
  nn_file_name = join(save_folder_name, f"Image_{i+1}_NN_recovery.png")
  fista_file_name = join(save_folder_name, f"Image_{i+1}_FISTA_recovery.png")

  plt.imsave(org_file_name, augment(abs(org_img[i]), a), cmap="gray")
  plt.clf()
  plt.imsave(nn_file_name, augment(abs(nn_img[i]), a), cmap="gray")
  plt.clf()
  plt.imsave(fista_file_name, augment(abs(fista_img[i]), a), cmap="gray")
  plt.clf()





