"""
Plots bars showing PSNR on 25 sample MRI images.
"""

import numpy as np
from lib.data_generation.evaluation import Evaluation
import matplotlib.pyplot as plt
from os.path import join

# parameters
#org_file_name_train = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_medium/excerpt_1/images_train_excerpt.npy"
#nn_file_name_train = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_medium/excerpt_1/measurements_1/recoveries_1/recoveries_train_excerpt.npy"
#fista_file_name_train = "/home/jab/Skole/Master/data/data_sets/2d/random_dots_128_medium/excerpt_1/measurements_1/recoveries_2/recoveries_train_excerpt.npy" # DUMMY
org_file_name_test = "/home/jab/Skole/Master/extra_data/data_sets/2d/mri_512/images_test.npy"
nn_file_name_test = "/home/jab/Skole/Master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_5/recoveries_test.npy"
fista_file_name_test = "/home/jab/Skole/Master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_2/recoveries_test.npy"
save_path = "/home/jab/Skole/Master/Figurer/chapter Validation/"

# load images
#org_img_train = np.load(org_file_name_train)
#print(f"Original images training set with shape {org_img_train.shape} loaded from file '{org_file_name_train}'.")
#nn_img_train = np.load(nn_file_name_train)
#print(f"NN recoveries of training set with shape {nn_img_train.shape} loaded from file '{nn_file_name_train}'.")
#fista_img_train = np.load(fista_file_name_train)
#print(f"FISTA recoveries on training set with shape {fista_img_train.shape} loaded from file '{fista_file_name_train}'.")
org_img_test = np.load(org_file_name_test)
print(f"Original images test set with shape {org_img_test.shape} loaded from file '{org_file_name_test}'.")
nn_img_test = np.load(nn_file_name_test)
print(f"NN recoveries of test set with shape {nn_img_test.shape} loaded from file '{nn_file_name_test}'.")
fista_img_test = np.load(fista_file_name_test)
print(f"FISTA recoveries on test set with shape {fista_img_test.shape} loaded from file '{fista_file_name_test}'.")

# compute stats
#nn_psnrs_train = Evaluation.PSNR_2D(org_img_train, nn_img_train)
nn_psnrs_test = Evaluation.PSNR_2D(org_img_test, nn_img_test)
#fista_psnrs_train = Evaluation.PSNR_2D(org_img_train, fista_img_train)
fista_psnrs_test = Evaluation.PSNR_2D(org_img_test, fista_img_test)

#nn_sup_norms_train = Evaluation.sup_norm_2D(org_img_train, nn_img_train)
nn_sup_norms_test = Evaluation.sup_norm_2D(org_img_test, nn_img_test)
#fista_sup_norms_train = Evaluation.sup_norm_2D(org_img_train, fista_img_train)
fista_sup_norms_test = Evaluation.sup_norm_2D(org_img_test, fista_img_test)

# make file names
#psnr_train_file_name = join(save_path, "psnr_train_plot.png")
psnr_test_file_name = join(save_path, "psnr_test_plot_MRI_val.png")
#sup_norm_train_file_name = join(save_path, "sup_norm_train_plot.png")
sup_norm_test_file_name = join(save_path, "sup_norm_test_plot_MRI_val.png")

# plot PSNRs training set
# plot PSNRs test set
plt.clf()
X = np.arange(len(nn_psnrs_test)) * 1.0
offset = 0
plt.bar(X + offset, nn_psnrs_test, color="b", width=0.25, label="Neural net")
offset = .25
plt.bar(X + offset, fista_psnrs_test, color="g", width=0.25, label="FISTA")

plt.legend()
plt.ylabel("Peak signal noise ratio")
plt.xlabel("Image number")
plt.title("PSNRs of reconstructions on the 512 MRI test set")
plt.get_current_fig_manager().full_screen_toggle()
plt.savefig(psnr_test_file_name)
plt.show()
"""

# plot sup-norms training set
plt.clf()

# plot sup-norms test set
plt.clf()
X = np.arange(len(nn_psnrs_test)) * 1.0
offset = 0
plt.bar(X + offset, nn_sup_norms_test, color="b", width=0.25, label="Neural net")
offset = .25
plt.bar(X + offset, fista_sup_norms_test, color="g", width=0.25, label="FISTA")

plt.legend()
plt.ylabel("Sup-norm error")
plt.xlabel("Image number")
plt.title("Sup-norm errors of reconstrucions on the test set")
plt.get_current_fig_manager().full_screen_toggle()
plt.savefig(sup_norm_test_file_name)
plt.show()

"""
