import numpy as np
import matplotlib.pyplot as plt

# parameters
LISTA_results_file_name = "/home/jab/Skole/Master/extra_data/data_sets/1d/random_sparse_500/measurements_1/recoveries_1/recoveries_test_stat.npz"
LISTAcp_results_file_name = "/home/jab/Skole/Master/extra_data/data_sets/1d/random_sparse_500/measurements_1/recoveries_3/recoveries_test_stat.npz"
LISTAss_results_file_name = "/home/jab/Skole/Master/extra_data/data_sets/1d/random_sparse_500/measurements_1/recoveries_4/recoveries_test_stat.npz"
LISTAcpss_results_file_name = "/home/jab/Skole/Master/extra_data/data_sets/1d/random_sparse_500/measurements_1/recoveries_2/recoveries_test_stat.npz"

LISTA_results = np.load(LISTA_results_file_name)["nmse"]
LISTAcp_results = np.load(LISTAcp_results_file_name)["nmse"]
LISTAss_results = np.load(LISTAss_results_file_name)["nmse"]
LISTAcpss_results = np.load(LISTAcpss_results_file_name)["nmse"]

plt.plot(LISTA_results, label="LISTA", marker="+")
plt.plot(LISTAcp_results, label="LISTAcp", marker="+")
plt.plot(LISTAss_results, label="LISTAss", marker="+")
plt.plot(LISTAcpss_results, label="LISTAcpss", marker="+")

plt.legend()
plt.xlabel("Layer")
plt.ylabel("NMSE")

plt.show()


