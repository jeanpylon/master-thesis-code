"""
Creates a figure of the desired number of MRI 512 images with their NN and FISTA recoveries.
"""

import matplotlib.pyplot as plt
import numpy as np

# parameters
org_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/mri_512/images_test.npy"
nn_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_5/recoveries_test.npy"
fista_file_name = "/home/jab/Skole/Master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_2/recoveries_test.npy"
save_file_name = "/home/jab/Skole/Master/Figurer/chapter Validation/img_comp.png"
k = 3

# load images
org_img = np.load(org_file_name)
nn_img = np.load(nn_file_name)
fista_img = np.load(fista_file_name)

# make figure
columns = 3
rows = k
fig, ax = plt.subplots(rows, columns)

# pick random images
perm = np.random.choice(len(org_img), k,replace=False)

for row in range(rows):
  ax[row, 0].imshow(abs(org_img[perm[row]]), cmap="gray", vmin=0, vmax=1)
  ax[row, 0].axis("off")
  ax[row, 1].imshow(abs(nn_img[perm[row]]), cmap="gray", vmin=0, vmax=1)
  ax[row, 1].axis("off")
  ax[row, 2].imshow(abs(fista_img[perm[row]]), cmap="gray", vmin=0, vmax=1)
  ax[row, 2].axis("off")

# add text
ax[0, 0].set_title("Original images")
ax[0, 1].set_title("Recovery with the neural net")
ax[0, 2].set_title("Recovery with FISTA")

# fix layout
fig.tight_layout(h_pad=.2, w_pad=.1)
plt.subplots_adjust(left=.07 ,right=.555, wspace=0)

#plt.savefig(save_file_name)
plt.get_current_fig_manager().full_screen_toggle()
plt.show()
