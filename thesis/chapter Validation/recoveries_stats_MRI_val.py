"""
Computes statistics on the recoveries of 512 MRI images by NN and FISTA
"""

import numpy as np
from os.path import expanduser, join
from lib.data_generation.evaluation import Evaluation

# parameters
org_file_name_train = join(expanduser("~"), "master/extra_data/data_sets/2d/mri_512/images_train.npy")
nn_file_name_train = join(expanduser("~"), "master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_5/recoveries_train.npy")
#fista_file_name_train = join(expanduser("~"), "master/data/data_sets/2d/random_dots_128_medium/measurements_1/recoveries_2/recoveries_train.npy")
org_file_name_test = join(expanduser("~"), "master/extra_data/data_sets/2d/mri_512/images_test.npy")
nn_file_name_test = join(expanduser("~"), "master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_5/recoveries_test.npy")
fista_file_name_test = join(expanduser("~"), "master/extra_data/data_sets/2d/mri_512/measurements_2/recoveries_2/recoveries_test.npy")

# load images
org_img_train = np.load(org_file_name_train)
print(f"Original images training set with shape {org_img_train.shape} loaded from file '{org_file_name_train}'.")
nn_img_train = np.load(nn_file_name_train)
print(f"NN recoveries of training set with shape {nn_img_train.shape} loaded from file '{nn_file_name_train}'.")
#fista_img_train = np.load(fista_file_name_train)
#print(f"FISTA recoveries on training set with shape {fista_img_train.shape} loaded from file '{fista_file_name_train}'.")
org_img_test = np.load(org_file_name_test)
print(f"Original images test set with shape {org_img_test.shape} loaded from file '{org_file_name_test}'.")
nn_img_test = np.load(nn_file_name_test)
print(f"NN recoveries of test set with shape {nn_img_test.shape} loaded from file '{nn_file_name_test}'.")
fista_img_test = np.load(fista_file_name_test)
print(f"FISTA recoveries on test set with shape {fista_img_test.shape} loaded from file '{fista_file_name_test}'.")

# compute stats
nn_psnrs_train = Evaluation.PSNR_2D(org_img_train, np.abs(nn_img_train))
nn_psnrs_test = Evaluation.PSNR_2D(org_img_test, np.abs(nn_img_test))
#fista_psnrs_train = Evaluation.PSNR_2D(org_img_train, fista_img_train)
fista_psnrs_test = Evaluation.PSNR_2D(org_img_test, np.abs(fista_img_test))

nn_sup_norms_train = Evaluation.sup_norm_2D(org_img_train, nn_img_train)
nn_sup_norms_test = Evaluation.sup_norm_2D(org_img_test, nn_img_test)
#fista_sup_norms_train = Evaluation.sup_norm_2D(org_img_train, fista_img_train)
fista_sup_norms_test = Evaluation.sup_norm_2D(org_img_test, fista_img_test)

# print stats
print("{:<15} {:<25} {:<25} {:<25} {:<25}".format("", "NN on train", "NN on test", "FISTA on train", "FISTA on test"))
print("{:<15} {:<25.2f} {:<25.2f} {:<25.2f} {:<25}".format("Min PSNR", nn_psnrs_train.min(), nn_psnrs_test.min(), "Not implemented.", fista_psnrs_test.min()))
print("{:<15} {:<25} {:<25} {:<25} {:<25}".format("Max PSNR", nn_psnrs_train.max(), nn_psnrs_test.max(), "Not implemented.", fista_psnrs_test.max()))
print("{:<15} {:<25} {:<25} {:<25} {:<25}".format("Mean PSNR", nn_psnrs_train.mean(), nn_psnrs_test.mean(), "Not implemented.", fista_psnrs_test.mean()))

print("{:<15} {:<25} {:<25} {:<25} {:<25}".format("Min sup-norm", nn_sup_norms_train.min(), nn_sup_norms_test.min(), "Not implemented.", fista_sup_norms_test.min()))
print("{:<15} {:<25} {:<25} {:<25} {:<25}".format("Max sup-norm", nn_sup_norms_train.max(), nn_sup_norms_test.max(), "Not implemented.", fista_sup_norms_test.max()))
print("{:<15} {:<25} {:<25} {:<25} {:<25}".format("Mean sup-norm", nn_sup_norms_train.mean(), nn_sup_norms_test.mean(), "Not implemented.", fista_sup_norms_test.mean()))

