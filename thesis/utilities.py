import numpy as np

def load_org_nn_fista(org_img_file_name, nn_rec_file_name, fista_rec_file_name):
  org_images = np.load(org_img_file_name)
  print(f"Original images with shape {org_images.shape} loaded from file '{org_img_file_name}'.")
  nn_recoveries = np.load(nn_rec_file_name)
  print(f"Neural net recoveries with shape {nn_recoveries.shape} loaded from file '{nn_rec_file_name}'.")
  fista_recoveries = np.load(fista_rec_file_name)
  print(f"Original images with shape {fista_recoveries.shape} loaded from file '{fista_rec_file_name}'.")

  return org_images, nn_recoveries, fista_recoveries


def compute ():
  pass