"""
Plots the l1 and l2 unit circle.
"""
import matplotlib.pyplot as plt
import numpy as np

fig, ax = plt.subplots(1,2)
print(type(ax))
constraint_x = np.linspace(-1.5, 1.5, 10)
constraint_y = constraint_x*(-2) + 2

# l1 circle

x = [-1, 0, 1, 0, -1]
y = [0, 1, 0, -1, 0]


ax[0].plot(x, y)
ax[0].plot(constraint_x, constraint_y)
ax[0].set_ylim((-1.1, 1.1))
ax[0].set_aspect('equal')

ax[0].axhline(y=0, color='k')
ax[0].axvline(x=0, color='k')

#l2 circle

t = np.linspace(0,2*np.pi,100)
r = .895
x = r*np.cos(t)
y = r*np.sin(t)

ax[1].plot(x, y)
ax[1].plot(constraint_x, constraint_y)
ax[1].set_ylim((-1.1, 1.1))
ax[1].set_aspect('equal')

ax[1].axhline(y=0, color='k')
ax[1].axvline(x=0, color='k')
plt.show()

