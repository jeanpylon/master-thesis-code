import numpy as np
from os.path import join, expanduser
import json

# parameters
p_b = 0.1 # corresponds to config.pnz (-P, --pnz) in LISTA code
N = 500
m = 250
k = 1_000_000 # should be 100_000 maybe
test_ratio = 0.01

train_signals_file_name = join(expanduser("~"), "master/extra_data/data_sets/1d/random_sparse_500/images_train")
test_signals_file_name = join(expanduser("~"), "master/extra_data/data_sets/1d/random_sparse_500/images_test")
meta_data_file_name = join(expanduser("~"), "master/extra_data/data_sets/1d/random_sparse_500/meta_data.json")
# set up sparse signals with shape (N, batch size)
supp_prob = np.random.random((k, N, 1))
supp = supp_prob < p_b

magnitudes = np.random.normal(size=(k, N, 1))
x = np.multiply(supp, magnitudes)

# partition into training and test signals
test_size = round(test_ratio*k)
x_train = x[test_size:].astype(np.float32)
x_test = x[:test_size].astype(np.float32)

# save sparse signals
np.save(train_signals_file_name, x_train)
print(f"Training signals with standard Gaussian distributed magnitudes, p_b={p_b} and shape={x_train.shape} saved to file '{train_signals_file_name}.npy'.")
np.save(test_signals_file_name, x_test)
print(f"Test signals with standard Gaussian distributed magnitudes, p_b={p_b} and shape={x_test.shape} saved to file '{test_signals_file_name}.npy'.")

# make meta data file
meta_data = {"Tag": "Sparse 500 entry signals",
             "Image description": "500 entries long signals with random support of mean size 50. Magnitude of entries in support is standard normal distributed.",
             "Total number of images": k,
             "Number of images for training set": k - test_size,
             "Number of images for test set": test_size,
             "Training images path": train_signals_file_name,
             "Tset images path": test_signals_file_name}

dictionary = {"Images": meta_data}

with open(meta_data_file_name, "w") as file:
  json.dump(dictionary, file, indent=2)
  print(f"Meta data successfully saved to file '{meta_data_file_name}'.")

