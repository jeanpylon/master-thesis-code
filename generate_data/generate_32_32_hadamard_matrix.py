""" Generates a 32 by 32 Hadamard sequency ordered matrix. """

from lib.data_generation.generate_hadamard_matrix import GenerateHadamardMatrix
import numpy as np
from os.path import join, expanduser

# parameters
n = 128
sampling_rate = 0.05
m = round(128**2*sampling_rate)

# generate hadamard matrix
print("Setting up {}x{}-matrix ...".format(n, n))
gen = GenerateHadamardMatrix()
gen.generate_sequency_hadamard_matrix(n)

# take kroenecker product with itself
print("Take kroenecker to yield {}x{}-matrix ...".format(n**2, n**2))
gen.kroenecker()

# subsample
print("Subsample ...")
subset = np.random.choice(gen.N, m)
A = gen.matrix
A_sub = A[subset].astype(np.float32)

# save
print("Saving ...")
file_path = join(expanduser("~"), "master/extra_data/matrices/{}x{}_sequency_hadamard_kroenecker_matrix".format(m, gen.N))
np.save(file_path, A_sub)
print("Matrix with shape {} succesfully saverd to file '{}'.".format(A_sub.shape, file_path))
