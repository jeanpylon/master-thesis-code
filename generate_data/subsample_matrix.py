from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern
import numpy as np

matrix_filename = "/home/jab/Skole/Master/extra_data/matrices/1024x1024_sequency_hadamard_kroenecker_matrix.npy"
sampling_scheme_file_name = "/home/jab/Skole/Master/extra_data/sampling_patterns/n_32/sampling_pattern_uniform_n32_sr0.050.mat"
sampling_pattern = MatLabSamplingPattern(sampling_scheme_file_name)
matrix = np.load(matrix_filename)

subsampled_matrix = matrix[sampling_pattern.sampling_pattern, :]
print(sampling_pattern.meta_data)
print(subsampled_matrix.shape)

subsampled_matrix_file_name = \
  f"/home/jab/Skole/Master/extra_data/matrices/{subsampled_matrix.shape[0]}x{subsampled_matrix.shape[1]}_sequency_hadamard_kroenecker_matrix.npy"

np.save(subsampled_matrix_file_name, subsampled_matrix)
print(f"Subsampled matrix saved to file '{subsampled_matrix_file_name}'.")
