print("Loading modules ...")
from lib.data_generation.image_set_generator import ImageSetGenerator
from lib.data_generation.measurement_simulator import MeasurementSimulator
from lib.sampling_operators.sampling_operator_fourier import SamplingOperatorFourier
from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern

# parameters
test_ratio = .053
img_path = "/home/jab/Skole/Master/raw_data/mri_512"
samp_patt_path = "/home/jab/Skole/Master/data/sampling_patterns/sampling_pattern_0.05.mat"
folder = "/home/jab/Skole/Master/data/data_sets/2D/mri"

# generate images
img_gen = ImageSetGenerator()
print("Loading images ...")
img_gen.load_dicom_images(img_path)
img_gen.save_image_set(folder, ratio=test_ratio, tag="MR images.")
del img_gen # free up memory

# simulate measurements
samp_op = SamplingOperatorFourier(MatLabSamplingPattern(samp_patt_path))
mes_sim = MeasurementSimulator(samp_op)
mes_sim.load_image_set(folder)
print("Simulating measurements ...")
mes_sim.simulate_measurements()
mes_sim.save_measurements()


