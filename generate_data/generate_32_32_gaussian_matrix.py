from utils.prob import random_A
from os.path import join, expanduser
import numpy as np

M = 51
N = 1024
con_num = 0
col_norm = True
file_name = join(expanduser("~"), "master/extra_data/matrices/51x1024_gaussian_matrix")
A = random_A(M, N, con_num=con_num, col_normalized=col_norm)
np.save(file_name, A)
