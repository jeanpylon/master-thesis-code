print("Loading modules ...")
from lib.data_generation.image_set_generator import ImageSetGenerator
from os.path import expanduser, join

# hyperparameters
test_ratio = .01
k = 50_000
folder = join(expanduser("~"), "master/data/data_sets/2d/random_dots_128_large")
n = 128
dots = 7
dot_size = 1

# generate images
img_gen = ImageSetGenerator()
print("Generating images ...")
img_gen.generate_random_dots_images(k, n, dots, dot_size, threshold=.2)
img_gen.save_image_set(folder, ratio=test_ratio, tag="Randomly generated images.")
