""" Generates a 64 by 64 Hadamard sequency ordered matrix. """

from lib.data_generation.generate_hadamard_matrix import GenerateHadamardMatrix
import numpy as np
from os.path import join, expanduser

# parameters
n = 64
samplingrate = 0.10
m = round(n**2*samplingrate)

# generate hadamard matrix
gen = GenerateHadamardMatrix()
gen.generate_sequency_hadamard_matrix(n)

# take kroenecker product with itself
gen.kroenecker()

# subsample

subset = np.random.choice(gen.N, m)
A = gen.matrix
A_sub = A[subset].astype(np.float32)/(n**2)

# save
file_path = join(expanduser("~"), "master/extra_data/matrices/{}x{}_sequency_hadamard_kroenecker_matrix".format(m, gen.N))
np.save(file_path, A_sub)
