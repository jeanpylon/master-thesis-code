import numpy as np
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Shows results from LISTA network result files (.npz-files).")
parser.add_argument("file_names", metavar="results files", type=str, help="File name(s) of .npz file(s) containing results dictionary.", nargs="+")
args = parser.parse_args()


results = []
name = []

for file_name in args.file_names:
  dict = np.load(file_name)
  nmse = dict["nmse"]
  #nmse = np.reshape((nmse.shape[1],))
  print([key for key in dict.keys()])
  results.append(nmse)
  name.append(file_name)

for y, label in zip(results, name):
  x = np.arange(len(y))
  plt.plot(x, y, label=label)

plt.legend()
plt.show()


