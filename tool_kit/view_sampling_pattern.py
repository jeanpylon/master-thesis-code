import matplotlib.pyplot as plt
import numpy as np
import argparse
from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern

# parse args
parser = argparse.ArgumentParser("This program displays sampling patterns.")
parser.add_argument("source", metavar="source path", type=str, help="Name of the sampling pattern file.")
parser.add_argument("-r", "--row", type=int, help="Plot only this row")
parser.add_argument("-c", "--column", type=int, help="Plot only this column")
args = parser.parse_args()

# initiate SamplingPattern object
samp_patt = MatLabSamplingPattern(args.source)
A = np.zeros((samp_patt.n**2,))
A[samp_patt.sampling_pattern] = 1
A = A.reshape((samp_patt.n, samp_patt.n), order="F")

# show sampling pattern
if args.row is None:
  if args.column is None:
    plt.imshow(A, cmap="gray", vmin=0.0, vmax=1.0)
  else:
    plt.imshow(A[:, args.column:args.column + 1], cmap="gray", vmin=0.0, vmax=1.0)
else:
  if args.column is None:
    plt.imshow(A[args.row:args.row + 1, :], cmap="gray", vmin=0.0, vmax=1.0)
  else:
    plt.imshow(A[args.row:args.row + 1, args.column:args.column + 1], cmap="gray", vmin=0.0, vmax=1.0)

plt.show()
