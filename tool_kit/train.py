import argparse
from os.path import abspath, split
from lib.models.optimizers import get_default_RMSprop, get_default_Adagrad

# parse args
parser = argparse.ArgumentParser(description="This program trains neural nets.")
parser.add_argument("source", metavar="source path", type=str, help="Name of the folder containing the input file. Expects parent folder to contain output file.")
parser.add_argument("model", metavar="neural net structure", type=str, help="Name of the structure of the neural net to use.")
parser.add_argument("--epochs", "-e", metavar="epochs of training", type=int, help="Number of training epochs.")
parser.add_argument("--batch_size", "-b", metavar="batch size", type=int, help="Number of samples in training batch.")
parser.add_argument("--optimizer", "-o", metavar="optimizer", type=str, default="sgd", help="Optimizer. Default is SGD.")
parser.add_argument("savename", metavar="model save name", type=str, help="Model save name.")
args = parser.parse_args()

optimizer, optimizer_meta_data = (None, None)
if args.optimizer.lower() == "sgd":
  pass
elif args.optimizer.lower() == "rmsprop":
  optimizer, optimizer_meta_data = get_default_RMSprop()
elif args.optimizer.lower() == "adagrad":
  optimizer, optimizer_meta_data = get_default_Adagrad()
else:
  raise ValueError(f"Unknown optimizer name: '{args.optimizer}'.")

from lib.models.model_trainer import ModelTrainer
if args.model == "ell50":
  from lib.models.ell50_2D import setup_ELL50_2D
  mod_train = ModelTrainer(setup_ELL50_2D, True, optimizer=optimizer, optimizer_meta_data=optimizer_meta_data)
  inputs_path = abspath(args.source)
  targets_path, dummy = split(inputs_path)
  mod_train.train(inputs_path, targets_path, epochs=args.epochs, batch_size=args.batch_size)
  mod_train.save(args.savename)
elif args.model == "automap":
  from lib.models.AUTOMAP import setup_AUTOMAP
  mod_train = ModelTrainer(setup_AUTOMAP, False, optimizer=optimizer, optimizer_meta_data=optimizer_meta_data)
  inputs_path = abspath(args.source)
  targets_path, dummy = split(inputs_path)
  mod_train.train(inputs_path, targets_path, epochs=args.epochs, batch_size=args.batch_size)
  mod_train.save(args.savename)
else:
  raise ValueError(f"Unknown model: '{args.model}'.")


