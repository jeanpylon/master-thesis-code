import numpy as np
import argparse
from os.path import abspath
from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern
from lib.sampling_operators.sampling_operator_fourier import SamplingOperatorFourier
from lib.sampling_operators.sampling_operator_gaussian import SamplingOperatorGaussian
from lib.sampling_operators.sampling_operator_gaussian_1d import SamplingOperatorGaussian1d
from lib.sampling_operators.sampling_operator_hadamard import SamplingOperatorHadamard
from lib.sampling_operators.sampling_operator_hadamard_fast import SamplingOperatorHadamardFast
from lib.sampling_operators.sampling_operator_hadamard_tensor import SamplingOperatorHadamardTensor
from lib.data_generation.measurement_simulator import MeasurementSimulator

# parse args
parser = argparse.ArgumentParser("This program simulates measurements from images.")
parser.add_argument("source", metavar="source path", type=str, help="Name of the folder containing the image files.")
#parser.add_argument("destination", metavar="destination path", type=str, help="The simulated measurements will be stored in this folder.")
parser.add_argument("sampling_operator", metavar="sampling operator", type=str, help="The name of the sampling operator to use.")
parser.add_argument("--matrix", "-m", metavar="matrix file name", type=str, help="File name of the matrix to use. (.npy-file)")
parser.add_argument("--sampling_pattern", "-s", metavar="sampling pattern file name", type=str, help="File name of the sampling pattern to use. (.mat-file)")
args = parser.parse_args()

# load sampling operator
sampling_operator = None
if args.sampling_operator == "fourier":
  if args.sampling_pattern is None:
    raise ValueError("No sampling pattern given. Use optional argument -s.")
  sampling_pattern = MatLabSamplingPattern(abspath(args.sampling_pattern))
  sampling_operator = SamplingOperatorFourier(sampling_pattern)
elif args.sampling_operator == "gaussian":
  matrix_file_name = args.matrix
  sampling_pattern = MatLabSamplingPattern(abspath(args.sampling_pattern))
  sampling_operator = SamplingOperatorGaussian(matrix_file_name, sampling_pattern)
elif args.sampling_operator == "gaussian1d":
  matrix_file_name = args.matrix
  sampling_pattern = MatLabSamplingPattern(abspath(args.sampling_pattern))
  sampling_operator = SamplingOperatorGaussian1d(matrix_file_name, sampling_pattern)
elif args.sampling_operator == "hadamard":
  matrix_file_name = args.matrix
  sampling_pattern = MatLabSamplingPattern(abspath(args.sampling_pattern))
  sampling_operator = SamplingOperatorHadamard(matrix_file_name, sampling_pattern)
elif args.sampling_operator == "hadamardfast":
  sampling_pattern = MatLabSamplingPattern(abspath(args.sampling_pattern))
  sampling_operator = SamplingOperatorHadamardFast(sampling_pattern)
elif args.sampling_operator == "hadamardtensor":
  matrix_file_name = args.matrix
  sampling_pattern = MatLabSamplingPattern(abspath(args.sampling_pattern))
  sampling_operator = SamplingOperatorHadamardTensor(matrix_file_name, sampling_pattern)
else:
  raise ValueError(f"Unknown sampling operator: '{args.sampling_operator}'.")

mes_sim = MeasurementSimulator(sampling_operator)
mes_sim.load_image_set(abspath(args.source))
mes_sim.simulate_measurements()
mes_sim.save_measurements()

