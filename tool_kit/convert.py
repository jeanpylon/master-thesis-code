import argparse
from lib.data_generation.tf_records_converter import TFRecords_converter

parser = argparse.ArgumentParser(description="Converts .npy files to .tfrec files.")
parser.add_argument("filename", metavar="images file", type=str, help=".npy file containing images.")
parser.add_argument("savefolder", metavar="save folder", type=str, help="Folder to which .tfrec file(s) will be saved.")
parser.add_argument("type", metavar="type", type=str, help="'train' saves a training data set and a validation data set. 'test' saves only a test data set.")
parser.add_argument("-r", "--ratio", type=float, default=0.01, help="Size ratio of validation set and train set when type=='train'.")
args = parser.parse_args()

tf_rec_conv = TFRecords_converter()

if args.type=="train":
  tf_rec_conv.save_as_tfrec(args.filename, args.savefolder, "train", ratio=args.ratio)
elif args.type=="test":
  tf_rec_conv.save_as_tfrec(args.filename, args.savefolder, "test")
else:
  raise ValueError("Argument type has invalid value: '{args.type}'.")

