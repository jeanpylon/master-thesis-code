import argparse

# parse args
parser = argparse.ArgumentParser(description="This program recovers images from measurements.")
parser.add_argument("source", metavar="source path", type=str, help="Name of the folder containing the measurement files.")
parser.add_argument("-d", "--decoder", metavar="decoder", type=str, help="Name of the decoder to use (default='adjoint').", nargs="+")
parser.add_argument("-r", "--trainonly", action="store_true", help="Recover training set only.")
parser.add_argument("-s", "--testonly", action="store_true", help="Recover test set only.")
parser.add_argument("-w", "--wavelet", metavar="wavelet", type=str, help="Wavelet for sparsifying tranform. Default: zero")
#parser.add_argument("-c", "--choose", metavar="choose", type=str, help="Reconstruct only the first c images.")
parser.add_argument("-a", "--chooserandom", metavar="chooserandom", type=str, help="Reconstruct only c random images.")
args = parser.parse_args()

# error check
if args.trainonly and args.testonly:
  raise ValueError("Cannot only recover test set and only recover training set.")

from os.path import join
from lib.sampling_operators.sampling_pattern import MatLabSamplingPattern
from lib.sampling_operators.sampling_operator_fourier import SamplingOperatorFourier
from lib.sampling_operators.sampling_operator_wavelet_fourier import SamplingOperatorWaveletFourier
from lib.sampling_operators.sampling_operator_gaussian import SamplingOperatorGaussian
from lib.sampling_operators.sampling_operator_hadamard import SamplingOperatorHadamard
from lib.sampling_operators.sampling_operator_hadamard_fast import SamplingOperatorHadamardFast
from lib.sampling_operators.sampling_operator_wavelet_hadamard import SamplingOperatorWaveletHadamard
from lib.sampling_operators.sampling_operator_wavelet_hadamard_fast import SamplingOperatorWaveletHadamardFast
from lib.sampling_operators.sampling_operator_hadamard_tensor import SamplingOperatorHadamardTensor
from lib.sampling_operators.sampling_operator_wavelet_hadamard_tensor import SamplingOperatorWaveletHadamardTensor
from lib.decoders.decoder_adjoint import DecoderAdjoint
from lib.decoders.decoder_fista import DecoderFISTA
from lib.decoders.decoder_adjoint_neural_net import DecoderAdjointNeuralNet
from lib.data_generation.image_recovery import ImageRecovery
import json

wavelet_type = "db4"

# load parameters
meta_data_file_name = join(args.source, "meta_data.json")
with open(meta_data_file_name, "r") as file:
  meta_data = json.load(file)

sampling_pattern_file_name = meta_data["Sampling operator"]["Sampling pattern"]["File name"]
sampling_pattern = MatLabSamplingPattern(sampling_pattern_file_name)
sampling_operator_name = meta_data["Sampling operator"]["Operator type"]

# instantiate
sampling_operator = None
extension = "zero"
if sampling_operator_name == "P o F":
  if args.wavelet is not None:
    extension = args.wavelet
    sampling_operator = SamplingOperatorWaveletFourier(sampling_pattern, wavelet_type, extension)
  else:
    sampling_operator = SamplingOperatorFourier(sampling_pattern)
elif sampling_operator_name == "P o G":
  matrix_file_name = meta_data["Sampling operator"]["Matrix file name"]
  sampling_operator = SamplingOperatorGaussian(matrix_file_name, sampling_pattern)
elif sampling_operator_name == "P o H":
  matrix_file_name = meta_data["Sampling operator"]["Matrix file name"]
  if args.wavelet is not None:
    extension = args.wavelet
    sampling_operator = SamplingOperatorWaveletHadamard(matrix_file_name, sampling_pattern, wavelet_type, extension)
  else:
    sampling_operator = SamplingOperatorHadamard(matrix_file_name, sampling_pattern)
elif sampling_operator_name == "P o fH":
  if args.wavelet is not None:
    extension = args.wavelet
    sampling_operator = SamplingOperatorWaveletHadamardFast(sampling_pattern, wavelet_type, extension)
  else:
    sampling_operator = SamplingOperatorHadamardFast(sampling_pattern)
elif sampling_operator_name == "P o H0H^T":
  matrix_file_name = meta_data["Sampling operator"]["Matrix file name"]
  if args.wavelet is not None:
    extension = args.wavelet
    sampling_operator = SamplingOperatorWaveletHadamardTensor(matrix_file_name, sampling_pattern, wavelet_type, extension)
  else:
    sampling_operator = SamplingOperatorHadamardTensor(matrix_file_name, sampling_pattern)
else:
  raise ValueError(f"Unknown sampling operator type: '{sampling_operator_name}'.")

decoder = None
if args.decoder[0] is None:
  decoder = DecoderAdjoint(sampling_operator)
elif args.decoder[0] == "adjoint":
  decoder = DecoderAdjoint(sampling_operator)
elif args.decoder[0] == "fista":
  if len(args.decoder) > 2:
    decoder = DecoderFISTA(sampling_operator, iterations=int(args.decoder[1]), lmb=float(args.decoder[2]))
  elif len(args.decoder) > 1:
    decoder = DecoderFISTA(sampling_operator, iterations=int(args.decoder[1]))
  else:
    decoder = DecoderFISTA(sampling_operator)
elif args.decoder[0] == "neuralnet":
  if len(args.decoder) > 1:
    decoder = DecoderAdjointNeuralNet(sampling_operator, args.decoder[1], parallel=False)
  else:
    raise ValueError("Missing neural net file name.")
else:
  raise ValueError(f"Unknown decoder name: '{args.decoder}'.")

img_rec = None
if args.trainonly:
  img_rec = ImageRecovery(decoder, test_set=False)
elif args.testonly:
  img_rec = ImageRecovery(decoder, train_set=False)
else:
  img_rec = ImageRecovery(decoder)

img_rec.load_data_set(args.source)
if args.chooserandom is not None:
  img_rec.recover(choose_k=int(args.chooserandom))
else:
  img_rec.recover()

img_rec.save_recoveries()

