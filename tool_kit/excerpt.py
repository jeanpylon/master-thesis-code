import argparse
from lib.data_generation.excerpt import Excerpt
from os.path import abspath, split

# parse args
parser = argparse.ArgumentParser(description="This program creates an excerpt of an image folder with measurements and recoveries.")
parser.add_argument("source", metavar="source path", type=str, help="Name of the image folder.")
parser.add_argument("k_train", metavar="datapoints training set", type=int, help="Number of datapoints to choose from training set.")
parser.add_argument("k_test", metavar="datapoints test set", type=int, help="Number of datapoints to choose from test set.")
parser.add_argument("-r", "--randomtrain", action="store_true", help="Choose datapoints uniformly at random from the training set.")
parser.add_argument("-s", "--randomtest", action="store_true", help="Choose datapoints uniformly at random from the test set.")

args = parser.parse_args()

exc = Excerpt()
exc.make(args.source, args.k_train, args.k_test, random_train=args.randomtrain, random_test=args.randomtest)





