import numpy as np
import argparse

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("filenames", type=str, help="Name of the .npy file(s) to print the shape of.", nargs="+")
args = arg_parser.parse_args()

for file_name in args.filenames:
  arr = np.load(file_name)
  print(f"Shape of array stored in file '{file_name}': {arr.shape}.")
