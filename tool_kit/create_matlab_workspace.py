import numpy as np
from scipy.io import savemat, loadmat
import argparse
from os.path import abspath, join, split

parser = argparse.ArgumentParser("This program compiles the data set into a .mat-file containing a workspace.")
parser.add_argument("source", metavar="source path", type=str, help="Name of the folder containing the measurement files.")
#parser.add_argument("destination", metavar="destination path", type=str, help="The simulated measurements will be stored in this folder.")
#parser.add_argument("sampling_operator", metavar="sampling operator", type=str, help="The name of the sampling operator to use.")
parser.add_argument("--matrix", "-m", action="store_true", help="Use this option to convert just a npy. array file.")
args = parser.parse_args()

if not args.matrix:
  measurement_train_file_name = join(abspath(args.source), "measurements_train.npy")
  measurement_test_file_name = join(abspath(args.source), "measurements_test.npy")
  sampling_pattern_file_name = join(abspath(args.source), "sampling_pattern.mat")
  images_train_file_name = join(split(abspath(args.source))[0], "images_train.npy")
  images_test_file_name = join(split(abspath(args.source))[0], "images_test.npy")
  work_space_file_name = join(abspath(args.source), "matlab_workspace.mat")

  workspace = loadmat(sampling_pattern_file_name)

  workspace["images_train"] = np.load(images_train_file_name)
  workspace["images_test"] = np.load(images_test_file_name)
  workspace["measurements_train"] = np.load(measurement_train_file_name)
  workspace["measurements_test"] = np.load(measurement_test_file_name)

  savemat(work_space_file_name, workspace)
  print(f"Mat lab workspace success fully saved to file '{work_space_file_name}'.")
else:
  matrix_file_name = abspath(args.source)
  matrix = np.load(matrix_file_name)
  dict = {"Matrix": matrix,
          "Conversion filename": matrix_file_name}
  matlab_file_name = ".".join(matrix_file_name.split(".")[:-1]) + ".mat"
  savemat(matlab_file_name, mdict=dict)
