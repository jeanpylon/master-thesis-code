import numpy as np
import matplotlib.pyplot as plt
import argparse
from scipy.io import loadmat

separator_thicknes = 2

description = "This program compares images in two ways:\n"
description += "1. By default it takes the image sets and shows image i from each of them for i=1,2,...,k.\n"
description += "2. If option -p is passed it computes psnrs and presents this as a bar diagram."

parser = argparse.ArgumentParser(description=description)
parser.add_argument("paths", metavar="image files", type=str, help="File names of all image sets to compare.", nargs="+")
parser.add_argument("-f", "--fullscreen", action="store_true", help="Enable full screen.")
parser.add_argument("-p", "--psnr", type=str, help="Compute psnrs with this argument as ground truth.")
parser.add_argument("-i", "--infnorm", type=str, help="Compute infinity norm with this argument as ground truth.")
parser.add_argument("-n", "--normalize", action="store_true", help="Normalize each image set individually.")
parser.add_argument("-e", "--equal", type=str, help="Check if images are equal with this image set as ground truth.")
parser.add_argument("-c", "--choose", type=int, help="Choose c images for processing at random. (Use on large datasets.)")

args = parser.parse_args()

def load_img_file(file_name):
  file_ext = file_name.split(".")[-1]
  if file_ext == "mat":
    workspace = loadmat(file_name)
    return workspace["recovered_images"]
  elif file_ext == "npy":
    return np.load(file_name)
  else:
    raise ValueError("Unknown file format")

if args.psnr is not None or args.infnorm is not None:
  from lib.data_generation.evaluation import Evaluation

  # compute and present psnrs in bar diagram
  if args.psnr is not None:
    original_images = load_img_file(args.psnr)
    if len(original_images.shape) == 2:
      original_images = np.expand_dims(original_images, -1)
  if args.infnorm is not None:
    original_images = load_img_file(args.infnorm)
    if len(original_images.shape) == 2:
      original_images = np.expand_dims(original_images, -1)
  print(f"Ground truth images loaded successfully from file '{args.psnr}'.")

  reconstructed_images = []
  for file_name in args.paths:
    images = load_img_file(file_name)
    if len(images.shape) == 2:
      images = np.expand_dims(images, -1)
    reconstructed_images.append(images)
    print(f"Image file '{file_name}' loaded successfully.")

  if args.infnorm is not None:
    psnr = Evaluation.L_inf#Evaluation.PSNR_2D
  if args.psnr is not None:
    psnr = Evaluation.PSNR_2D
  psnrs = []

  for rec_img in reconstructed_images:
    psnrs.append(psnr(abs(original_images), abs(rec_img)))

  for psnr, file_name in zip(psnrs, args.paths):
    print(f"PSNR stats for images in file '{file_name}':")
    print(f"Maximum PSNR: {max(psnr)}, minimum PSNR: {min(psnr)}")

  if len(args.paths) > 4:
    raise ValueError("To many image recovery files to compare.")

  X = np.arange(len(psnrs[0]))*1.25
  offset = 0
  colors = ["b", "g", "m", "k"]

  for psnr, color, file_name in zip(psnrs, colors, args.paths):
    plt.bar(X + offset, psnr, color=color, width = 0.25, label=file_name)
    offset += .25

  plt.legend()
  plt.ylabel("PSNR")
  plt.xlabel("Image number")
  plt.title("PSNRs of reconstructions of images in test sets.")
  plt.show()
elif args.equal is not None:
  # compute and present psnrs in bar diagram
  original_images = load_img_file(args.equal)
  if len(original_images.shape) == 2:
    images = np.expand_dims(original_images, -1)
  print(f"Ground truth images loaded successfully from file '{args.psnr}'.")

  reconstructed_images = []
  for file_name in args.paths:
    reconstructed_images.append(load_img_file(file_name))
    print(f"Image file '{file_name}' loaded successfully.")

  for rec_imgs in reconstructed_images:
    for rec_img, org_img in zip(rec_imgs, original_images):
      correct = sum(abs(rec_img - org_img)**2)/(rec_img.shape[0]*rec_img.shape[1])
      print(f"Images had MSE: {correct}.")


else:
  # show images side by side, one by one
  n_image_files = len(args.paths)
  images = abs(load_img_file(args.paths[0]))
  print(f"Img shape {images.shape}")
  print(f"Image file '{args.paths[0]}' loaded successfully.")
  images_min = np.min(abs(images), axis=(1, 2))
  images_max = np.max(abs(images), axis=(1, 2))
  k, m, n = images.shape
  images_compared = np.concatenate([images, np.ones((k, m, separator_thicknes))], axis=-1)
  min_values = []
  max_values = []
  min_values.append(images_min)
  max_values.append(images_max)
  for i in range(1, n_image_files):
    images = abs(load_img_file(args.paths[i]))
    images_min = np.min(abs(images), axis=(1, 2))
    images_max = np.max(abs(images), axis=(1, 2))
    if args.normalize:
      for j, (img, norm) in enumerate(zip(images, images_max)):
        images[j] = img/norm
    images_compared = np.concatenate([images_compared, images, np.ones((k, m, separator_thicknes))], axis=-1)
    print(f"Img shape {images.shape}")
    print(f"Image file '{args.paths[i]}' loaded successfully.")
    min_values.append(images_min)
    max_values.append(images_max)
  min_values = np.array(min_values).T
  max_values = np.array(max_values).T

  for image, min_value, max_value in zip(images_compared, min_values, max_values):
    plt.clf()
    plt.imshow(abs(image), cmap="gray", vmin=0, vmax=1)
    title = ""
    for i, (min_v, max_v) in enumerate(zip(min_value, max_value)):
      title = title + ("Image %d: min=%.4f, max=%.4f. " % (i+1, min_v, max_v))
    plt.title(title)
    if args.fullscreen:
      plt.get_current_fig_manager().full_screen_toggle()
    plt.show()



